load SN12345_LAJIT2014raw   %Imports structure "Vel" from raw datafile
rawVel = Vel;

load data_processed         % Overwrites structure "Vel" from processed datafile

% Plot the counts from the echosounder to determine if/where the ADCP data
% begins to reflect off of the surface
% figure(2)
% pcolor(rawVel.yday, rawVel.z_adcp-110,rawVel.ec1_bm+rawVel.ec2_bm+rawVel.ec3_bm+rawVel.ec4_bm)
% shading flat 
% axis ij
% 
% figure(12)
% pcolor(Vel.yday, Vel.z,Vel.u)
% shading flat 
% axis ij

% Plot the velocity from the processed datafiles 
figure(1)
clim = [-0.3 0.3];
ncolors = 15;

ax(1) = subplot(4,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.u,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
dynamicDateTicks 
% datetick
colorbar
title('Eastward Velocity')

ax(2) = subplot(4,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.v,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
dynamicDateTicks 
% datetick
colorbar
title('Northward velocity')

ax(3) = subplot(4,1,3);
clim = [-0.1 0.1];
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.w,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
dynamicDateTicks 
% datetick
colorbar
title('Vertical Velocity')

% ax(4) = subplot(4,1,4);
% clim = [-0.1 0.1];
% % ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
% ezcf(Vel.dtnum,Vel.z,Vel.werr,clim,ncolors)
% shading flat
% axis ij
% colormap(redblue)
% caxis(clim)
% dynamicDateTicks 
% % datetick
% colorbar
% title('Error Velocity')

linkaxes(ax,'xy')



%rotate velocities to canyon coordinates
rot=0.7077; %angle to be rotated in radians- positive is counterclockwise rotation

% rotate vectors to new coordinates
Vel.ac = Vel.u.*cos(rot)-Vel.v.*sin(rot); %ac= along canyon
Vel.xc = Vel.u.*sin(rot)+Vel.v.*cos(rot); %xc= across canyon

figure(2)
clim = [-0.3 0.3];
ncolors = 15;
clear ax
ax(1) = subplot(3,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.ac,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
% dynamicDateTicks 
% datetick
colorbar
title('Along-canyon Velocity')

ax(2) = subplot(3,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.xc,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
colorbar
title('Cross-canyon velocity')

linkaxes(ax,'xy')
% dynamicDateTicks(ax,'link','keeplimits')
% set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);









