% function Vel=Conv_ADCP_mooring_LAJIT();
% function Vel=Conv_ADCP_mooring_LAJIT(ParamsFile);
% Conv_ADCP_mooring.m  -  Convert set of ADCP files to raw data files with
% lots of aux. data
%here ParamsFile is the params setting m-file.. (leave off the '.m'!!).
%
%   with results going into a new set of matlab files.  Each RDI file will
%   yield a corresponding Matlab file.  The leading characters of the
%   matlab files are specified below, and are checked for prior use.
%   The new index file will also use this designation.  Access to these
%   files is made via get_adcp_any.m. --  DPW, May 2008

%loading in set_ADCP_params data.
%clear
% run(ParamsFile);
set_SN12345_LTQ_paramsLAJIT
cd(fileparts(mfilename)) 

% For Alford mooring group, organize in ADCP s/n subfolders ...

mfiles=InP.mfiles;

ADtop = fullfile([InP.Cruise.Dir,'/ADCP/']); % everything is/will be under this.
%ADmfiles= fullfile(InP.Cruise.Dir,'ADCP/mfiles');
%Different for NEMO
% ADmfiles= fullfile('/Users/johnmickett/Cruises_Research/TTIDE/adcp_processing');  %directory to cruise structure on compute');



% Individual moored ADCPs, one for each deployment
ADnms = {['SN' InP.snADCP]};
DeployNo = InP.deployno; % for multi deploys of same S/N

ADsub = ADnms; % if multi deploys, each has own subfolder:
for i=1:length(DeployNo)
    if ~isnan(DeployNo(i))
        ADsub{i} = fullfile(ADnms{i},['Deployment' num2str(DeployNo(i))]);
    end
end

Rnms = {[InP.snADCP]};

% Matlab root name for raw conversion files and index files
Mnms = {['SN' InP.snADCP '_' InP.Cruise.Name]};

% Select deployment to convert to matlab:
[iA,ok] = listdlg('PromptString','Select an ADCP deployment:',...
    'Name','RDI-to-Matlab conversion', 'SelectionMode','single',...
    'ListString',ADsub);
if ~ok
    disp('None chosen, exit')
    return
end
% iA is ADCP/deployment index number
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Specify files and folders %%
RawFld = fullfile(ADtop, ADsub{iA}, 'data_raw'); % raw BBsliced files are here
MatFld = fullfile(ADtop, ADsub{iA}, 'data_mat'); % matlab data files folder
%IndFld = MatFld; % matlab data index folder

RdiDeploy = Rnms{iA};

RawWild = ['SN' InP.snADCP '*']; % to get BBsliced file names via 'dir'
MatRoot = Mnms{iA}; % for root names of matlab files and index


MatIndx = fullfile(MatFld, [MatRoot '_matfiles.mat']);



%ADCPIndx = fullfile(MatFld,['ADCP_' InP.Cruise.Name '_matfiles.mat']); %this is the CRUISE index file used in get_ADCP_any




% load existing index, or initialize new one (THIS is for get_ADCP_any)
clear Cruise Set_params Index PROG
iSet = 1; % single-sequence index only
if exist(MatIndx,'file')
    load(MatIndx)
else
    if ~exist(MatIndx,'file')
        %create the frigging index file!
        cruiseStruct_init_ADCP
        %disp(['Cannot find template index file: ' ADCPIndx])
        %return
        Index(iSet).filename = [];
        Index(iSet).dtnum_beg = [];
        Index(iSet).dtnum_end = [];
        Index(iSet).RDIFile = [];
        Index(iSet).RDIbytes = [];
        PROG = mfiles;
    else
        load(ADCPIndx);
        Index(iSet).filename = [];
        Index(iSet).dtnum_beg = [];
        Index(iSet).dtnum_end = [];
        Index(iSet).RDIFile = [];
        Index(iSet).RDIbytes = [];
        PROG = mfiles;
    end
end



% Gather RDI filenames into cell array (for non-empty files)
RDFs = dir(fullfile(RawFld,RawWild));
RDFnms = []; RDbyts = []; nf = 0;
for i=1:length(RDFs)
    if RDFs(i).bytes>10
        nf = nf+1;
        RDFnms{nf} = RDFs(i).name;
        RDbyts(nf) = RDFs(i).bytes;
    end
end
if nf<1
    disp( ['Exit, NO files found using: ' fullfile(RawFld,RawWild)] )
    return
end

RDcnv = 0*RDbyts; % conversion flag
% Search for new files to be converted, and for those that have grown
RDlist = [];
for i=1:nf
    ia = find( strcmpi(RDFnms{i}, Index(iSet).RDIFile) );
    if isempty(ia)
        RDcnv(i) = 1; % new file
        RDlist{end+1} = [RDFnms{i} ' - new'];
    elseif RDbyts(i) > Index(iSet).RDIbytes(ia)
        RDcnv(i) = -ia; % File is larger than before
        RDlist{end+1} = [RDFnms{i} ' - GREW'];
    end
end

nf = length(find(RDcnv));
if nf<1
    disp(['Exit, no new/updated files found for ' fullfile(RawFld,RawWild)] )
    return
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that processing is as intended %%
disp(['Looking in ' RawFld ','])
disp(['   found ' int2str(nf) ' RDI files:'])
disp('====================================')
disp(RDlist(:))
disp('====================================')
disp('The above-named files will be converted to matlab.')
x = 'y'; %= input('Okay to continue?', 's');
if isempty(x) | ~strcmpi(x(1),'y')
    disp('NOT Continuing!')
    return
end
% Create matlab data directory (may already exist)
if ~exist(MatFld)
    [sta,msg] = mkdir( MatFld );
    if ~sta
        disp(msg);
        return
    end
end
disp(['Resulting matlab files will be saved as (e.g.):'])
disp( fullfile( MatFld, [MatRoot '*nn_00raw.mat'] ) )
disp([' with index = ' MatIndx])
disp(' ')
disp('Hit <enter> to continue:')
pause
x = 'y'; %= input('Ready to start converting files? ', 's');
if isempty(x) | ~strcmpi(x(1),'y')
    disp('Conversion aborted!')
    return
end

for ifn = 1:length(RDcnv)
    if RDcnv(ifn)
        idxn = 0; % new file, add to end of index
        if RDcnv(ifn)<0
            idxn = -RDcnv(ifn); % existing, alter index entry
        end
        RDnm = RDFnms{ifn};
        typ = 2;  %THIS IS SAVING ALL DATA!!
        %         switch RdiDeploy
        %             case {'ORE05','ORR05'}
        %                 typ = 22; % means use amplitude factor 1.015 (for OS75)
        %             otherwise
        %                 typ = 2;
        %         end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% comment below if BT
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% or no.  
        
        %Vel = Get_ADCP_fullSC_BT( fullfile(RawFld,RDnm), -1, typ); %BT 
       Vel = Get_ADCP_fullSC( fullfile(RawFld,RDnm), -1, typ); %NO BT
        
        if isfield(InP,'TimeShift')==1;  %time Shift is a constant shift (e.g. in CRT..Canadian Retarded time, NOT a drift over the timeseries).
            
            Vel.yday=Vel.yday+InP.TimeShift./24;
            Vel.dtnum=Vel.dtnum+InP.TimeShift./24;
            
        end
        
        
        
        
        %checking read-in params vs. those in params file:
        
        if strcmp(Vel.coordXform,'Earth')==1;
            AA=1;
        elseif strcmp(Vel.coordXform,'Beam')==1;
            AA=0;
        elseif strcmp(Vel.coordXform,'Instrument')==1;
            AA=2;
        elseif strcmp(Vel.coordXform,'Ship')==1;
            AA=3;
        end
        
        if InP.set.Ptyp~=AA;
            disp('warning, coordinate xform in params file does not match leader info in data file');
            disp(['setting infile transform to ' Vel.coordXform ' coordinates']);
            %disp('check instrument params script/mat file');
            disp('hit enter to continue');
            pause
            
        end
        
        InP.set.Ptyp=AA;
        
        save(['SN' InP.snADCP '_' InP.MooringID '_params_' InP.Cruise.Name],'InP');

        
        
        %here we load pressure info to replace xducerdepth if this is empty
        %or constant.
        
        
        
        if isempty(Vel)
            disp('No data, skipping')
            continue
        end
        %         % when computer swapped, forgot to set PC clock to UTC:
        %         switch lower( RDnm(1:max(7,length(RDnm))) )
        %             case {'hc07006','hc07007'}
        %                 Vel.dnum = Vel.dnum + (7/24);
        %                 Vel.lPCtimeoff = Vel.lPCtimeoff + (7*3600);
        %                 disp('Added 7/24 to dnum, PDT to UTC correction')
        %         end
        ydb = Vel.dtnum(1); yde = Vel.dtnum(end);
        
        %return
        %% Find subset that these data belong to (yearday range)
        iSet = 0;
        for i=1:length(Set_params)
            if Set_params(i).start_dtnum <= ydb & Set_params(i).end_dtnum > ydb
                iSet = i;
                break
            end
        end
        if ~iSet
            disp(['Start dnum=' num2str(ydb) ...
                ' is outside the bounds of Grid Index - skipping!'])
            continue
        end
        
        
        %% Make filename
        ia = length(RdiDeploy)+11;
        MTfn = [MatRoot RDnm(ia:end) 'raw.mat'];
        ib = findstr(MTfn, '.');
        if length(ib)>1
            MTfn(ib(1:end-1)) = '_';
        end
        %% Save data in matlab file, update index
        disp(['  Saving as ' fullfile(MatFld, MTfn)])
        
        Vel.mfiles=mfiles;
        
        
        
        save(fullfile(MatFld, MTfn), 'Vel')
        % index
        if ~idxn
            idxn = length(Index(iSet).dtnum_end) + 1; % new entry
        end
        Index(iSet).filename{idxn} = MTfn;
        Index(iSet).dtnum_beg(idxn) = ydb;
        Index(iSet).dtnum_end(idxn) = yde;
        Index(iSet).RDIFile{idxn} = RDnm;
        Index(iSet).RDIbytes(idxn) = RDbyts(ifn);
        save(MatIndx, 'Cruise','Set_params','Index','PROG')
    end
end


if iSet~=0;
% Sort by starting yearday
[d,iOr] = sort(Index(iSet).dtnum_beg);
Index(iSet).filename = Index(iSet).filename(iOr);
Index(iSet).dtnum_beg = Index(iSet).dtnum_beg(iOr);
Index(iSet).dtnum_end = Index(iSet).dtnum_end(iOr);
Index(iSet).RDIFile = Index(iSet).RDIFile(iOr);
Index(iSet).RDIbytes = Index(iSet).RDIbytes(iOr);
end


save(MatIndx, 'Cruise','Set_params','Index','PROG')

disp('All finished writing raw output.');


%reading data from all raw data files:


%once we've processed all of the raw files then
%we

%combine files if necessary after averaging,
%correct for declination
%grid onto a standard depth grid
%parse out bad data
%save into an informed directory structure.

%THEN combine ADCPs into one file if wanted.


