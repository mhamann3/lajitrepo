%script read to get ADCP deployment parameters prior to processing.
%this is intended for use for self-contained (SC) ADCPs on moorings.
%jmickett 6/30/09

%CRUISE SPECIFIC
InP.Cruise.Name='LAJIT2014'; %cruise short name, e.g. MC09, PAPA 08, this should be the same as the directory name
InP.Cruise.Dir='/Volumes/DataDrive1/Projects/LaJIT/moorings/LTQ';  %directory to cruise structure on computer
%within this directory the raw ADCP files should be in 'ADCP/SN10010/data_raw
  InP.Cruise.StartTime='5-Dec-2014 18:00:00'; %start before ADCP logging, end after mooring out of water.
  InP.Cruise.EndTime='8-Dec-2014 12:00:00'; %time in UTC
%  InP.Cruise.StartYear=2010;

 InP.Cruise.StartYear=2014;


%MOORING SPECIFIC
InP.MooringID='LTQ';  %enter mooring name here. This is used for info only
InP.Lat=32+51./60+23./3600;
InP.Lon=117+16./60+4.7./3600; %negative for west longitude

%[dev,d,h,i,f,x,y,z]=magdev(InP.Lat,InP.Lon);

InP.MagDec=12.90;  %east decliniation is positive!

InP.TimeOffset=0; %time offset in seconds at end of deployment, positive is ADCP ahead of GMT.
InP.TimeShift=0;%this is a constant offset for the entire duration of the deployment in HOURS (positive is HOURS BEHIND GMT).

%get this at http://www.ngdc.noaa.gov/geomagmodels/struts/calcDeclination or from a chart.
                        %positive is EAST declination.
InP.NomBotDepth=90; %confirmed using echo intensity of 4 beams, raw data. JM 1/25/15
InP.NomInstDepth=90; %35

%INSTRUMENT SPECIFIC
InP.snADCP='12345'; %instrument serial number---this could come from the filename.
InP.deployno=NaN; %NaN for single deployments, 1 or more for multiple
                    %deployments of the same instrument (even if on
                    %different moorings).
                    
InP.ZGrid=[0:8:92]';  %in true depth coordinates
                    
%InP.set.theta_o=20; %Angle of the ADCP beams from vertical
%InP.set.Cnvx = 1; % convex config, 0 is concave
InP.set.UpDown = 0; % down facing = 1, else up facing  %this will be checked vs. ADCP header info.

%funky stuff here because internal pressure sensor is reading 900-something
%m, should be on average around 1713.  Unfortunately we fried the pressure sensor on the SBE37 
%that was placed right above this ADCP.

% load('/Users/johnmickett/Cruises_Research/TTIDE/moorings/T2/ADCP/SN12819/data_mat/SN12819_TTIDEraw.mat');
% 
% PRFix=[Vel.dtnum;Vel.depth_xducer+793];

dtenew=[datenum(2014,12,5,0,0,0):0.5:datenum(2014,12,9,0,0,0)];

PRFix=[dtenew;dtenew.*0+110];

InP.set.Psrc = PRFix; % pressure source: 1=ADCP internal, OR 2xN vector=[dtnum; dbar] for interp1
%InP.set.Psrc=1;

InP.set.Ztyp = 1; % Type of output depth grid: surface rel = 1; ADCP rel = 0; 
InP.set.Ptyp = 0; % beam coords and all pings = 0, earth coords (ensembles) = 1;
InP.set.Sal0 = -1; % <0=use recorded soundspeed, >0(eg 35)= use this salinity for soundspeed calc
   


% data culling/quality control settings
                     
InP.qc.Xblank=7; % blanks out Xblank percent of the instrument depth (uplooking) or 
                    %height above the bottom (downlooking) near the surface and bottom respectively. 
                    %default is 10 (percent).

InP.qc.WC_val = 10;  %this is the Low Correlation threshold to be used for data in beam coords. (RDI default is 64).
InP.qc.werrMax=1; %culls data above this error threshold (default is 1);
InP.qc.stdMax=4; %culls data greater than X standard deviations (2 is good) 

InP.qc.velMax=2.00; %culls data greater than x 
InP.qc.diffMax=1;  %culls data in adjacent bins (in time) with abs differences greater than this (does forward
%and back to get spikes.



InP.AVG.flag='Y';
InP.AVG.int=5; %averaging interval in minutes when AVG.flag='Y';
InP.AVG.TimeGridInt=5; %in minutes.

%%%BELOW FLAGS for PRESSURE-averaging.  If pressure recorded every ping, it
%%%is noisy, this needs to be knocked down to properly map bins if using
%%%ADCP pressure as reference. 

InP.PAVG.flag='Y';
InP.PAVG.int=5; %averaging interval in minutes when AVG.flag='Y';





InP.mfiles={['set_' InP.snADCP '_' InP.MooringID '_paramsLAJIT.m'];'cruiseStruct_init_ADCP';'Conv_ADCP_mooring_LAJIT.m';'Get_ADCP_fullSC.m';'raw2mat_SCv3.m'};  %list of mfiles used


save(['SN' InP.snADCP '_' InP.MooringID '_params_' InP.Cruise.Name],'InP');


%InP.ZGrid % this should be calculated rather than set...using the bin
%size, ADCP depth, etc.