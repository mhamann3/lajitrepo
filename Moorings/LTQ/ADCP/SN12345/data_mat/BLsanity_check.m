setup_LAJIT2014
%cruise name
name='SP1411';

%go to data drive
% cd /Volumes/student/LaJIT/ADCP_uw
cd /Volumes/DataDrive1/Projects/LaJIT/SIO219/ADCP_uw

data = load_getmat('allbins_','all');

% DO apply the editing mask
u_full = data.u .* data.nanmask;
v_full = data.v .* data.nanmask;

%add base year to get full time (samples every 2 minutes)
time=datenum(2014,1,1)+data.dday;

%convert x longitude 0-360 to -180-180
lon=data.lon-360;
lat=data.lat;
zlev=data.depth(:,1);
%calculate vertical mean velocity and remove to get 'baroclinic' components

%indices for station 1 and station 2
ist1=285:1033;
ist2=1053:1810;

%set u_ship NaNs to zero
data.uship(isnan(data.uship))=0;
data.vship(isnan(data.vship))=0;

%plot u measured - u ship
for i=1:length(zlev)
    uship(i,:)=data.uship;
    vship(i,:)=data.vship;
end


%rotate velocities to canyon coordinates
rot=0.7077; %angle to be rotated in radians- positive is counterclockwise rotation

% rotate vectors to new coordinates
vel_ac = data.u.*cos(rot)-data.v.*sin(rot); %ac= along canyon
vel_xc = data.u.*sin(rot)+data.v.*cos(rot); %xc= across canyon

%anomaly from depth mean
ac_zavg = nanmean(vel_ac,1);
xc_zavg = nanmean(vel_xc,1);

for i=1:length(zlev)
ac_zanom(i,:) = vel_ac(i,:)-ac_zavg;
xc_zanom(i,:) = vel_xc(i,:)-xc_zavg;
end

load data_processed 

%rotate velocities to canyon coordinates
rot=0.7077; %angle to be rotated in radians- positive is counterclockwise rotation

% rotate vectors to new coordinates
Vel.ac = Vel.u.*cos(rot)-Vel.v.*sin(rot); %ac= along canyon
Vel.xc = Vel.u.*sin(rot)+Vel.v.*cos(rot); %xc= across canyon

figure(3); clf
clim = [-0.3 0.3];
ncolors = 15;
clear ax
ax(1) = subplot(2,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.ac,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
% dynamicDateTicks 
% datetick
colorbar
title('Bottom Lander Along-canyon Velocity')


ax(2) = subplot(2,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(time,zlev,vel_ac,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
colorbar
title('Shipboard Along-canyon velocity')

linkaxes(ax,'xy')
% dynamicDateTicks(ax,'link','keeplimits')
% set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);

bdr_savefig(fullfile(rec.prpath,'ADCP'),['ac_vel_compare'],'p',300,'fontsize',11)


figure(4); clf
clim = [-0.3 0.3];
ncolors = 15;
clear ax
ax(1) = subplot(2,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.xc,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
% dynamicDateTicks 
% datetick
colorbar
title('Bottom Lander Across-canyon Velocity')


ax(2) = subplot(2,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(time,zlev,vel_xc,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
colorbar
title('Shipboard Across-canyon velocity')

linkaxes(ax,'xy')

bdr_savefig(fullfile(rec.prpath,'ADCP'),['xc_vel_compare'],'p',300,'fontsize',11)


%% Try it for station 1 with isopycnal contours now

load(fullfile(rec.matfiles,'chi_stations.mat'))
isovar = 'theta1';

figure(5); clf
clim = [-0.3 0.3];
ncolors = 15;
ax(1) = subplot(2,1,1);
id = find(time>=sta1.avg.datevec(1) & time<=sta1.avg.datevec(end));
contourf(time(id),zlev,vel_ac(:,id),15,'EdgeColor','none'); hold on
contour(sta1.ctd.datevec,sta1.ctd.Pvec,eval(['sta1.ctd_grid.',isovar]),10,'k','Linewidth',2)
colormap('redblue'); caxis(clim); c = colorbar; 
ylabel(c,'Station 1- Shipboard Along-Canyon Velocity, m/s');
axis ij
set(ax(1),'Xlim',[sta1.ctd.datevec(1),sta1.ctd.datevec(end)],'Ylim',[0,170])
datetick('keeplimits')


ax(2) = subplot(2,1,2);
load WWctd.mat
id = find(Vel.dtnum>=sta1.avg.datevec(1) & Vel.dtnum<=sta1.avg.datevec(end));
ezcf(Vel.dtnum(id),Vel.z,Vel.ac(:,id),clim,ncolors); hold on
id = find(ww.std_profiles.time>=sta1.avg.datevec(1) & ww.std_profiles.time<=sta1.avg.datevec(end));
contour(ww.std_profiles.time,ww.std_profiles.P,ww.std_profiles.rho,10,'k','Linewidth',2)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim); c = colorbar;
set(ax(2),'Xlim',[sta1.ctd.datevec(1),sta1.ctd.datevec(end)],'Ylim',[0,170])
datetick('keeplimits')
% dynamicDateTicks 
% datetick
ylabel(c,'Station 1- Bottom-Lander Along-Canyon Velocity, m/s');

bdr_savefig(fullfile(rec.prpath,'ADCP'),['station1_vel_compare'],'p',300,'fontsize',11)


figure(6); clf
clim = [-0.3 0.3];
ncolors = 15;
ax(1) = subplot(2,1,1);
id = find(time>=sta2.avg.datevec(1) & time<=sta2.avg.datevec(end));
contourf(time(id),zlev,vel_ac(:,id),15,'EdgeColor','none'); hold on
contour(sta2.ctd.datevec,sta2.ctd.Pvec,eval(['sta2.ctd_grid.',isovar]),10,'k','Linewidth',2)
colormap('redblue'); caxis(clim); c = colorbar; 
ylabel(c,'Station 1- Shipboard Along-Canyon Velocity, m/s');
axis ij
set(ax(1),'Xlim',[sta2.ctd.datevec(1),sta2.ctd.datevec(end)],'Ylim',[0,170])
datetick('keeplimits')


ax(2) = subplot(2,1,2);
load WWctd.mat
id = find(Vel.dtnum>=sta2.avg.datevec(1) & Vel.dtnum<=sta2.avg.datevec(end));
ezcf(Vel.dtnum(id),Vel.z,Vel.ac(:,id),clim,ncolors); hold on
id = find(ww.std_profiles.time>=sta2.avg.datevec(1) & ww.std_profiles.time<=sta2.avg.datevec(end));
contour(ww.std_profiles.time,ww.std_profiles.P,ww.std_profiles.rho,10,'k','Linewidth',2)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim); c = colorbar;
set(ax(2),'Xlim',[sta2.ctd.datevec(1),sta2.ctd.datevec(end)],'Ylim',[0,170])
datetick('keeplimits')
% dynamicDateTicks 
% datetick
ylabel(c,'Station 1- Bottom-Lander Along-Canyon Velocity, m/s');

bdr_savefig(fullfile(rec.prpath,'ADCP'),['station2_vel_compare'],'p',300,'fontsize',11)

%% Put shipboard ADCP into 8m bins as well

for ii=1:size(vel_ac,2)
[vel_acbin(:,ii),vel_acstd(:,ii),nbin]=binavg(zlev,vel_ac(:,ii),Vel.z);
[vel_xcbin(:,ii),vel_xcstd(:,ii),nbin]=binavg(zlev,vel_xc(:,ii),Vel.z);
end

figure(6); clf
clim = [-0.3 0.3];
ncolors = 15;
clear ax
ax(1) = subplot(2,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.ac,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
% dynamicDateTicks 
% datetick
colorbar
title('Bottom Lander Along-canyon Velocity')


ax(2) = subplot(2,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(time,Vel.z,vel_acbin,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
colorbar
title('Shipboard Along-canyon velocity')

linkaxes(ax,'xy')
% dynamicDateTicks(ax,'link','keeplimits')
% set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);

bdr_savefig(fullfile(rec.prpath,'ADCP'),['ac_bin_vel_compare'],'p',300,'fontsize',11)




figure(7); clf
clim = [-0.3 0.3];
ncolors = 15;
clear ax
ax(1) = subplot(2,1,1);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(Vel.dtnum,Vel.z,Vel.xc,clim,ncolors)
% pcolor(Vel.dtnum,Vel.z,Vel.u)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
% dynamicDateTicks 
% datetick
colorbar
title('Bottom Lander Across-canyon Velocity')


ax(2) = subplot(2,1,2);
% ezcf(Vel.dtnum-Vel.dtnum(1),Vel.z,Vel.u,clim,ncolors)
ezcf(time,Vel.z,vel_xcbin,clim,ncolors)
shading flat
axis ij
colormap(redblue)
caxis(clim)
set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);
datetick('keeplimits')
colorbar
title('Shipboard Across-canyon velocity')

linkaxes(ax,'xy')
% dynamicDateTicks(ax,'link','keeplimits')
% set(gca,'Xlim',[min(Vel.dtnum) max(Vel.dtnum)]);

bdr_savefig(fullfile(rec.prpath,'ADCP'),['xc_bin_vel_compare'],'p',300,'fontsize',11)




