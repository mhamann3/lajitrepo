load SN12345_LAJIT2014raw   %Imports structure "Vel" from raw datafile
rawVel = Vel;

figure(10)
clim = [-0.3 0.3];

ax(1) = subplot(6,1,1);
pcolor(rawVel.v1)
shading flat
colormap(redblue)
caxis(clim)
axis tight
% colorbar
title('v3')

ax(2) = subplot(6,1,2);
pcolor(rawVel.v2)
shading flat
colormap(redblue)
caxis(clim)
axis tight
% colorbar
title('v2')

ax(3) = subplot(6,1,3);
pcolor(rawVel.v3)
shading flat
colormap(redblue)
caxis(clim)
axis tight
% colorbar
title('v3')

ax(4) = subplot(6,1,4);
pcolor(rawVel.v4)
shading flat
colormap(redblue)
caxis(clim)
axis tight
% colorbar
title('v4')


ax(5) = subplot(6,1,5);
plot(rawVel.pitch)
title('pitch')

ax(6) = subplot(6,1,6);
plot(rawVel.roll)
title('roll')

linkaxes(ax,'x')



