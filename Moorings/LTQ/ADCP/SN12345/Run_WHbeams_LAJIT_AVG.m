function Vel = Run_WHbeams_LAJIT_AVG(dtnum_beg, dtnum_end, WHtyp, Params)
% ADP = Run_WHbeams_mc09(dtnum_beg, dtnum_end, WHtyp, Params);
%   Converts ping beam data to velocities in geomagmetic frame,
%       returned in structure ADP .
%   Inputs:
%      dtnum_beg,dtnum_end = yearday range to process;
%      WHtyp = root name of matlab ADCP type (SN4021));
%      Params = structure with optional arguments, in fields:
%       'MatFld','MatIndx','WC_val','Psrc','Ztyp','ZGrid','Sal0','MagDec'
%          MatFld = name of directory that contains folders
%             for matlab data and matlab index (default = parent of pwd);
%          WC_val = lower correlation threshold for good beam data (def=30)
%           StartYear= starting year of deployment
%          NomDepth= nominal bottom depth at mooring location.


%here's where we can do some sort of for loop to average chunks of
%data...but would this be best to do in get_ADCP_Any? Must be sure to
%average onto a constant time grid that we overlap sections to avoid averaging problems near
%the ends of data chunks

%taking ten-minute boxcar averages of data and sub-sampling at 5 min
%intervals to avoid aliasing.


%load([WHtyp '_T2_params_TTIDE.mat']);

load([WHtyp '_LTQ_params_LAJIT2014.mat']);

%load([WHtyp '_' MooringID '_params_TTIDE.mat']);

%load('SN11181_params_PAPA09.mat');

% InP = 
%           Cruise: [1x1 struct]
%        MooringID: 'PAPA ADCP'
%              Lat: 50.114666666666665
%              Lon: -1.449710000000000e+02
%           MagDec: 17.433333333333334
%      NomBotDepth: 4235
%     NomInstDepth: 803
%           snADCP: '11181'
%         deployno: NaN
%              set: [1x1 struct]
%               qc: [1x1 struct]
%              AVG: [1x1 struct]
CrName=InP.Cruise.Name;
StartYear=InP.Cruise.StartYear;
MagDec=InP.MagDec;

TmeOffT=InP.TimeOffset;  %%time offset in seconds at end of deployment, positive is ADCP ahead of GMT.
TgridInt=InP.AVG.TimeGridInt;

MooringID=InP.MooringID;
Lat=InP.Lat;
Lon=InP.Lon;
NomDepth=InP.NomBotDepth;


%seconds offset (positive when ADCP is ahead of GMT); %this is OVER the entire timeseries!!
%above offset will be scaled by the size of the subset.

Vel=[];
%breaking total into intervals of 1 day if longer than this.
%%%below not necessary for Papa sampling


TMEgrid=[floor(dtnum_beg.*24)./24:TgridInt./1440:ceil(dtnum_end.*24)./24];

depthvec=[];

InTT=dtnum_end-dtnum_beg;
OVRLAP=(InP.AVG.int.*4)./1440; %overlapping so we can average and then join segments without edge problems



if InTT>1;  %if greater than a day break down interval into day-long segments for averaging
    IsLN=ceil(InTT); %number of sub-divisions
    for jj=1:IsLN;
        jj
        
        if jj==1; %if first sub-division use dtnum_beg
            dtnum_begS=dtnum_beg+(jj-1);
        else
            dtnum_begS=dtnum_beg+(jj-1-OVRLAP);
        end
        
        if jj==IsLN; %if last sub-division use dtnum_end
            dtnum_endS=dtnum_end;
        else
            dtnum_endS=dtnum_beg+jj+(OVRLAP);  %if not last sub-division increment by one day.
        end
        
        ADP = [];
        % WHtyp must be one of the strings in cell array Mnms:
        Mnms = WHtyp;
%         iA = strmatch(WHtyp, Mnms, 'exact');
%         if isempty(iA)
%             disp(['WHtyp = ' WHtyp ' is not one of the valid choices, exit.'])
%             return
%         end
%         
        
        % For Alford mooring group, organized in ADCP s/n subfolders ...
        CrName = InP.Cruise.Name;
        ADtop = [InP.Cruise.Dir '/ADCP/']; % everything is/will be under this.
        % Individual moored ADCPs, one for each deployment
        ADnms = Mnms;
        
        DeployNo = [NaN, NaN, NaN, NaN, NaN, NaN, NaN]; % for multi deploys of same S/N
        
        
        ADsub = ADnms; % if multi deploys, each has own subfolder:
        for i=1:length(DeployNo)
            if ~isnan(DeployNo(i))
                ADsub{i} = fullfile(ADnms{i},['Deployment' num2str(DeployNo(i))]);
            end
        end
        %
        % Now, define default matlab folder,index ...
        MatFld = fullfile(ADtop, ADsub,'data_mat'); % matlab data files folder
       
        IndFld = MatFld; % matlab data index folder
        %MatRoot = Mnms{iA}; % for root names of matlab files and index
        MatRoot = ADnms; % for root names of matlab files and index
        
         MatIndx = fullfile(IndFld, [MatRoot '_' CrName '_matfiles.mat']);
%         % and other defaults:
         
         ParamFile=fullfile(pwd,[MatRoot '_' MooringID '_params_' CrName '.mat']);
% %         
        load(ParamFile);
%         
         load(MatIndx);
        
         TmeOff=TmeOffT.*(dtnum_end-dtnum_beg)./(Set_params.end_dtnum-Set_params.start_dtnum); %scaling time offset by subset of time-series.
        
       %verify Set_params.end_dtnum-Set_params.start_dtnum is instrument
       %start to when time offset recorded. 
       
        WC_val = InP.qc.WC_val;
        %
        % Assign values, special considerations for each ADCP WH deployment
        %EnsUP2DN = []; DepUP2DN = NaN; % for synch'd ADCPs (R-C L's EQ ones)
        
        
        
        theta_o=20; %Angle of the ADCP beams from vertical
         Cnvx = 1; % convex config
%         

        UpDown = InP.set.UpDown; % down facing = 1, else up facing
        
        global UpDown
        %UpDown=1;
        
        % Pr/dbar at ADCP:
        Psrc=InP.set.Psrc; % 1=ADCP internal, 2xN vector=[dtnum; dbar] for interp1
        Ztyp=InP.set.Ztyp; % Type of output depth grid: surface rel = 1; ADCP rel = 0;
        Ptyp=InP.set.Ptyp; %beam coords = 0, earth coords=1;
        
        %Ptyp=1;
        
        
        ZGrid = InP.ZGrid;
        Sal0 = InP.set.Sal0; % <0=use recorded soundspeed, >0(eg 35)= use for soundspeed calc
       
        
        if exist('Params', 'var') && isstruct(Params)
            vvs = {'MatFld','MatIndx','WC_val','Psrc','Ztyp','ZGrid','Sal0','MagDec','NomDepth','StartYear'};
            for i=1:length(vvs)
                if isfield(Params, vvs{i}) && ~isempty(Params.(vvs{i}))
                    eval([vvs{i} '=Params.(vvs{i});']);
                end
            end
        end
        
      
        
        
        %
        % Now, get beam velocities for this ADCP
        ADRaw = get_ADCP_Any_dnum(dtnum_begS, dtnum_endS, MatIndx, MatFld, 1);
        %ADRaw = get_ADCP_AnyMOORED(dtnum_begS, dtnum_endS,ParamFile, MatIndx, MatFld, 1);
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %OK< BUT we need to have in earth-coords, too..
        
        
        
        
        if isfield(ADRaw,'btvel_bm') & Ptyp==1; %if bottom tracking is field, and in earth coords.
           ADRaw.u=ADRaw.u-ones(length(ADRaw.z_adcp),1)*ADRaw.btvel_bm(1,:);
           ADRaw.v=ADRaw.v-ones(length(ADRaw.z_adcp),1)*ADRaw.btvel_bm(2,:);
           ADRaw.w=ADRaw.w-ones(length(ADRaw.z_adcp),1)*ADRaw.btvel_bm(3,:);
           ADRaw.bt_error=ADRaw.btvel_bm(4,:);
        end
   
        
        
        if ~isempty(ADRaw);
            sysconfig=ADRaw.sysconfig;
        end
        
        
        if isempty(ADRaw)
            disp(['Run_WHbeams_LAJIT_AVG: Cannot get ' WHtyp ' data, exit'])
            continue
        end
        
        
        % Assign ADCP pressures:
        if size(Psrc,1)==2 & size(Psrc,1)>1 % interpolation array
            ADRaw.depth_xducer=[];
            id = find(ADRaw.dtnum>=Psrc(1,1) & ADRaw.dtnum<=Psrc(1,end));
            if ~isempty(id)
                ADRaw.depth_xducer(id) = interp1(Psrc(1,:),Psrc(2,:),ADRaw.dtnum(id));
            end
        end
        
        %isoo=find(isnan(ADRaw.depth_xducer));
        %ADRaw.depth_xducer(isoo)=0;
        
        %averaging depth_xducer over our averaging interval.
        
        if InP.PAVG.flag=='Y';
        SampInta=nanmean(diff(ADRaw.dtnum)).*86400; %sampling interval in seconds
        AVGINT=InP.PAVG.int; %averaging time in minutes.
        NNBa=ceil((AVGINT.*60)./SampInta); %rounding up...this is the number of samples in the 30-minute average.
        
        BBa=boxcar(NNBa)./nansum(boxcar(NNBa));
        
        depthS=boxcar_smoothNONAN(ADRaw.depth_xducer,NNBa);
        
        
        igg=find(~isnan(depthS));
        
        [oTy,iTy]=unique(ADRaw.dtnum(igg));
        
        depthSI=interp1(ADRaw.dtnum(igg(iTy)),depthS(igg(iTy)),ADRaw.dtnum,'nearest','extrap');
        
        ADRaw.depth_xducer=depthSI;
        
        
        end
        
        
        
        
        
        
        ADRaw.svel_calc = [];
        if Sal0 > 0 % Compute actual soundspeeds
            s0 = ones(size(ADRaw.temp)) * Sal0;
            % comment out improper one (sal,pres in CU,MPa for MCG's)
            %ADRaw.svel_calc = sw_svel(s0/1000, ADRaw.temp, ADRaw.depth_xducer/100);
            ADRaw.svel_calc = sw_svel(s0, ADRaw.temp, ADRaw.depth_xducer);
            clear s0
        end
        %
        % Convert beam data
        if Ztyp > 0 & Ptyp==0 % Full conversion: correct for pitch,roll, and soundspeed,
            %    map to surface-referenced depth grid and geomagnetic frame
            WHbeam_Process
            %ADP=rmfield(ADP,'z_adcp');
            
        else %map to surface-ref'd depth grid and geomagnetic frame.
            WHEarth_Process
            
            
            ....
            
        %else % stg1: no soundspeed correction, ADCP-relative depth bins
        %    WHbeam_Process_stg1
        end
        
        %now we're averaging the ADP data
        
        %Output
        %
        % ADP =
        %            depth: [40x1 double]
        %                u: [40x6481 double]
        %                v: [40x6481 double]
        %                w: [40x6481 double]
        %             werr: [40x6481 double]
        %              ec1: [40x6481 double]
        %              ec2: [40x6481 double]
        %              ec3: [40x6481 double]
        %              ec4: [40x6481 double]
        %             dtnum: [1x6481 double]
        %           ens_no: [1x6481 double]
        %         soundvel: [1x6481 double]
        %        svel_calc: [1x6481 double]
        %     depth_xducer: [1x6481 double]
        %             temp: [1x6481 double]
        %            pitch: [1x6481 double]
        %             roll: [1x6481 double]
        %          heading: [1x6481 double]
        PInt=round(nanmedian(diff(ADP.dtnum.*86400).*10))./10;
        
         %commenting out for the time-being;
% %         
%          %culling data that exceeds werr threshold
%          Isbb=find(abs(ADP.werr>1));
%          ADP.u(Isbb)=NaN;
%          ADP.v(Isbb)=NaN;
%          ADP.w(Isbb)=NaN;
%         
        
        
        MagVel=sqrt(ADP.u.^2+ADP.v.^2);
        MagVelR=reshape(MagVel,1,size(MagVel,1).*size(MagVel,2));
        Bstd=nanstd(MagVelR);
        
        
        %          Ibdd=find(MagVel>=(nanmean(MagVelR)+3.*Bstd));
        %
        %          ADP.u(Ibdd)=NaN;
        %          ADP.v(Ibdd)=NaN;
        %          ADP.w(Ibdd)=NaN;
        %          clear Ibdd
        %
        %       Ibdd=find(MagVel>=0.7);
        %
        %          ADP.u(Ibdd)=NaN;
        %          ADP.v(Ibdd)=NaN;
        %          ADP.w(Ibdd)=NaN;
        
        
        
        ecMM=ones(length(ADP.depth),length(ADP.dtnum),4);
        %averaging echo intensities
        ecMM(:,:,1)=ADP.ec1;
        ecMM(:,:,2)=ADP.ec2;
        ecMM(:,:,3)=ADP.ec3;
        ecMM(:,:,4)=ADP.ec4;
        ecM=nanmean(ecMM,3);
        
        
        %getting rid of data w/in 10% of sampling depth or near bottom
        %taking 10% of depth xducer, removing this from top
    
        
        if UpDown==0; %upward-looking   OK, here it is clear that z_adcp is positive for down, negative for up-looking.
            Xblank=(InP.qc.Xblank./100).*(ADP.depth_xducer);
            Ido=find(ADP.depth*ones(1,length(Xblank))<=ones(length(ADP.depth),1)*Xblank);
            
            ADP.u(Ido)=NaN;
            ADP.v(Ido)=NaN;
            ADP.w(Ido)=NaN;
            
            
        else %downward looking
            XPer=InP.qc.Xblank;
            
            
            %cutting off velocities below bottom and those within X% of bottom
            DiffD=NomDepth-nanmedian(ADP.depth_xducer);
            
            
            BADD=((XPer./100)).*DiffD;
            Ido=find(ADP.depth>(NomDepth-BADD)); 
            
            ADP.u(Ido,:)=NaN;
            ADP.v(Ido,:)=NaN;
            ADP.w(Ido,:)=NaN;
        end
        
 
        
    
        
        
        if ~isempty(ADRaw) & isempty(depthvec);
            depthvec=ADP.depth;
        end
        
        
        
        
        
        %if it is the first day, creating matrices
        if ~exist('uMat')==1 & ~isempty(ADP);
            
            uMat=NaN.*ones(length(ADP.depth),length(TMEgrid));
            vMat=uMat;
            wMat=uMat;
            ecMat=uMat;
            xDucerDep=NaN.*ones(1,length(TMEgrid));
            tempM=xDucerDep;
            
        end
        
        %%%%%%%%%%%%%%
        %%%%%%%%%%%%%%
        %%% must have diff't approaches for ours and Jody's ADCPs..
        
       
        
        if InP.AVG.flag=='Y';
        
        SampInt=nanmean(diff(ADP.dtnum)).*86400; %sampling interval in seconds
        AVGINT=InP.AVG.int; %averaging time in minutes.
        
        NNB=ceil((AVGINT.*60)./SampInt); %rounding up...this is the number of samples in the 30-minute average.
        
        BB=boxcar(NNB)./nansum(boxcar(NNB));
        
        
        %averaging variables here
        
                %uAve=boxcar_smoothNONAN(ADP.u,NNB);
        
%                 uAve=conv2(1,BB',ADP.u,'same');
%                 
%                 vAve=conv2(1,BB',ADP.v,'same');
%                 wAve=conv2(1,BB',ADP.w,'same');
%                 ecMAve=conv2(1,BB',ecM,'same');
%                 xducer_depth=conv2(1,BB',ADP.depth_xducer,'same');
%                 tempAve=conv2(1,BB',ADP.temp,'same');
                
                
                
                 uAve=boxcar_smoothNONAN(ADP.u,NNB);
                
                vAve=boxcar_smoothNONAN(ADP.v,NNB);
                wAve=boxcar_smoothNONAN(ADP.w,NNB);
                ecMAve=boxcar_smoothNONAN(ecM,NNB);
                
                
                
                %xducer_depth=boxcar_smoothNONAN(ADP.depth_xducer,NNB);
                %smoothed this earlier
                xducer_depth=ADP.depth_xducer;
                
                tempAve=boxcar_smoothNONAN(ADP.temp,NNB);
        
        %NaNing beginning and ends where averaging causes problems. Being overly
        %conservative here.
                uAve(:,1:NNB)=NaN;
                uAve(:,end-NNB:end)=NaN;
                vAve(:,1:NNB)=NaN;
                vAve(:,end-NNB:end)=NaN;
                wAve(:,1:NNB)=NaN;
                wAve(:,end-NNB:end)=NaN;
                ecMAve(:,1:NNB)=NaN;
                ecMAve(:,end-NNB:end)=NaN;
        
                xducer_depth(end-NNB:end)=NaN;
                xducer_depth(1:NNB)=NaN;
                tempAve(end-NNB:end)=NaN;
                tempAve(1:NNB)=NaN;
                DnumAve=ADP.dtnum;
                
        else
            
            uAve=ADP.u;
            vAve=ADP.v;
            wAve=ADP.w;
            ecMAve=ecM;
            xducer_depth=ADP.depth_xducer;
            tempAve=ADP.temp;
            DnumAve=ADP.dtnum;
        end
        
            
            
            
            
        
        %end
        
        
        Isff=find(TMEgrid>=DnumAve(1) & TMEgrid<=DnumAve(end)); %to save time only interpolating onto a
%         portion of the total time.
%         
%         interpolating onto standard grid.

  [C,Iuu]=unique(DnumAve);
        
        for ii=1:length(ADP.depth);
            %ii=10;
            %u
          
            
            uAveI=interp1(DnumAve(Iuu),uAve(ii,Iuu),TMEgrid(Isff),'linear'); 
            iso=find(~isnan(uAveI));
            uMat(ii,Isff(iso))=uAveI(iso);
            %v
            vAveI=interp1(DnumAve(Iuu),vAve(ii,Iuu),TMEgrid(Isff),'linear');
            iso=find(~isnan(vAveI));
            vMat(ii,Isff(iso))=vAveI(iso);
            %w
            wAveI=interp1(DnumAve(Iuu),wAve(ii,Iuu),TMEgrid(Isff),'linear'); 
            iso=find(~isnan(wAveI));
            wMat(ii,Isff(iso))=wAveI(iso);
            %ec
            ecAveI=interp1(DnumAve(Iuu),ecMAve(ii,Iuu),TMEgrid(Isff),'linear'); 
            iso=find(~isnan(ecAveI));
            ecMat(ii,Isff(iso))=ecAveI(iso);
            
        end
        
        xducerDepthI=interp1(DnumAve(Iuu),xducer_depth(Iuu),TMEgrid(Isff),'linear'); 
        
        iso=find(~isnan(xducerDepthI));
        
        xDucerDep(Isff(iso))=xducerDepthI(iso);
        
        
        tempAveI=interp1(DnumAve(Iuu),tempAve(Iuu),TMEgrid(Isff),'linear'); 
          
        iso=find(~isnan(tempAveI));
        
        tempM(Isff(iso))=tempAveI(iso);
        
        
        
        %putting pieces back together, remembering we overlapped by 20 minutes, so just
        %adding unique time values.
        
        
        
        
    end
end

%finding all nans at beginning or end of timeseries, removing from
%timeseries

%this gets all nans, just removing beginning and end sections
Igood=find(~isnan(nanmean(uMat)));

if ~isempty(Igood);
    IgdS=Igood(1);
    IgdE=Igood(end);
else
    disp('No good data within range specified');
    return
end

%now applying time offset over entire timeseries..
%TmeOff

TmeOffL=linspace(0,TmeOff./86400,length(TMEgrid)); %linear offset fix.

TMEgridn=TMEgrid+TmeOffL;


%if LR4 then create a vector of lat and lon AND of nominal water depth.
%with depth of Xducer 11 m above the bottom.

Vel.u=uMat(:,IgdS:IgdE);
Vel.v=vMat(:,IgdS:IgdE);
Vel.w=wMat(:,IgdS:IgdE);
Vel.ecAve=ecMat(:,IgdS:IgdE);
Vel.dtnum=TMEgridn(IgdS:IgdE);
ydaya=datenum2yday(TMEgridn(IgdS));
Vel.yday=ydaya+[Vel.dtnum-Vel.dtnum(1)];

%Vel.datenum=yday2datenum(TMEgrid(IgdS:IgdE),StartYear);
Vel.z=depthvec;
Vel.temp=tempM(IgdS:IgdE);
Vel.xducer_depth=xDucerDep(IgdS:IgdE);
Vel.botdepth=InP.NomBotDepth;
Vel.lat=Lat;
Vel.lon=Lon;

Vel.MagDecUsed=MagDec;

Vel.info.snADCP=WHtyp(1:end);
Vel.info.MooringID=MooringID;
Vel.info.Cruise=CrName;
Vel.info.startyear=StartYear;
Vel.info.timeOffset=TmeOff;
Vel.info.timeOffset_notes='applied. Seconds that ADCP was ahead of GMT--linearly applied over timeseries';

Vel.info.sysconfig=sysconfig;
%Vel.info.params=ADRaw.params;
Vel.info.NomInstDepth=InP.NomInstDepth;
Vel.info.mfiles=['in order: Conv_ADCP_mooring_LAJIT (coverts to mat files)'...
    'Run_WHbeams_LAJIT_AVG.m (conv. to earth coord +avgs. & calls get_ADCP_Any_dnum.m, WHbeam_Process.m, etc.)'];
Vel.info.processing=['10 1-s water pings & 10 BT pings avg. into  ' num2str(TgridInt) ' minute ensembles.'];
if exist('Notes')==1;
    Vel.info.notes=Notes;
end

%          Cruise: 'PAPA08'
%        MooringID: 'PAPA ADCP'
%           MagDec: 17.666666666666668
%           snADCP: {2x1 cell}
%     NomInstDepth: {2x1 cell}
%        sysconfig: [1x1 struct]
%           mfiles: 'merger_moored_ADCPs.m'





