% 2/17/15 MMH

% Shift the data for version 3 of the processed bottom lander data
% Be sure that the data in the directory is from v3
setup_LAJIT2014

load data_processed 
cd(fullfile(rec.topfolder,'/Moorings/LTQ/ADCP/SN12345/data_mat'))
% save('data_processedOG','Vel');

Vel.u(4:5,:) = 0;
Vel.v(4:5,:) = 0;
Vel.w(4:5,:) = 0;
Vel.z = Vel.z-8;
Vel.u(:,549:588) = 0; % Garbage data from ADCP pitching
Vel.v(:,549:588) = 0;
Vel.w(:,549:588) = 0;

save('data_processed','Vel');


