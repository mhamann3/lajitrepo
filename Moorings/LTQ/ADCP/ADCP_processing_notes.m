%first edit set_(sn)_params(cruisename).m  This sets the specifics for the
%ADCP settings, with many entries verified against the ADCP settings in
%header file.  The start dtnum should be sometime BEFORE the start of the
%ADCP file.  This a relict of Dave's SWIMS coding.

%EDIT Conv_ADCP_mooring_(CruiseName)  edit where this calls set_(sn)_params(cruisename).m
%and update list of m_files (just for recordkeeping purposes).  Edit
%through lines 22.   

%Run it OUTSIDE of data_raw directory, or it will get confused and try to
%open the params file as a data file..


%this program calls Get_ADCP_fullSC_BT, or Get_ADCP_fullSC, which calls the
%program that does all the parsing: raw2mat_SCv3_BT

%Conv_ADCP_mooring will spit out a Vel structure of all the raw data---it
%won't be binned in depth, no mag dec. correction, no rotation if in beam
%coordinates... BUT, it saves everything.

%data files should be SN"snhere"_cruisename_000.000  or whatever for file
%sequence.





