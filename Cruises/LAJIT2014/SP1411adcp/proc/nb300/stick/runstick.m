% Usage:  1) edit the file runstick.m to specify the input parameters
%         2) in Matlab, type 'runstick'
%
% Input:  *_uv.mat and *_xy.mat output from adcpsect
% Output: PostScript file (.ps)
%
% runstick generates one or more of the following types of plots:
%
%   1) Velocity;
%   2) Mean, trend, tides, inertial;
%   3) Residual;
%   4) Inertial clockwise (cw) and counterclockwise (ccw);
%   5) Diurnal cw and ccw;
%   6) Semidiurnal cw and ccw;
%   7) Harmonic analysis of velocity
%      a) mean and trend, and
%      b) ellipse, phase and direction of diurnal, semidiurnal and inertial.
%
% where the y-axis is always depth (in meters) and the x-axis is:
%
%   1) time, or longitude, or latitude for plot types 1 thru 6
%   2) labeled 'mean+trend', 'semidirunal', 'diurnal', 'inertial' for type 7
%      (Note:  The user must edit the Matlab version 3.5 PostScript output file
%              before printing in order to delete the numerical labels on the
%              x-axis; Matlab v. 4 has a facility for suppressing these.)
%
% The period for semidiurnal = 12.42 hours
%                diurnal     = 24 hours
%                inertial    = 12/sin(lat) hours
%                              e.g., if lat = 22.75 deg, inertial = 31 hours
%
% For each type of plot to be generated, the user must specify one or
% more of the following parameters:
%
%   1) xyrange     = [ xmin xmax ymin ymax ]
%                     x- and y-axis range of the plot.
%   2) vscale      = velocity units per inch
%                    e.g. (m/s) per paper inch
%   3) key         = [ x y v ]
%                    where x and y are location of key in axis units
%                    and v is length of key in velocity units.
%   4) arrowlength = length of arrowhead in velocity units
%                    Note: set arrowlength = 0 to omit arrowheads;
%                          this greatly reduces memory requirements.
%

% 91/05/30 Willa Zhu

% >>>>>>>>>>>>>>>>>>>> user must edit the following lines >>>>>>>>>>>>>>>>>>>>

                    %---------------------------------------------------------
infile = 'os'      ;% root of input Mat-files (_uv.mat & _xy.mat output of adcpsect)
cruise = 'ADCP DEMO'% cruise id (appears in upper righthand corner of plot)
year   = 1993;      % base year for decimal days
outfile= 'ademo_os';% root of plot output file; set to [] to disable PostScript output
append = 0;         % 0 = overwrite outfile if it exists; 1 = append
pause_flag = 1;     % 1 = pause between screenfuls of plots; 0 = don't

                    %---------------------------------------------------------
x_index = 3;        % 1 = longitude on x-axis
                    % 2 = latitude on x-axis
                    % 3 = time on x-axis

if x_index == 3     %--------------------------------------------------------
  harmonic  = 4;    % 0 = don't plot harmonic analysis of velocity
                    % 1 = plot mean and trend at end time
                    % 2 = 1 + ellipse, phase & direction for semidiurnal
                    % 3 = 2 + ellipse, phase & direction for diurnal
                    % 4 = 3 + ellipse, phase & direction for inertial
  plot_title = '';  % optional string to be concatentated to standard plot titles

elseif x_index == 1 | x_index == 2      % specify plot title
  plot_title = '158W Southbound';
end

                    %---------------------------------------------------------
                    % select types of plots to generate:  1 = plot
                    %                                     0 = don't plot
velocity    = 1;    % velocity stick plot
mean_trend  = 1;    % mean, trend, tides, inertial stick plot
residual    = 1;    % residual stick plot
semidiurnal = 1;    % semidiurnal stick plot
diurnal     = 1;    % diurnal stick plot
inertial    = 1;    % inertial stick plot
min_inertial = 2.7; % Don't calculate inertial component if
                    %     time series is shorter than this (length in days).

xyrange = [ 98.0   99.5  -300 0];  % [ x_min x_max y_min y_max ]
vscale  = 1;                       % velocity units per paper inch
arrowlength = 0.025;               % arrowhead length in velocity units
key = [ 99.2 -10 0.1];             % key position/scale = [x y v]
latitude = 13                     % latitude in degrees for inertial; 13 for demo
                                  %------------------------------------------
inertial_period = 0.5/sin(abs(latitude)*pi/180)

% <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

runstic2                           % do it
