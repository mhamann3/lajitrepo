%plots percent good on color (or b/w) panels
% this is just a stub to call gplotpg.  do 'help gplotpg' to look at it
                                                       
%------------------------------edit this section --------------------

  start_dd                =       98;            % start decimal day
  end_dd                  =       100;           % end decimal day
  pgstruct.year_base      =       1993;          % year base
  pgstruct.comment        =       'ADCP demo';   % title
  pgstruct.dbpath         =       '../adcpdb/';  % path to database (from here)
  pgstruct.dbname         =       'ademo';       % dataabse name

  
% -------------------- end of section necessary to edit ---------  
  
  %optional fields (and associated defaults)
  pgstruct.printflag      =  1   % print postscript files to disk
                                 % (default is 0 (no))
  pgstruct.nbins          =  60; % maximum number of bins 
                                 % watch it! this is for a particluar dataset
								 % check your configuration to find out what
								 % you should put here
  pgstruct.PGOOD_MIN      =  0;  % mininum  values for clipping data          
  pgstruct.PGOOD_MAX      =  100;% maximum  values for clipping data          
  pgstruct.icolor         =  1;  % if 1, use color, otherwise gray scale      
  pgstruct.reduce_res     =  0;  % yes=1,no=0;50% resolution in time and depth
  pgstruct.interval_dd    =  8;  % number of days to plot/page, default is 8 
  pgstruct.days_per_panel =  2;  % number of days to plot/panel; default is 2
                                 % (4 panels per page is hardwired)  
% -------------------- end of any editing --------------------
  %now run it
  clf
  numplots = ceil((ceil(end_dd) - floor(start_dd))/(pgstruct.interval_dd))

  for plotnum = 1:numplots
	pgstruct.start_dd = floor(start_dd) + (plotnum - 1)*pgstruct.interval_dd;
	gplotpg(pgstruct);
	drawnow
  end
