function pltemp(xy, T, axleft, axright, xlab, the_title)
% function pltemp(xy, T, axleft, axright, xlab, the_title)
%
% Plot an ADCP temperature series as a stacked set of panels.
%  xy is the abcissa, T is the ordinate.
%  axleft is a vector of left-hand abcissa limits.
%  The stack of plots starts with the first axleft value in the top panel.
%  axright can be either a scalar width or a vector of right-hand limits.
%  xlab and the_title are strings for labelling.
%
%  The ordinate autoscales to 1.2 times the range.
%
%  E.F. 95/01/14

print_flag = 0;


np = length(axleft);                       % number of panels;
if length(axright) ~= np,
   axright = axleft + axright(1);
end
axleft  = axleft (np:-1:1);
axright = axright(np:-1:1);

ym = 0.1;                                % top and bottom margin
ysep = 0.03;                              % separation between panels
yp = (1 - 2 * ym - (np-1) * ysep) / np;   % panel height

xm = 0.15;                                 % side margin
xp = 1 - 2 * xm;                          % panel width

figure
orient tall

px0 = xm;
py0 = ym;


for ii = 1:np,
   ir = find(xy >= axleft(ii) & xy <= axright(ii));
   y = T(ir);
   y0 = min(y);
   y1 = max(y);
   yhr = 0.5 * (y1 - y0);   % half the range
   ym = 0.5 * (y1 + y0);   % middle

   axes('position', [px0 py0 xp yp] );
   plot(xy(ir), y, '.');
   axis([axleft(ii) axright(ii) ym-1.2*yhr ym+1.2*yhr]);
   grid
   if (ii == 1),
      xlabel(xlab)
   end
   py0 = py0 + yp + ysep;
end

title(the_title)

date_it('pltemp.m');
if print_flag == 1,
   print -dps2
end

