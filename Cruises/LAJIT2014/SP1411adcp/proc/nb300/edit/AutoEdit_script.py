
#!/usr/bin/env python

## written by quick_adcp.py-- edit as needed:





from pycurrents.adcp.pingedit import CODAS_AutoEditor

# docs

## if the file 'codas_editparams.txt' exists, values
##        present will override defaults from
##        pycurrents.adcp.adcp_specs.codas_editparams
##
## this file can also be used by gautoedit.py to override
##        defaults (same mechanism)



from pycurrents.adcp.adcp_specs import codas_editparams
A = CODAS_AutoEditor(codas_editparams, '../adcpdb/a_sp',
                      editparams_file='codas_editparams.txt',
                      verbose=True)
A.set_beamangle(beamangle = 30) #from ../dbinfo.txt
A.get_data(ddrange=[341.327696759, 341.627696759])
A.write_flags(fformat='a%s_tmp.asc', openstyle = 'w')



