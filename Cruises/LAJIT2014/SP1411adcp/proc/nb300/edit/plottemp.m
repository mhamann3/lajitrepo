function plottemp(infile)
% plottemp plots the ADCP mean and last transducer temperatures vs. time
%
% Input: text file (output of lst_temp program) with following columns:
%           1) profile time (in decimal days)
%           2) mean transducer temperature
%           3) last transducer temperature
%
% Output: PostScript file "plottemp.ps"
% Usage:  In Matlab, in cruiseid/edit directory
%       plottemp
% or...
%       plottemp('a0001.tem')


%late 2000/ eary 2001
% reworked by Craig Althen and Jules Hummon


%find the input name

if (nargin == 0)

   dd = dir('*.tem');
   if (length(dd)>1)
      help plottemp
      error('too many files ending with ".tem" - specify it as an argument ')
   end
   infile = dd.name;
end
%otherwise comes from command line   


if (strcmp(computer, 'PCWIN'))
   ss = '\';
else
   ss = '/';
end


%now get the cruiseid.  We'll assume it is the name of the processing directory
this_dir = fileparts(which(infile));
marks = findstr(this_dir, ss);  % assumes in  "../cruiseID/edit"
cruiseID = upper(this_dir(marks(end-1)+1:marks(end-1)+6));

plot_title = strcat('ADCP -',cruiseID,'  (mean = magenta; last = blk)');
xaxis_label = 'decimal days';
yaxis_label = 'deg. C';

[t, mn_temp, last_temp, snd_spd_used] = read_asc(infile);

% NaN-out missing values (coded as BADFLOAT = 1e38):
mn_temp = tonan(mn_temp);
last_temp = tonan(last_temp);


mintemp = min(min(mn_temp), min(last_temp));
maxtemp = max(max(mn_temp), max(last_temp));
if mintemp == maxtemp
   mintemp = mintemp - .1; maxtemp = maxtemp + .1;
end

% -----------------
clf
orient landscape

subplot(2,1,1);
p1 = plot(t, mn_temp, 'm', t, last_temp, 'k');
set(p1,{'linewidth'},{2; 0.2});
axis([t(1) t(end) mintemp maxtemp])
xlabel('');
ylabel(yaxis_label)
set(gca, 'xticklabel','');
grid
title(plot_title)

subplot(2,1,2);
p2 = plot(t, mn_temp - last_temp, 'ro');
set(p2, 'markersize', 4)
axis([t(1) t(end) -0.2, .2]);
xlabel(xaxis_label)
ylabel('(Mean - Last) Difference')
set(gca, 'ytick',[-.2 -.1 0 .1 .2]);
grid

print -dpsc plottemp.ps

