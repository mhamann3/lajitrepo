% threshld is used to generate a first estimate of threshold settings
% to use for editing the CODAS database based on the W-variance and
% second derivative criteria.
%
% Input:  1) .mat file of profile statistics for W (output of profstat program)
%         2) .mat file of profile statistics for 2nd difference of U, V, and W
%            (output of profstat program)
% Output: threshold values for W variance, and 2nd derivative of U, V, and W
% Usage:  1) Edit this file to specify the input parameters.
%         2) In Matlab, type 'threshld'.

% >>>>>>>>>>>>>>>>>>>> user must edit the following lines >>>>>>>>>>>>>>>>>>>>

Wdiff0_file   = 'ademodf0'; % .mat file with stats for W 
UVWdiff2_file = 'ademodf2'; % .mat file with stats for U, V, W 2nd diff

% <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

eval(['load ' Wdiff0_file ]);
w_var_threshold = (mean(pstat_W(5:20,4)*1000) * 3) ^ 2

eval(['load ' UVWdiff2_file])
d2w_threshold = 2 * (mean(pstat_W(5:20,4)*1000) * 2)
d2uv_threshold = (mean(pstat_U(5:20,4)*1000) + mean(pstat_V(5:20,4)*1000))

