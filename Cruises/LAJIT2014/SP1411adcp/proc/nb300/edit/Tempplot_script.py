
#!/usr/bin/env python

## written by quick_mplplots.py -- edit as needed:

## cruiseid      is 'ADCP'
## dbname        is 'a_sp'
## proc_yearbase is 2014
## printformats   is 'png'

import matplotlib.pyplot as plt




from pycurrents.adcp.quick_mplplots import Tempplot

PT = Tempplot()
PT(temp_filename='a_sp.tem',
   titlestr = 'ADCP: Transducer Temperature',
   proc_yearbase = 2014,
   printformats = 'png',
   ddrange = None)

plt.show()
