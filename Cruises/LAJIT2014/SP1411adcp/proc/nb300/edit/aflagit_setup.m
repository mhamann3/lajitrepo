%This is a setup script to set the structure values used by aflagit.m
% "pfc" is "profile flags config"
% substuctures are:
%     flag (the value of the bad data)
%     cfg  (configuration information)
%
% You either have good data and want to automate the old-fashioned editing, or
% you have lousy data and want to cavalierly eliminate anything of dubious 
% character.   Here is a table with suggested settings for a "good" or "bad" 
% dataset.  The defaults for this file will start out for "good" data so you 
% don't lose too much right at the start.
% 
%
% THIS TEMPLATE IS FOR NEWDAY.PRL (grep on "__" to look for variables)
%
% field of structure "pfc"             good dataset       bad dataset
%                                     (eg. Kaimimoana)    (eg. NBPalmer)
% -----------------------              ------------       -----------
% pfc.refl_startbin                   config.rl_range(1)   config.rl_range(1)
% pfc.refl_endbin                     config.rl_range(2)   config.rl_range(2) 
% pfc.yearbase                        config.yearbase      config.yearbase   
%                             
% pfc.cfg.bigtarget_amp                      25             25
% pfc.cfg.bigtarget_offset                    5              5
% pfc.cfg.bigtarget_avg                       2              2
% pfc.cfg.pgcutoff                           30             50 (data dependent)
% pfc.cfg.maxpgbin_fudge                     -1 (disable)    3 (data dependent)
% pfc.cfg.badpgrefl                          30             50
% pfc.cfg.badpgrefl_nbins             (maybe 70% of the number of bins in the
%                                      reference layer? easier to code it:
%                               round(.7*(pfc.refl_endbin - pfc.refl_startbin))
% pfc.cfg.trimtopbins                         0              1 (data dependent)
% pfc.cfg.amp_cutoff_atbin1                   0            130 (data dependent)
% pfc.cfg.ampdecrperbin                      10             10 (data dependent)
% pfc.cfg.ecutoff                           100            100 (data dependent)
% pfc.cfg.wire_ecutoff                       70             70 (data dependent)
% pfc.cfg.wire_onstation                      2              2 (data dependent)
% pfc.cfg.wire_binrange                  [1 20]          [1 20](data dependent)
% pfc.cfg.jitter_cutoff                       5              5 (data dependent)
% pfc.cfg.orphan_numbins                      0         pfc.cfg.badpgrefl_nbins
% pfc.cfg.orphan_slide                        0              3 (data dependent)

%=============================================================================

% initialize some useful information
pfc.cfg.underway  = 100;        % reject underway profiles (ship speed, m/s)
                                % disable it initially 
                                % May want to reject all underway profiles 
                                %     if ashtech went out (for instance)

                                         
pfc.refl_startbin  = config.rl_range(1);% ref layer should match what was   
pfc.refl_endbin =  config.rl_range(2);  %  used in smoothr (for consistency) 
                                        % (get it from ../nav/as_nav.tmp or
                                        % (  ../nav/adcpsect.cnt)
pfc.yearbase = config.yearbase;         % (for labelling)

%set the config information to impliment the editing


%------------ for binwise glitch filtering over time -----------
% for each bin, flag glitches but only if accelerating
pfc.cfg.agility = 3;        % ship-dependent
pfc.cfg.degperminute = 20;  % really accelerating - true outliers
pfc.cfg.glitchfactor = 4;   % alittle conservative (see "help glitch")
pfc.cfg.topglitchbin = 1;   % use glitch flagging starting at this bin (and deeper)


% ---------- for the bottom ----------
% looking for a high amplitude signal which SHOULD be the bottom.
% these values might have to be changed if we end up flagging 
% scattering layers

pfc.cfg.bigtarget_amp       = 25;   % these came from Eric's ky0006 
pfc.cfg.bigtarget_offset    =  5;   % implimentation - I don't know what
pfc.cfg.bigtarget_avg       =  2;   % might be important to tweak

%---------- percent good ----------
% percent good is used in three ways:
% (1) low percent good: eliminate all bins with pg < pfc.cgf.pgcutoff
% (2) ringing: identified by a near-surface maximum (caused by a decrease 
%       in PG  near the surface, then PG returns to its normal value deeper
%       down).  We locate the shallowest bin where PG attains its maximum
%       and chop of everything shallower than that.  Because PG may hover  
%       near its maximum for several bins, we back off the strict requirement 
%       of "the shallowest bin with the maximum pg" and instead look for
%       "the shallowest bin within pfc.cfg.maxpgbin_fudge of the the maximum"
% (3) reference layer: eliminate profiles if there are too few reference layer
%       bins with sufficiently good PG.


pfc.cfg.pgcutoff = 50;    % (1) low percent good for all bins 
                                 %      (lowest allowable number is 30; 
                                 %       set it higher to be more selective)
pfc.cfg.maxpgbin_fudge = 100;    % (2) ringing (allow this much slop
                                 %     in identifying the "maximum" PG
                                 %     --> set to -1 to disable
                                 %     --> set to 1-3 if ringing
pfc.cfg.badpgrefl = 50;   % (3) reference layer PG cutoff
                                 %     (lowest allowable number is 30;
                                 %       set it higher to be more selective)
pfc.cfg.badpgrefl_nbins = 1;     % make it higher to edit out "weak" (i.e.
                                 %     underrepresented) profiles


% ---------- trim the top ----------
%There are two editing criteria in this section eliminate bad data at the top.
%(1) remove top N bins from all profiles
%(2) remove bins where low amplitude may be artificially inflating
%     signal/noise ratio.  At the moment this is a linear amplitude
%     cutoff with bin.
%


pfc.cfg.trimtopbins = 0;         %exclude top N bins anyway
pfc.cfg.amp_cutoff_atbin1 = 0;   %amplitude cutoff at bin 1 is this 
pfc.cfg.ampdecrperbin = 10;      %relative to bin 1, lose this many amplitude
                                 %counts ber bin in depth

% ---------- error velocity ----------
% error velocity is used in two ways:
% (1) plain old error velocity cutoff
% (2) wire interference: different criterion (?), only on station, remove
%     an additional bin left, right, up, and down from flagged bins
%     (disable by setting cutoff to a large value (eg 10^6))

pfc.cfg.ecutoff      = 120;     % (1) plain old error velocity cutoff
pfc.cfg.wire_ecutoff = 90;      % (2) wire interference cutoff
pfc.cfg.wire_onstation = 2;     %     (only look when speed is less than this)
pfc.cfg.wire_binrange  = [1 20];%     (only look in this bin range)


% ---------- jitter flag ----------
% jitter in measurement as defined by a running std of (sm - refl)
pfc.cfg.jitter_cutoff = 15; %cm/s

% ---------- flag isolated profiles ----------
%
%If the data are mostly bad, we want to eliminate isolated profiles as
%   suspect.  We do this by requiring a profile to have a few good close
%   friends.  NOTE: to disable, must set both of these to zero
%
%

pfc.cfg.orphan_numbins = ...
   pfc.cfg.badpgrefl_nbins ;    % number of unflagged bins in ref layer
                                % required to accept the profile
                                % 
pfc.cfg.orphan_slide = 0;       %half-width of isolation profile
                                % i.e. slide left and right by this,
                                % and accept the center of a run of 3
                                % or middle 3 if a run of 5


%--------------------------------------------
%%% flag values are now set in aflagit.m