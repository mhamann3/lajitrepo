%% this is where you set flagging thresholds to override the defaults in
% aflagit.m if you're running quick_adcp.py with find_pflags set.
%
% For example, to override jitter threshold , and glitch factor,
%      uncomment the lines below
%
%
% pfc.cfg.jitter_cutoff = 15
% pfc.cfg.glitchfactor = 4
