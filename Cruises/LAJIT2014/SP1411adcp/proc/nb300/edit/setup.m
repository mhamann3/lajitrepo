% SETUP.M
% USAGE: SETUP
%
%  SETUP initializes the global variables that will be used in the following
% .M file or function calls: GET.M, INIT.M, OFFSET.M, DRAW.M, BAD.M, LIST.M,
% etc. (see DBEDIT.DOC).  By convention, all global variables are in caps.
%
%%global DBNAME DEFAULT_NPRFS DEFAULT_NBINS DEFAULT_SEQ_FLAG INITed
%%global REFLAYER_STARTBIN REFLAYER_ENDBIN
%
% modification:
% 96/10/02 JF: when percent_good criteria is set, never plots bins where
%  PG < criteria
% 97/05/15 JF: replace flags BOTMPAS3 and LAST_85 with BOTM_EDIT for
%  consistancy with database editing command set_lgb.
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


%% must have a file that is in the matlab adcp directory (where edit/ sits)
%% 97/09/10 - was harmcrv before this: since we moved it, I chose smoothr (jh)
test_file = 'smoothr';

if (exist('shipedit') ~= 2)
   base_path = which(test_file);            %find path to dir_edit_base
   if (isempty(base_path))
      fprintf('you do not have the adcp editing programs on your path.\n')
      return
   end
   bl = length(base_path);                  %length of total path
   base_path((bl-(length(test_file)+1)):bl) = [];  %strip the file off
   path([base_path 'edit'], path);          %add edit to path
end
clear test_file

% editglob
global ABS_U ABS_V AMP AMP_OFFSET BADACC BADBIN BADBIN_BAK
global BADPRF BADPRF_BAK BMAX BMIN BOTM_EDIT BOTTOM BOTTOM_BAK
global CURRX DAYS DBNAME DEFAULT_NBINS DEFAULT_NPRFS DEFAULT_SEQ_FLAG
global DEPTH DISP E EDIT_MODE E_OFFSET IBLKPRF IKEY INITed
global LASTCOMM LASTPLOT LAT LISTED LON LONLAT NBINS NPRFS
global PFLAG PGOOD PLOTBAD PMAX PMIN REFLAYER_ENDBIN REFLAYER_STARTBIN
global TIME U UV_OFFSET U_ROTATE V V_ROTATE W W_OFFSET YAXIS
global PLOTBAD_ORIGINAL
global PGOOD_THRESHOLD D2W_THRESHOLD D2UV_THRESHOLD ...
       WVAR_THRESHOLD AMP_THRESHOLD EV_THRESHOLD
global USE_LGB PLOTNAV start_bin verbose PLOTUV
global PGOOD_OFFSET LGB MAB


LISTED = 1;
INITed = 0;

% Following are default settings for the demo database.          .
% Edit the following lines to set up your own defaults.
%
%                          *********************************
%                          ** getmat.exe default settings **
%                          *********************************

% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Retrieval Parameters >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
DBNAME = '../adcpdb/ademo';      % retrieve data from CODAS database DBNAME
DEFAULT_SEQ_FLAG = 1; % 0 means retrieve nprfs profiles before & nprfs after getp arg
                      % 1 means retrieve nprfs profiles after getp arg
                      % -1 means retrieve nprfs profiles before getp arg
DEFAULT_NPRFS = 25;   % retrieve 50 profiles <before & after> / <after> getp arg
                      % set to lower number if you run into memory problems
DEFAULT_NBINS = 128;  % retrieve maximum of 128 bins per profile
REFLAYER_STARTBIN = 4;% subtract this reference layer mean from U, V and W
REFLAYER_ENDBIN = 12; % set REFLAYER_STARTBIN to 0 to disable;

% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Editing Parameters >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
% To disable any of the editing criteria below, set it to []
EDIT_MODE = 0         % 1 = edit according to marks on plots
                      % 0 = just viewing; do not record marks
PGOOD_THRESHOLD = 30; % don't plot velocity data for bins with pgood < 30
USE_LGB = 1;          % don't show velocity profile below access variables.last_good_bin

D2W_THRESHOLD = 40;   % circle bins that fail to meet both D2W_ and D2UV_THRESHOLD
D2UV_THRESHOLD = 75;

EV_THRESHOLD = 70.0;  % circle bins with error velocity greater than EV_THRESHOLD

WVAR_THRESHOLD = 700.;% use '*' to mark profiles with W variance > WVAR_THRESHOLD
start_bin = 3;        % start calculation of W variance from this bin

AMP_THRESHOLD = 10;   % use '+' to mark bins at which amp. reaches a local max.
BOTM_EDIT =1;         % show effects of set_lgb for bottom editing
verbose = 1;          % display block-profile-bin nos., etc. of failures found
% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Plotting Parameters >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
UV_OFFSET    = 0.1;
W_OFFSET     = 0.02;
E_OFFSET     = 0.02;
AMP_OFFSET   = 10;
PGOOD_OFFSET = 10;
PLOTBAD      = 1;     % 1 = don't show effect of flagging when plotting
                      % 0 = show effect of flagging when plotting
                      %  Toggle with the "pb" command.
PLOTBAD_ORIGINAL = 0; % 1 = don't show effect of bin range
                      %     and percent_good criteria, regardless of
                      %     whether they are set here or via the
                      %     profile_flags array in the database.
                      % 0 = show result of these editing
                      %     steps; that is, display only the data
                      %     points that pass.  Recommended default.
                      %  Toggle with the "pbo" command.

PLOTNAV      = 1;     % 1 = autoplot navigation in a window, 0 = don't
PLOTUV       = 1;     % 1 = autoplot U and V in another window, 0 = don't

editpdef
