function loadrun(pingfile, starttime, endtime)
% loadrun extracts selected variables over a specified time range
% from a ping data file into Matlab .mat files, calculates and plots
% absolute reference layer velocities vs. time (in decimal days),
% and generates stagger plots of the absolute velocities and other
% variables.  Note that the method used for calculating ship velocity
% is a rough first approximation, and no calibration is attempted.
%
% Usage:  1) Edit this file to specify the input parameters
%         2) In Matlab, type 'loadrun'
%    or:  1) In Matlab, type 'loadrun(<pingfile>, <starttime>, <endtime>)'
% Input:  ping data file
% Output: Matlab .mat files
%         PostScript file (.ps for Matlab4) or .met file (Matlab3)

% 88/06/03 Willa Zhu
% 91/10/03
% 92/02/25
% 93/10/27 JR - revisions for Matlab 4
% 94/05/24 MZ - speficy NPRFS as a global variable, otherwise the first "loadrun"
%               run will not get the smoothred velocity.
% 97/12/15 JF - add lines to set matlab45/edit path for call to smoothr.m
% >>>>>>>>>>>>>>>>>>>>> edit the following lines >>>>>>>>>>>>>>>>>>>>>>
if isempty(nargin) | nargin == 0   % Matlab 3.5 prefers 1st form                 
  pingfile  = 'pingdata.000';      % to read from
  starttime = '93/04/09-00:00:00'; % time range to extract (or 'E')
  endtime   = '93/04/09-12:00:00'; % (or number of hours to extract)
end

matfile = 'out_';      % root of output .mat files (4 chars. or less for DOS)

fbin = 5;              % first bin of the reference layer
lbin = 20;             % last bin of the reference layer
pgc  = 30;             % percent good cutoff for editing velocities
filter_width = 5;      % size of fix window for smoothing--must be odd-valued!
iterations = 1;        % no. of smoothing iterations

offset_uv  = 0.1;      % offset for stagger-plotting u & v velocities 
offset_w   = 0.1;      % offset for stagger-plotting w velocity
offset_amp = 10;       % offset for stagger-plotting amplitude  
offset_pg  = 10;       % offset for stagger-plotting percent good
offset_e   = 0.1;      % offset for stagger-plotting error velocity

print_psfile = 0;      % make postscript file? 1 = yes, 0 = no

% following parameters are useful only for Matlab version 4:
pos1 = [.0 .0 .78 .5089]; % position/size of figure 1 window relative to screen
pos2 = [.0 .33 1 .67];    %                         2
pos3 = [.0 .0 1 .67];     %                         3

% <<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
global NBINS NPRFS LONLAT ABS_U ABS_V


if exist([matfile 'time.mat'])
  eval(['delete ' matfile 'time.mat'])
end

eval(['!ping2mat ' pingfile ' ' starttime ' ' endtime ' ' matfile ' UVWPANE' ])

eval(['load ' matfile 'time']);
eval(['load ' matfile 'u']);
eval(['load ' matfile 'v']); 
eval(['load ' matfile 'w']);
eval(['load ' matfile 'e']);
eval(['load ' matfile 'pg']); 
eval(['load ' matfile 'amp']);
eval(['load ' matfile 'nav']);
u = u / 1000; 
v = v / 1000;
w = w / 1000;
e = e / 1000;

[NBINS NPRFS] = size(u);

%--- edit velocity data according to percent good ---
if exist('pg')
  ibad = find(pg < pgc);
  u(ibad) = NaN * ibad;
  v(ibad) = NaN * ibad;
end

%--- average relative reference layer velocities, ignoring NaNs ---
rel_ref_U = [mean_nan(u(fbin:lbin,:)); mean_nan(v(fbin:lbin,:))];

%--- check/interpolate fixes ---
nav = [nav(2,:); nav(1,:)];                   % make it lon-lat! 
ibad = find(nav(1,:) == 0 & nav(2,:) == 0 | ...
            abs(nav(1,:)) > 180.0 | ...
            abs(nav(2,:)) > 90.0);            % find bad fixes 
if length(ibad) > 0
  nav(:,ibad) = NaN * ones(2, length(ibad));  % set bads to NaN
end
LONLAT = intrpnav(nav, rel_ref_U, time);      % interpolate over NaNs

%--- calculate and smooth absolute ref. layer velocities ---
abs_U = refabs(rel_ref_U, time, LONLAT);

test_file = 'smoothr'; 		% set path to editing *.m files
if (exist('editglob') ~= 2)
   base_path = which(test_file);
   bl = length(base_path);
   base_path((bl-(length(test_file)+1)):bl) = [];
   path([base_path 'edit'], path);
end
sm_U = smoothr(abs_U, filter_width, iterations);

%--- reference the ADCP velocity (all bins) ---
ABS_U = ones(NBINS,1) * (sm_U(1,:) - rel_ref_U(1,:)) + u;
ABS_V = ones(NBINS,1) * (sm_U(2,:) - rel_ref_U(2,:)) + v;

%--- generate plots ---
loadrun2
