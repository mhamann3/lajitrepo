
#!/usr/bin/env python

## written by quick_mplplots.py -- edit as needed:

## cruiseid      is 'ADCP'
## dbname        is 'a_sp'
## proc_yearbase is 2014
## printformats   is 'png'

import matplotlib.pyplot as plt





from pycurrents.adcp.quick_mplplots import Btplot

BT = Btplot()
BT(btm_filename='a_sp.btm',
   ref_filename='a_sp.ref',
   cruiseid = 'ADCP',
   printformats = 'png',
   proc_yearbase='2014')

plt.show()
