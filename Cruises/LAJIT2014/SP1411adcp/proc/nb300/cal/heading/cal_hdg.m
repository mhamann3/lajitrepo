% function cal_hdg()
% FILE: cal_hdg.m
%
% Assuming that calibration phase has the following relation with the
% ship's heading and time:
%       
%   Phase = a0 + a1*cos(Heading) + a2*sin(Heading) + a4*time
%
% cal_hdg.m calculates the mean ship's heading and mean phase, and calls
% the function fitphase() to fit the above equation.   The estimates 
% (a0,a1,a2) and residuals (fitted phase - phase) will be stored in 
% variable 'est' and 'err' repectively.
% 
% User must specify the following parameters:
%
%  file    = output file name from TIMSLIP.EXE (*.cal file)
%  label   = part of the title on the plot
%  trimErr = the max residual from the fitting, Phase beyong the which
%            will be trimmed off and the fitting will be done iteratively
%            until all phases are within this max residual.
%  timeOrder = time term order (up to 4th order).
%  plot_hdg = 1; plot time vs. heading;
%             0; otherwise.
%  at_hdg  = []; if all data will be used without calculating the mean 
%            else; (a vector), the mean ship's heading and mean phase
%            will be calculated at range (at_hdg-del_hdg) to (at_hdg+
%            del_hdg)
%  del_hdg = see above explanation
%
% ---------------------------------------------------------------------------
%             Willa Zhu,   Aug. 6, 1991
%----------------------------------------------------------------------------

% >>>>>>> edit these parameters >>>>>>>>>

file = '../watertrk/ademocal.dat';
label= 'ADCP DEMO';                   % title for plot
trimErr = 2.0;
timeOrder = 0;                   % time term order
plot_hdg = 1;                    % 1 to plot time vs. heading, 0 otherwise
at_hdg = []; [0 90 180 270 360];
del_hdg = 10;

% <<<<<<< end edit parameters <<<<<<<<<<

cal_hdg2

