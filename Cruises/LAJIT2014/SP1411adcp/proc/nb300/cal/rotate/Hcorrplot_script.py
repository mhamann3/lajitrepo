
#!/usr/bin/env python

## written by quick_mplplots.py -- edit as needed:

## cruiseid      is 'ADCP'
## dbname        is 'a_sp'
## proc_yearbase is 2014
## printformats   is 'png'

import matplotlib.pyplot as plt






from pycurrents.adcp.quick_mplplots import Hcorrplot

PH = Hcorrplot()
PH(hcorr_filename='ens_hcorr.asc',
   titlestr = 'ADCP: Heading Correction',
   outfilebase = 'hcorr_mpl',
   printformats = 'png',
   proc_yearbase = 2014,
   ddrange = 'all')

plt.show()
