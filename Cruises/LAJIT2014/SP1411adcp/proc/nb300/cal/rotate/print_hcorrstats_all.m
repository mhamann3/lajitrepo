%%% user edits this section %%%


yearbase = 2006;
comment = 'heading from posmv applied to ensembles';
hcorfile = 'ens_hcorr.asc';
hcorr_inst = 'posmv';

   
%% --------- end of section to edit ----------   
   
   
if exist(hcorfile)
   print_hcorstats('yearbase', yearbase,...
                   'stats_ddrange', 360,...
                   'hcorfile', hcorfile,...
                   'comment', comment,...
                   'outfile', 'hcorr_all_stats.txt');
else
  fid = fopen('hcorr_stats.txt', 'w');
  fprintf(fid, 'no heading correction applied \n')
  fclose(fid)
end

