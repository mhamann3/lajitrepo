function caldat = adcpcal(cal_, label, trange, dvrange);
% function caldat = adcpcal(cal, label, trange, dvrange);
%  input:
%     cal     = the output filename from timslip, or output matrix
%               which has been loaded into matlab
%     label   = text for plot titles
%  output: caldat (structure with watertrack calib info)
%  optional arguments:
%     trange  = a vector [t0 t1] with the time range of the
%        calibration points to be used.
%     dvrange = a vector [umin umax vmin vmax] with the
%        range of acceptance of velocity component differences,
%        post-acceleration minus pre-acceleration.
%
%  This function calculates statistics for the
%  amplitude and phase calibration factors, and
%  the seconds to be added to the recorded time to
%  maximize agreement with the navigation, and
%  plots them as time series and as histograms.
%
%  Statistics are appended to the file adcpcal.out.

%       Eric Firing
%       Fri  02-17-1989; major mod Fri  12-15-1989
% 97/05/15 JF: modified to read in cal matrix from timslip output file
%               directly without stripping off header


% >>>>>>>>>>>>>>>>>>>> user must edit the following lines >>>>>>>>>>>>>>>>>>>>
                % phase seems more often to contain a trend than amplitude,
                % so it may be justified to clip phase less stringently
config.clip_ph  = 3;        % degrees on either side of median
config.clip_amp = 0.04;     % eliminate anything over x percent from median
config.clip_var = 0.05;     % maximum variance of ref layer velocity
config.clip_dt  = 60;       % maximum time slip beyond median
% >>>>>>>>>>>>>>>>>>>>>>>>>>>>> end editing >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

config.label = label;
config.cal_ = cal_;

config.trange = [];
config.dvrange = [];

if nargin == 3,      % check which optional argument it is
   if length(trange) == 4   % it must be dvrange
      config.dvrange = trange;
   else
      config.trange = trange;
   end
elseif nargin == 4,
   config.trange = trange;
   config.dvrange = dvrange;
elseif nargin ~= 2,
   help adcpcal
   error('must specify cal output file name and plot label')
end

caldat = adcpcal1(config);
