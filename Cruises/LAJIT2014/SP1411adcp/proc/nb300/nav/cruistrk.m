% cruistrk is used to generate plots of the cruise track (lon vs. lat,
% lon vs. time, and lat vs. time).
%
% Usage:  1) Edit this file to specify the input parameters
%         2) In Matlab, type 'cruistrk'
% Input:  ASCII fix file with columns decimal day, longitude and latitude
%         (from ubprint or some other source).
% Output: PostScript file cruistrk.ps

%>>>>>>>>>>>>>>>>>>>>>>> edit the following lines >>>>>>>>>>>>>>>>>>>>>>>

fix_file    = 'ademo.ags' 
cruise_id   = 'ADCP DEMO'             
year_base   = 1993;
plot_symbol = '.';     % choose from '.', '+', '--', etc.
	
%<<<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

cruistr2               % do it
