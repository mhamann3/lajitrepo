
#!/usr/bin/env python

## written by quick_mplplots.py -- edit as needed:

## cruiseid      is 'ADCP'
## dbname        is 'a_sp'
## proc_yearbase is 2014
## printformats   is 'png'

import matplotlib.pyplot as plt




from pycurrents.adcp.quick_mplplots import Refplot

PT = Refplot()
PT(sm_filename='a_sp.sm',
   ref_filename='a_sp.ref',
    cruiseid = 'ADCP',
    proc_yearbase = 2014,
    printformats = 'png',
    ddrange = 'all')

plt.show()
