
#!/usr/bin/env python

## written by quick_adcp.py-- edit as needed:




from pycurrents.adcp.quick_npy import Refsm

Ref = Refsm()
Ref(dbname='a_sp',
    dbpath='../adcpdb',
    proc_yearbase=2014,
    fixfile='a_sp.gps',
    ens_len=300,
    pgmin=50,
    rl_startbin=2,
    rl_endbin=20)


