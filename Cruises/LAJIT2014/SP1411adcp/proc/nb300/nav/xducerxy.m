% xducerxy.m
%
%% This is not yet standard procedure.  Be careful.
%
% If the antenna measuring position is not directly over the transducer, an
% error is introduced in the ship speed used to calculate ocean velocity.
%
% A correction can be applied to the positions used in processing if this
% offset is known. 
%
% For instance, suppose the demo data were collected on a ship where the
% the antenna and the transducer were mounted on the centerline, 
% with the transducer forward of the antenna by 17m
% and the transducer port of the antenna by 1m.
% 
% To get a file of corrected positions,  usage would be 
% 
%    xducerxy_core('dx', -1, 'dy', 17, 'yearbase', 1993);
%
% and then you would use the output file ademo.agt for the fix file
%
%
%
%
% If you want to see how changing this value affects the second derivative 
% of the ocean velocities (a characteriestic signature of transducer/antenna 
% offset), look at best_xducerxy.
%
% eg.  best_xducerxy('yearbase',1993);

