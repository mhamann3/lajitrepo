% refsm.m:  This is a stub that calls the program(s) that use the new
%           matlab scheme for smoothing navigtion.
%           This stub calls a function (refsm_core) in the matlab/codas3
%           directory.  That (in turn) calls "adcp_nav", the heart of
%           the matter.  Edit the next three lines of code and run it from
%           the nav/ directory.


%% edit these things 

yearbase = 1993;
dbname =  'ademo';                
fixfile = 'ademo.ags';


%%%%%%%%%%%%%%  end of section to be edited by user %%%%%%%%%%%%%%%

% add the matlab/codas3 directory
adcppath

%now call the engine, refsm_core
refsm_core(dbname, fixfile, yearbase);
