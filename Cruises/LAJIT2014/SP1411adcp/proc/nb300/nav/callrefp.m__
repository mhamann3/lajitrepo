function callrefp(t0)
%
% Usage:  callrefp(t0)
% where:  t0 = time origin (for x-axis) in decimal days
%
% callrefp generates the input files and sets up the call for plotting
% the absolute reference layer velocities (smoothed and/or unsmoothed).
%
% It calls the ref2mat and sm2mat programs for converting
% the user-specified input files into Mat-files, and then
% calls the refplot.m function to perform the plotting.
% These programs/function must therefore be in the user's search path.

% >>>>>>>>>>>>>>>>>>>> user must edit the following lines >>>>>>>>>>>>>>>>>>>>

% >>> input files >>>      % set either one to [] if not to be plotted
smoothrfile = '__bin_file__'; % smoothed velocity input file
                           % (binary output of smoothr program)
refabsfile  = '__ref_file__'; % unsmoothed velocity input file if use_ref = 1
                           % (text file output of refabs program)

% >>> if smoothrfile is to be plotted, set following to match smoothr.cnt values >>>
max_gap_ratio = 0.05;
min_speed     = 1.0;
max_speed     = 5.0;

% >>> output file (t0 will be appended to this root + extension .ps) >>>
outfile = '__outfile__';   % root for output filename (max. 5 chars.)

% >>> axis settings >>>
dt      = 2.0;       % no. of days to plot (x-axis length in decimal days)
y0      = -1.5;        % velocity origin (y-axis min. value, in m/s)
dy      = 3;         % velocity range (y-axis range, in m/s)

% >>> plot labels >>>
cruise  = '__title__';             % title for plot
year    = __year_base__;         % base year for decimal days (x-axis label)

% <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

if nargin < 1
  t0 = 0.0;
end

% when retrieving data, allow a little before and a little after:
time_range = sprintf(' %f %f ', t0 - 0.1 * dt, t0 + 1.1 * dt);

if ~isempty(refabsfile)
  ref_matname = [ outfile 'ref.mat' ];
  ref_ncol = 9;                  % # of columns to be retrieved
  tmpcmd = fullfile('__bin_dir__', 'ref2mat');
  eval([ '!'  tmpcmd ' '  refabsfile ' ' ref_matname time_range sprintf('%f', ref_ncol) ]);
else
  ref_matname = '';
end

if ~isempty(smoothrfile)
  sm_matname  = [ outfile 'sm.mat' ];
  sm_ncol = 8;                   % # of columns to be retrieved
  step = 1;                      % default 1
  tmpcmd = fullfile('__bin_dir__', 'sm2mat');
  eval([ '!' tmpcmd ' ' smoothrfile ' ' sm_matname time_range sprintf('%f %f', step, sm_ncol) ]);
else
  sm_matname = '';
end;

ax = [t0 t0+dt y0 y0+dy];
refplot(year, [outfile sprintf('%03.0f', fix(t0)) ], ax, ref_matname, sm_matname, min_speed, max_speed, max_gap_ratio, cruise);
