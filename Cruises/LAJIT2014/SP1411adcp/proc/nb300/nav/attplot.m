%plot ashtech statistics
%NOTE: not relevant for the demo since it has no ashtech
% this is just a stub to call gattplot.  do 'help gattplot' to look at it

%... but of course, ...
% this won't work in the demo because there are no Ashtech data!!


arg1.attfile     =   'ub000001';     % attfile is an array of matlab
                                     %  attitude filenames, without the
                                     %  ".mat" extension, from ubprint
                                     %  --> use ("attitude_mat") in ubprint
                                     %
arg1.year_string =      '1993';      % year base
                                     %
arg1.print_flag  =       2;          % print postscript files to disk or not
                                     % defaults to 2 (append to file)
                                     % 0: do not print
                                     % 1: print to default printer
                                     % 2 print postscript files to disk 
                                     %      (filename is from arg1.attfile:
                                     %      eg: ademo.ps)
                                     %
arg1.psfile      = 'att_ademo.ps';   % name of psfile (if print_flag == 2)
                                     % defaults to [arg1.attfile,'.ps']
                                     %
arg1.title       =  'ADCP demo';     % figure title
                                     %
arg1.start_dd    = [98:1:99];        % array of decimal days to START each plot
                                     %
arg1.days_per_page =    1;           % number of days of data per plot
                                     % defaults to 3
                                     % (probably would be diff(arg1.start_dd))
                                     %
arg1.pages       =      1:3;         % pages is a scaler or vector specifying
                                     %  which plot pages are to be produced:
                                     %  1 is the primary page, with dh_mean,
                                     %     dh_std, p_std, r_std, and n_used;
                                     %  2 is secondary,  with min, max, and
                                     %  mean of pitch and roll.
                                     %  3 is yet another page, this time  with
                                     %     mrms, mrbrms, mnn, mrms, n brms,
                                     %     outlier, reacq, n att.
                                     %
arg1.newfigwins = 0;                 % defaults to 0 (recycle fig windows)
                                     % 0: use the same figures over and over
                                     %     figs numbered as arg1.pages
                                     % 1: create a new figure each time

%end of section to edit ----------------------------------------
gattplot(arg1);

