% stn_udw is used to plot on-station and/or underway section means and
% standard deviations of U, V, W, Error Velocity, Amplitude, Spectral Width, 
% Percent Good and Percent 3-Beam Solutions.  It is useful for assessing 
% ADCP performance on-station and underway.
%
% Usage:  1) Edit this file to specify the input parameters.
%         2) In Matlab, type 'stn_udw'
% Input:  .mat file with on-station and/or underway data (from profstat program)   
% Output: PostScript file (.ps)
%

global j_npts j_depth j_mean j_std outfile sub_title nplot;

% >>>>>>>>>>>>>>>>>>>> user must edit the following lines >>>>>>>>>>>>>>>>>>>>

% >>> input files >>>        % set to [] if not to be plotted
stn_file  = 'ademo_os.mat';  % input Matfile name for on-station data
udw_file  = 'ademo_uw.mat';  % input Matfile name for underway data

% >>> output >>>
outfile   = 'ademo';         % root for output PostScript file
sub_title = '(Solid: On Station; Dash: Underway)';  % plot subtitle

% <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< end editing <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

stn_udw2                     % do it
