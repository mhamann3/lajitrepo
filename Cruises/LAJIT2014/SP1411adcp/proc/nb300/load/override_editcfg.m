%%%%%%%%%%%%%% For experts only %%%%%%%%%%%%%%%

%% use this file to override pingedit defaults.

%% Find out the pignedit defaults for a particular 
%% instrument + pingtype, by doing the following
%
%
%    editcfg = get_pedefaults(os, 'nb')
%
%

%% Then override any settings by prefixing with "params."
%%
%% eg. (uncomment the following line)


% params.weakprof_numbins  = 6;



%% to add your own singleping editing, 
%% (1) create a function 
%%      eg. "wref_edit.m" and put it in this directory
%% (2) override by uncommentnig the following line:


%  params.editcfg.useredit_functions = {'wref_edit'};
