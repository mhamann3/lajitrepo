little_endian
binary_file: ens_blk001.bin
new_block
dp_mask: 1
binary_data: DEPTH FLOAT 60 0
binary_data: CONFIGURATION_1 DOUBLE 23 240
new_profile: 2014/12/05 14:10:29  /* 1 21 */
depth_range: 11 247
binary_data: ACCESS_VARIABLES DOUBLE 8 424
binary_data: ANCILLARY_1 DOUBLE 10 488
binary_data: ANCILLARY_2 DOUBLE 23 568
binary_data: BOTTOM_TRACK DOUBLE 3 752
binary_data: NAVIGATION DOUBLE 4 776
binary_data: U FLOAT 60 808
binary_data: V FLOAT 60 1048
binary_data: W FLOAT 60 1288
binary_data: ERROR_VEL FLOAT 60 1528
binary_data: AMP_SOUND_SCAT UBYTE 60 1768
binary_data: RAW_AMP UBYTE 240 1828
binary_data: PERCENT_GOOD UBYTE 60 2068
binary_data: PERCENT_3_BEAM UBYTE 60 2128
binary_data: RESID_STATS FLOAT 360 2188
binary_data: TSERIES_STATS FLOAT 7 3628
binary_data: PROFILE_FLAGS UBYTE 60 3656
