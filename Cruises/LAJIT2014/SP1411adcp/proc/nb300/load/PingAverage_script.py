
#!/usr/bin/env python

## written by quick_adcp.py-- edit as needed:





from pycurrents.adcp.quick_npy import PingAverage


# paths are relative to "load" (quick_adcp.py already 'cd load')
# hcorr inst must be set in control files

P = PingAverage()
P(sonar = 'nb300',
  cruisename = 'SP1411',
  cfgpath = '/home/data/SP1411/proc/nb300/config',
  configtype = 'python',
  py_gbindirbase = '/home/data/SP1411/gbin',
  edit_paramfile = 'ping_editparams.txt',
  ping_headcorr = False,
  ens_len = 300,
  max_BLKprofiles = 300,
  dday_bailout = None,
  badbeam = None,
  incremental = True,
  yearbase = 2014)



