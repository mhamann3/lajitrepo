
%--------------------- raw logging -----------------------------

% cruise configuration file template, used by cruisesetup.py
% to write  __cruiseid__cfg.m

% Things marked with arrows may change if the installation
% changes; this may be automated later, but for now it must
% be done manually.

% these variables are used by quick_adcp.py

shipname = 'R.G.Sproul';
                                 % acquisition yearbase must match data
yearbase = 2014;  % acquisition yearbase (integer, e.g., 2005)
proc_yearbase = 2014;  % processing yearbase
cruiseid = 'SP1411';         % a title string, cruise designation
uhdas_dir = '/home/data/SP1411';   % root for dirs: raw, rbin, gbin, proc

% startcruise.py should get instrument names from config/sensor_cfg.py


%  This cellarray "msg_info" holds a list of all possible serially-logged rbin
%  directories and the message names extracted from them.  This is
%  only edited to ADD a new instrument.  Instruments with rbin files
%  available are extracted as needed (see below for determination of "best"
%  --> See "help get_instfields"   for more info
%
%      directory    extension   extension
%                    to read     to write
%     -----------  ---------   ----------
%       {'gyro'  ,  'hdg',         % gyro heading
%       'ashtech',  'adu',         % attitudes from adu
%       'posmv',    'pmv',         % attitudes from posmv
%       'seapath',  'sea',         % attitudes from seapath
%       %
%       'gpsnav',   'gps',         % positions from generic GGA (eg. pcode)
%       'simrad',   'gps',         % positions from simrad
%       'ashtech',  'gps',         % positions from adu
%       'posmv',    'gps',         % positions from posmv
%       'seapath',  'sea_gps',     % positions from seapath
%       %
%       'sndspd',   'spd',         % soundspeed sensor (polar ships)
%
% %if msg_info is specified below, it overrides the default (which
%          uses everthing)

msg_info = ...
        {'ashtech', 'adu',
         'gpshead', 'hdg',
         'gpsnav', 'gps',
                 };


% Now choose the rules to use to get 'best' position and attitude:
% Only include the fields with values; they must be IN ORDER
% (1) choose the variables to get 'best' versions of:

best.rules = {'gps', 'heading', 'pitch', 'roll'};

% (2) Choose the instrument to get the field from:
%
%                   instrument        message
%                   ---------        --------
best.gps_rule  =    {'gpsnav',          'gps'};  % usually pcode or other gps
best.heading_rule = {'gpshead',           'hdg'};  % "best" = "most reliable"
best.pitch_rule =   {'ashtech',          'adu'};  % recorded, but not implemented
best.roll_rule  =   {'ashtech',          'adu'};  % recorded, but not implemented


% choose instrument to correct gyro  (at sea: this field was created
%        using the value specified in  procsetup_onship.py)
%
% NOTE: for PYTHON processing, this field is ignored.
%       it must be specified in quick_adcp.py
% Must be an available attitude.  "disable" by setting to 'gyro';
hcorr_inst = 'ashtech';  % eg 'posmv'



% set full paths to raw  bin, gbin, and processing  directories
rawdir   =  fullfile(uhdas_dir, 'raw');
rbindir  =  fullfile(uhdas_dir, 'rbin');

% directory to write processing log files
logfpath  =  fullfile(uhdas_dir, 'proc/proclog');


%---------------- processing (quick_adcp.py) --------------------------

gbindirbase  =  fullfile(uhdas_dir, 'gbin');    %<--
         %contains son125, os38b, os38n for gbin subdirs (time, best...)

