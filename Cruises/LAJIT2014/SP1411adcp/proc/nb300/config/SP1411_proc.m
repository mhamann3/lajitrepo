% R.G.Sproul ADCP configuration

% See foo for documentation of the format and contents.
nb300.nb.ducer_depth = 3;
nb300.nb.h_align = 44.8;
nb150.nb.beamangle = 30;
nb300.nb.velscale.soundspeed = 'calculate';
nb300.nb.velscale.scalefactor = 0.98;
nb300.nb.velscale.salinity = 35.0;


allparams = struct('nb300', nb300);