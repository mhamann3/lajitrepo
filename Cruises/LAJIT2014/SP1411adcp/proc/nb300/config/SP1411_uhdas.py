shipkey = "sp"  
## ship key must match shipletters in sensor_cfg.py
#shipname = "R.G.Sproul"


#####################################
# Additional ship configuration information: (all as comments)
#
# deeper blanking interval      use defaults

# IP number                     (dynamically allocated)
# ntp server                    ntp
#
# last changed May 31, 2011
###################################


# for DAS_while_cruise.py
#-------------------------
# List of paths to backup "data" subdirectories.  Within each such
# subdirectory, the cruise data directory (e.g., km0507) will
# be found.  If the active cruise is "km0507", then rsync will
# be called periodically to copy the contents of /home/data/km0507
# to data/km0507 in each path in the list.

backup_paths = []

# turns off gzipping (for R2R)
gzip_serial_files = False #default would be to gzip


# Interval in seconds between backups

rsync_t = 3600


# for DAS_while_logging.py
#--------------------------

# Interval in seconds between ensemble updates:

short_t = 300

# Interval in seconds between database updates:

long_t = 1800


# Working directory for calculations and intermediate products:

workdir = '/home/adcp/uhdas_tmp'

# Command string to start up the speedlog; empty string or
# None to disable the speedlog.  If there is a speedlog command,
# the string must end with '&' so that it will be executed in
# the background.

#speedlog_cmd = '/home/adcp/scripts/DAS_KMspeedlog.py &'  # backgrounded
speedlog_cmd = None # No speedlog at present.


# (4) values for web plots and quality monitoring

## daily.py will get statistics on these (assumes correct messages are logged)
## and quality plots will go on the web site on the long_t timer
attitude_devices = ['ashtech']   # string or list of strings

##ocean surveyor beam statistics;   # disable is emtpy, i.e.  []
beamstats = [    
    'scattering',
    'velocity',
]

# new section, to create cruise_proc.m
# all dictionaries must be filled

## for quick_adcp.py: # initialize; # fill
## going to be top_plotbin
top_plotbin = {}
top_plotbin['nb300'] = 1

## which virtual instrument will be used for kts+dir 5-minute bridge plots?
## going to be kt_dir_instlist ['os75nb', 'os75bb']
kts_dir_instrument = ['nb300']

#####################################

# for daily.py:
#--------------

# Each email "mailto" must be a list of email addresses:
#        ["et@this_ship.podunk.edu", "guru@podunk.edu"]
# An empty list (no addresses) is : []

tarball_mailto        = ["uhdas@soest.hawaii.edu",]
local_status_mailto   = ["restech@rv-sproul.ucsd.edu"]
shore_status_mailto  = ["hummon@hawaii.edu", 
                        "efiring@hawaii.edu",
                        "scg@rv-sproul.ucsd.edu"]

##################

SMTP_server     = "smtp"
mail_from       = "adcp@rv-sproul.ucsd.edu"


