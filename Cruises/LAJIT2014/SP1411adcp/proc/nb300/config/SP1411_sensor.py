# This configuration file is Python code.  You should not
# need to change it; but if you do, you need to know that
# in Python, the *indentation matters*.

# The following will normally be empty lists, like this:
#
#ignore_ADCPs = []
#ignore_other_sensors = []
#
# But if you want to run with only a subset of the ADCPs
# and/or ancillary sensors that are defined in this file,
# you can list the ones you want to ignore like this:
#
#ignore_ADCPs = ['wh300', 'os75']
#ignore_other_sensors = ['GPS']
#
# In this case, you are listing the 'instrument' field of each
# ADCP or sensor you wish to exclude.
#

ignore_ADCPs = []
ignore_other_sensors = []

# 2-letter abbreviation for logging file prefix and constructing dbase name;
# read by procsetup.py
shipabbrev = "sp"


ADCPs = [
        { 'instrument'  :  'nb300',
          'setup'       :  'rdi_setup',
          'terminal'    :  'nb_term',
          'defaultcmd'  :  'nb300_default.cmd',
          'commands'    :  (),
          'datatypes'   :  ('nb300',),
          'wakeup_baud' :  19200
        }

        ]


# Do not edit the next three lines:
common_opts = '-f %s -F -m 1 -H 2 ' % (shipabbrev,)
nb_opts =   '-rlE -c -I'          # raw data, write a log file, log errors
oswh_opts = '-rlE -c -O -I'       # -O for OS/WH data format
                                  # -c for checksum, -I to initiate pinging

sensors = [
        { 'instrument'  :  'nb300',
          'device'      :  'ttyUSB0',
          'baud'        :   19200,      
          'format'      :  'binary',
          'subdir'      :  'nb300',
          'ext'         :  'raw',
          'opt'         :  nb_opts}, 


        { 'instrument'  :  'Ashtech',
          'device'      :  'ttyUSB1',
          'baud'        :   4800,
          'format'      :  'ascii',
          'subdir'      :  'ashtech',
          'ext'         :  'adu',
          'strings'     :  ('$GPGGA','$PASHR,ATT'),
          'messages'    :  ('adu',),
          'opt'         :  '-tc'},

        # replace synchro-converted feed with "GPS Compass" 2012/04/16
        { 'instrument'  :  'GPS Compass', #Furuno
          'device'      :  'ttyUSB2',
          'baud'        :   4800,
          'format'      :  'ascii',
          'subdir'      :  'gpshead',
          'ext'         :  'hdg',
          'strings'     :  ('$GPHDT',),
          'messages'    :  ('hdg',),
          'opt'         :  '-tc '},

        { 'instrument'  :  'GPS17',
          'device'      :  'ttyUSB3',
          'baud'        :   4800,
          'format'      :  'ascii',
          'subdir'      :  'gpsnav',
          'ext'         :  'gps',
          'strings'     :  ('$GPGGA',),
          'messages'    :  ('gps',),
          'opt'         :  '-tc '},

          ]


## enabling or disabling this occurs in procsetup_onship.py
speedlog_config = {
    'instrument'        : 'nb300',
    'serial_device'     : '/dev/tty_dgnc_0_7',
    'baud'              : 4800,
    'dir_output'        : '',
    'heading_offset'    : 45.4,
    'scale'             : 1.0,
    'bins'              : (1,12),

    'heading_sensor'    : 'ashtech',
    'heading_msg'       : 'adu',

    'soundspeed_sensor' : None,
    'soundspeed_msg'    : 'snd',
    'nominal_soundspeed': 1536.0,
    }




#### DO NOT CHANGE the following ################

ADCPs = [A for A in ADCPs if A['instrument'] not in ignore_ADCPs]
sensors = [S for S in sensors if S['instrument'] not in ignore_ADCPs]
sensors = [S for S in sensors if S['instrument'] not in ignore_other_sensors]



