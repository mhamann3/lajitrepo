shipname = 'R.G.Sproul'
cruiseid = 'SP1411'
yearbase = 2014
uhdas_dir = '/home/data/SP1411'

# from proc_cfg.py:
# for processing
#----------------

## all heading+msg pairs, for hbin files
hdg_inst_msgs =  [("gpshead","hdg"),("ashtech", "adu")]

## chose first heading applied  (directory and rbin msg)
hdg_inst = 'gpshead' # Furuno, not 'gyro'
hdg_msg =  'hdg'  # has checksum 'hnc'
# the next line is for temporary compatibility with pingavg
hbest = [hdg_inst, hdg_msg]

# choose position instrument (directory and rbin message)
pos_inst = 'gpsnav'
pos_msg  = 'gps'

### instrument for heading corr to ADCP data (dir and msg) from hdg_inst_msgs
hcorr_inst = 'ashtech' 
hcorr_msg = 'adu'
hcorr_gap_fill = 0  ## fallback correction for hcorr gaps

pitch_inst = 'ashtech'
pitch_msg = 'adu'

roll_inst = 'ashtech'
roll_msg = 'adu'

# (2) values substituted into cruise_proc.m  (for matlab)

# heading alignment:  nominal - (cal/watertrack)
h_align = {}
h_align['nb300']  =  44.8 ## SP1110Constable

# transducer depth, meters
ducer_depth = {}
ducer_depth['nb300']  = 3

# optional: weakprof_numbins
# weakprof_numbins = {'os38bb' : 2}

# velocity scalefactor
scalefactor = {}
scalefactor['nb300'] = 0.98

# soundspeed
soundspeed = {}
soundspeed['nb300'] = 'calculate'

# salinity
salinity = {}
salinity['nb300'] = 35.0

# (3) values for quick_adcp.py

## choose whether or not to use topography for editing
## uses smith,sandwell version from about 2004

max_search_depth = {}
max_search_depth['nb300'] = 300      # New name

# averaging interval (seconds) usually 300.  might use 120sec for wh300
enslength = {}
enslength['nb300']  = 120
