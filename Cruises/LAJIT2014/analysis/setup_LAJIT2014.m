% setup_LAJIT2014
% Updated 1/27/15 MMH
clear rec info

info.cruise_name = 'LAJIT2014';
info.project_name = 'LAJIT';
info.unols_name = 'SP1411';

cd(fileparts(mfilename('fullpath')))
cd ..
cd ..
cd ..

% rec.topfolder = fullfile('/Volumes/DataDrive1/Projects',info.project_name);
rec.topfolder = cd;
rec.cruise = fullfile(rec.topfolder,'Cruises',info.cruise_name);

rec.bathy = fullfile(rec.cruise,'bathy');
rec.bathyfile = fullfile(rec.cruise,'bathy','ljcan_merged.mat');

rec.prpath = fullfile(rec.cruise,'Figures/');
rec.matfiles = fullfile(rec.cruise,'analysis/matfiles/');

rec.CTD.raw_fullfiles = fullfile(rec.cruise,'CTD/data_mat/raw_fullfiles/');
rec.CTD.mat = fullfile(rec.cruise,'CTD/data_mat/');
rec.CTD.proc_cast = fullfile(rec.CTD.mat,'proc_castfiles/');
rec.CTD.raw_cast = fullfile(rec.CTD.mat,'raw_castfiles/');
rec.CTD.raw_cast_sep = fullfile(rec.CTD.mat,'raw_castfiles/sep/');
rec.CTD.gridded = fullfile(rec.CTD.mat,'gridded/');
rec.CTD.MSA = fullfile(rec.CTD.mat,'procMSA/');

rec.chi.raw = fullfile(rec.cruise,'chipod/data_raw/');
rec.chi.proc = fullfile(rec.cruise,'/chipod/data_proc/');
rec.chi.up = fullfile(rec.chi.proc,'chi_up/');
rec.chi.dn = fullfile(rec.chi.proc,'chi_down/');
rec.chi.up_avg = fullfile(rec.chi.proc,'chi_up/avg/');
rec.chi.dn_avg = fullfile(rec.chi.proc,'chi_down/avg/');

rec.sADCP = fullfile(rec.cruise,[info.unols_name,'adcp']);

