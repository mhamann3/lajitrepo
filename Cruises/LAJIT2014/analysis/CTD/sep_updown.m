% Updated 1/28/15 MMH
% seperates raw CTD cast_files into UP and DN based on value of maximum
% pressure. 
% make_raw_castfiles MUST be run before this can be used, otherwise the
% file will just split files with many casts into ~halves

setup_LAJIT2014

fnames = help_FilenamesFromPath(rec.CTD.raw_cast,'.mat',0);

for ii=1:length(fnames)
    load(fnames{ii})
    
    id = find(data2.p==max(data2.p));
    
%     figure(40); clf
%     plot(data2.datenum,data2.p); hold on
%     plot(data2.datenum(id),data2.p(id),'r*')
%     datetick
    
    % Set aside all the data
    data2all=data2;
    if length(id>1)
%         disp(id)
        [r,c,id]=findnearest(length(data2.p)/2,id);
    end
    
    clear data2
    
    % Make data2 the relevant subset of data2all for Downcast
    data2.t1=data2all.t1(1:id);
    data2.c1=data2all.c1(1:id);
    data2.p=data2all.p(1:id);
    data2.t2=data2all.t2(1:id);
    data2.c2=data2all.c2(1:id);
    data2.fl=data2all.fl(1:id);
    data2.trans=data2all.trans(1:id);
    data2.oxygen=data2all.oxygen(1:id);
    data2.lon=data2all.lon(1:id);
    data2.lat=data2all.lat(1:id);
    data2.modcount=data2all.modcount(1:id);
    data2.time =data2all.time(1:id);
    data2.time0 = data2all.time0(1:id);
    data2.datenum = data2all.datenum(1:id);
    
    % Alter outfile name
    matname=[fnames{ii}(1:end-4),'_DN.mat'];
    save(fullfile(rec.CTD.raw_cast_sep,matname),'data2')
    
    clear data2
    
    % Make data2 the relevant subset of data2all for upcast
    data2.t1=data2all.t1(id:end);
    data2.c1=data2all.c1(id:end);
    data2.p=data2all.p(id:end);
    data2.t2=data2all.t2(id:end);
    data2.c2=data2all.c2(id:end);
    data2.fl=data2all.fl(id:end);
    data2.trans=data2all.trans(id:end);
    data2.oxygen=data2all.oxygen(id:end);
    data2.lon=data2all.lon(id:end);
    data2.lat=data2all.lat(id:end);
    data2.modcount=data2all.modcount(id:end);
    data2.time =data2all.time(id:end);
    data2.time0 = data2all.time0(id:end);
    data2.datenum = data2all.datenum(id:end);
    
    % Alter outfile name
    matname=[fnames{ii}(1:end-4),'_UP.mat'];
    save(fullfile(rec.CTD.raw_cast_sep,matname),'data2')
    
    
end



% %% Check the results of the separation
% 
% fnames = help_FilenamesFromPath(rec.CTD.raw_cast_sep,'.mat',0);
% 
% for ii=1:length(fnames)
%     load(fullfile(rec.CTD.raw_cast_sep,fnames{ii}))
%     figure(41); clf
%     plot(data2.datenum,data2.p)
%     axis ij
%     datetick
%     title({fnames{ii}})
%     pause
% end


