% Generate a structure with the times of all CTD casts from the processed
% CTD data

addMaddie('lajit')
cd('procMSA')

fnames = help_FilenamesFromPath(cd,'.mat',0);
for ii = 1:length(fnames)
%     example = matfile(fnames{ii});
%     downstart(ii) = example.datad
load(fnames{ii}) % each file contains datad, datau, datad_05m, and datau_05m

    down_start(ii) = min(datad.datenum);
    down_end(ii) = max(datad.datenum);
    up_start(ii) = min(datau.datenum);
    up_end(ii) = max(datau.datenum);   
end 

cd('/Volumes/DataDrive1/Projects/LaJIT2014/matfiles')
save('cast_times','down_start','down_end','up_start','up_end')
    
% datestr(max(datad.datenum))
% datestr(min(datad.datenum))
% datestr(max(datau.datenum))
% datestr(min(datau.datenum))
