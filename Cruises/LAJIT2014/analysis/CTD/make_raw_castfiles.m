% Updated 1/27/15 MMH
% Now adds time0 and datenum structure on separation

setup_LAJIT2014

fnames = help_FilenamesFromPath(rec.CTD.raw_fullfiles,'.mat',0);

for jj=8:length(fnames)
    load(fnames{jj})
    
    % Set aside all the casts
    data2all=data2;
    % use smoothed diff(p) to find end points of casts
    dp=smooth(diff(data2.p),4001);
    dp=smooth(dp,1001);
    % find indices of diff(p) sign changes
    ii=find((dp(1:end-1).*dp(2:end)) < 0);
    % use only sign changes from positive to negative
    ii(dp(ii) > 0)=[];
    % Set end points for casts
    endpts=[1; ii];
    % index for naming
    S=1;
    
    for kk=1:length(endpts)-1
        % Ensure peak between endpts
        if max(data2all.p(endpts(kk):endpts(kk+1))) < 50
            continue
        end
        
        clear data2
        
        % Make data2 the relevant subset of data2all
        data2.t1=data2all.t1(endpts(kk):endpts(kk+1));
        data2.c1=data2all.c1(endpts(kk):endpts(kk+1));
        data2.p=data2all.p(endpts(kk):endpts(kk+1));
        data2.t2=data2all.t2(endpts(kk):endpts(kk+1));
        data2.c2=data2all.c2(endpts(kk):endpts(kk+1));
        data2.fl=data2all.fl(endpts(kk):endpts(kk+1));
        data2.trans=data2all.trans(endpts(kk):endpts(kk+1));
        data2.oxygen=data2all.oxygen(endpts(kk):endpts(kk+1));
        data2.lon=data2all.lon(endpts(kk):endpts(kk+1));
        data2.lat=data2all.lat(endpts(kk):endpts(kk+1));
        data2.modcount=data2all.modcount(endpts(kk):endpts(kk+1));
        data2.time=data2all.time(endpts(kk):endpts(kk+1));
        data2.time0 = data2.time(1):1/24:data2.time(end)+1; data2.time0 = data2.time0(1:length(data2.time))';
        data2.datenum = data2.time0/24/3600+datenum([1970 1 1 0 0 0]);
        
        % Alter outfile name
        matname=[fnames{jj}(1:end-4),sprintf('_%03d',S) '.mat'];
        save(fullfile(rec.CTD.raw_cast,matname),'data2')
        S=S+1
    end
end


%     % Do whatever you want to the raw data
%
% for ii=1:length(fnames)
%     k = findstr('001_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=1; end
%         k = findstr('002_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=2; end
%         k = findstr('003_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=3; end
%         k = findstr('004_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=4; end
%         k = findstr('005_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=5; end
%         k = findstr('006_',fnames{ii});
%     if ~isempty(k),cast_number(ii)=6; end
%         k = findstr('007_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=7; end
% %         k = findstr('008_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=8; end
% %         k = findstr('009_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=9; end
% %         k = findstr('010_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=10; end
% %         k = findstr('011_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=11; end
% %         k = findstr('012_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=12; end
% %         k = findstr('013_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=13; end
% %         k = findstr('014_',fnames{ii});
% %     if ~isempty(k),castnumber(ii)=14; end
% end
%
% cast_number(222:229) = 7:14;
%
% cd('/Volumes/DataDrive1/Projects/LaJIT2014/matfiles')
% save('cast_number','cast_number')
%