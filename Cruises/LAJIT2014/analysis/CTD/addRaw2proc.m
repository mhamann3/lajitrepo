% 1/29/91 MMH
% Add raw data to the processed castfiles
% Run again with updates 2/2/15

setup_LAJIT2014

fnames_raw = help_FilenamesFromPath(rec.CTD.raw_cast,'.mat',0);
% fnames_proc = help_FilenamesFromPath(rec.CTD.proc_cast,'.mat',0);
fnames_proc = help_FilenamesFromPath(rec.CTD.MSA,'.mat',0);

for ii = 1:length(fnames_raw)
    disp(['Processed file: ', num2str(fnames_proc{ii})])
    load(fullfile(rec.CTD.MSA,fnames_proc{ii}))
    load(fullfile(rec.CTD.raw_cast,fnames_raw{ii}))
    disp(['Raw file: ', num2str(fnames_raw{ii})])
    figure(1)
    plot(datau.t1)
    drawnow
    %     pause
    
    save(fullfile(rec.CTD.proc_cast,fnames_proc{ii}),'datau','datad',...
        'datau_05m','datad_05m','data2')
end

