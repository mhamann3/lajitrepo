%ctd_ladcp_structures.m
%
% make a big structure combining all available  data for each station during
% LAJIT2014
%
% Based on Jen's CTD processing routine
% 
% Updated 2/1/15 MMH

%%
setup_LAJIT2014

% rootdir=['/Volumes/scienceparty_share/TTIDE-RR1501/data/'];
% ladcp_dir=[rootdir 'ladcp_processed/'];
% ctd_dir=[rootdir 'ctd_processed/'];
% %ctd_dir=[rootdir 'science_share/ctd/processed/'];
% outdir=[rootdir 'ctd_processed/'];
%%
rootdir=rec.cruise;
ctd_dir = rec.CTD.proc_cast;
outdir=fullfile(rec.CTD.mat,'gridded');

%% load ctd data

% cast 1:     001 to 028  (001_028)
% cast 2:     029 to 082  (002_054)
% cast 3:     083 to 122  (003_040)
% cast 4:     123 to 135  (004_013)
% cast 5:     135 to 182  (005_047)
% cast 6:     136 to 221  (006_039)
% cast 7:     222
% cast 8:     223
% cast 9:     224
% cast 10:    225
% cast 11:    226
% cast 12:    227
% cast 13:    228
% cast 14:    229



%% station names
Names{1}='S1';
Names{2}='S2';

for station=1:2
%% load ctd data

if station==1
    files=1:122;
elseif station==2
    files=123:221;  
end
 


%%
doup=1; % add the up-casts too?
disp(['Loading ctd data'])
nn=NaN*ones(3000,length(files)); 
clear C
todo = {'t1','t2','c1','c2','s1','s2','theta1','theta2','sigma1','sigma2',...
    'oxygen','trans','fl','depth','p',...
    'z','p','t1_hi','t2_hi','s1_hi',...
    's2_hi','z_hi','p_hi','datenum_down','t1_up','t2_up','c1_up','c2_up','s1_up','s2_up','datenum_up','p_up'};
for j=1:length(todo);
    C.(todo{j})=nn;
end

fnames=help_FilenamesFromPath(ctd_dir,'.mat',0);
ind=1;
for ifile=files; 
   
%         load([ctd_dir 'ttide_leg1_' sprintf('%03d',ifile) '.mat'])
load(fullfile(rec.CTD.proc_cast,fnames{ifile}))
disp(fullfile(rec.CTD.proc_cast,fnames{ifile}))
figure(1)
plot(datau_1m.t1)
    for ind=1:length(files)
%     if ifile>8
%     iz=1:length(datad_1m.t1);
%     C.s1_hi(iz,ind)=datad_1m.s1;
%     C.s2_hi(iz,ind)=datad_1m.s2;
%     C.t1_hi(iz,ind)=datad_1m.t1;
%     C.t2_hi(iz,ind)=datad_1m.t2;
%     C.z_hi(iz,ind)=datad_1m.depth;
%     C.p_hi(iz,ind)=datad_1m.p;
%     end
    iz1=1:length(datad_1m.t1);
    C.s1(iz1,ind)=datad_1m.s1;
    C.s2(iz1,ind)=datad_1m.s2;
    C.t1(iz1,ind)=datad_1m.t1;
    C.t2(iz1,ind)=datad_1m.t2;
    C.c1(iz1,ind)=datad_1m.c1;
    C.c2(iz1,ind)=datad_1m.c2;
    C.z(iz1,ind)=datad_1m.depth;
    C.p(iz1,ind)=datad_1m.p;
    C.theta1(iz1,ind)=datad_1m.theta1;
    C.theta2(iz1,ind)=datad_1m.theta2;
    C.sigma1(iz1,ind)=datad_1m.sigma1;
    C.sigma2(iz1,ind)=datad_1m.sigma2;
    C.oxygen(iz1,ind)=datad_1m.oxygen;
    C.trans(iz1,ind)=datad_1m.trans;
    C.fl(iz1,ind)=datad_1m.fl;
    
    C.lon(ind)=nanmean(datad_1m.lon);
    C.lat(ind)=nanmean(datad_1m.lat);
    C.datenum_down(iz1,ind)=datad_1m.datenum;
    C.cast(ind)=ifile;
    
    if doup
        iz1=1:length(datau_1m.t1);
      C.t1_up(iz1,ind)=datau_1m.t1;  
      C.t2_up(iz1,ind)=datau_1m.t2;  
      C.c1_up(iz1,ind)=datau_1m.c1;  
      C.c2_up(iz1,ind)=datau_1m.c2;  
      C.s1_up(iz1,ind)=datau_1m.s1;  
      C.s2_up(iz1,ind)=datau_1m.s2;  
      C.datenum_up(iz1,ind)=datau_1m.datenum;
      C.p_up(iz1,ind)=datau_1m.p;
          C.theta1_up(iz1,ind)=datau_1m.theta1;
    C.theta2_up(iz1,ind)=datau_1m.theta2;
    C.sigma1_up(iz1,ind)=datau_1m.sigma1;
    C.sigma2_up(iz1,ind)=datau_1m.sigma2;
    C.oxygen_up(iz1,ind)=datau_1m.oxygen;
    C.trans_up(iz1,ind)=datau_1m.trans;
    C.fl_up(iz1,ind)=datau_1m.fl;
    end  % if doup

    end
    
end


%% so many nans from silly package carrying water. interpolate over them. 
[n,m]=size(C.t1);
for ii=1:m
    ig=find(~isnan(C.t1(:,ii))&~isnan(C.t2(:,ii)));
    C.t1(:,ii)=interp1(C.p(ig,ii),C.t1(ig,ii),C.p(:,ii));
    C.t2(:,ii)=interp1(C.p(ig,ii),C.t2(ig,ii),C.p(:,ii));
    C.c1(:,ii)=interp1(C.p(ig,ii),C.c1(ig,ii),C.p(:,ii));
    C.c2(:,ii)=interp1(C.p(ig,ii),C.c2(ig,ii),C.p(:,ii));
    C.s1(:,ii)=interp1(C.p(ig,ii),C.s1(ig,ii),C.p(:,ii));
    C.s2(:,ii)=interp1(C.p(ig,ii),C.s2(ig,ii),C.p(:,ii));
    ig=find(~isnan(C.t1_up(:,ii))&~isnan(C.t2_up(:,ii))&~isnan(C.p(:,ii)));
    C.t1_up(:,ii)=interp1(C.p(ig,ii),C.t1_up(ig,ii),C.p(:,ii));
    C.t2_up(:,ii)=interp1(C.p(ig,ii),C.t2_up(ig,ii),C.p(:,ii));
    C.c1_up(:,ii)=interp1(C.p(ig,ii),C.c1_up(ig,ii),C.p(:,ii));
    C.c2_up(:,ii)=interp1(C.p(ig,ii),C.c2_up(ig,ii),C.p(:,ii));
    C.s1_up(:,ii)=interp1(C.p(ig,ii),C.s1_up(ig,ii),C.p(:,ii));
    C.s2_up(:,ii)=interp1(C.p(ig,ii),C.s2_up(ig,ii),C.p(:,ii));

end


%% make the appropriate depth
C.z=nanmean([C.z],2); 
C.p=nanmean([C.p],2); 
todo = {'t1','t2','s1','s2','c1','c2','z','p','datenum_down'};
ig=find(~isnan(C.z)); iz=1:max(ig);
for j=1:length(todo);
    C.(todo{j})=C.(todo{j})(iz,:);
end
if doup
todo = {'t1_up','t2_up','c1_up','c2_up','datenum_up','p_up','s1_up','s2_up'};
for j=1:length(todo);
    C.(todo{j})=C.(todo{j})(iz,:);
end
end

C.z_hi=nanmean([C.z_hi],2); 
C.p_hi=nanmean([C.p_hi],2); 
todo = {'t1_hi','t2_hi','s1_hi','s2_hi','z_hi','p_hi'};
ig=find(~isnan(C.p_hi)); iz=1:max(ig);
for j=1:length(todo);
    C.(todo{j})=C.(todo{j})(iz,:);
end


% occasionally nans in pressure vector
C.p=sw_pres(C.z,nanmean(C.lat));
C.p_hi=sw_pres(C.z_hi,nanmean(C.lat));


%% s1 or s2 flag to choose a best guess "Good" t/s pair for each profile
%
%%
doplots=0;
if doplots
    figure(1); clf; fig_setup(0);
    subplot(131); imagesc(C.s1);caxis([34.4 35]); colorbar; fg
    subplot(132); imagesc(C.s2);caxis([34.4 35]); colorbar; fg
    subplot(133); imagesc(C.s2-C.s1); caxis([-1 1]*.03); colorbar; fg
    colormap(redblue)
end

%%

% some bad data. 1 for s1, 2 for s2
C.t=C.t1;
C.s=C.s1; 
C.c=C.c1;
C.t_hi=C.t1_hi;
C.s_hi=C.s1_hi;

C.s12flag=ones(size(C.lat));
if station==1;
%    C.s12flag(17:24)=2;
end

i2=find(C.s12flag==2);
C.s(:,i2)=C.s2(:,i2);
C.t(:,i2)=C.t2(:,i2);
C.c(:,i2)=C.c2(:,i2);
C.sgth = sw_pden(C.s,C.t,C.p,0)-1e3;

if doup
    C.t_up=C.t1_up; C.c_up=C.c1_up;
    C.t_up(:,i2)=C.t2_up(:,i2);
    C.c_up(:,i2)=C.c2_up(:,i2);
end  

%C.yday=C.datenum-datenum([2010 1 1 0 0 0]);

%% a few more useful quantities

% compute nsq from sorted gridded data, tis a bit more smoothed
a=1; b=ones(40,1)/40;
s0=C.s*NaN; t0=s0; 
for iy=1:length(C.lon); 
    ig=find(~isnan(C.sgth(:,iy)));
    [xxx,isg]=sort(C.sgth(ig,iy));
    s0(ig,iy)=nanfilt(b,a,C.s(ig(isg),iy));
    t0(ig,iy)=nanfilt(b,a,C.t(ig(isg),iy));
end
[n2,q,p_ave] = sw_bfrq(s0,t0,C.p_up,ones(size(C.p_up,1),1)*C.lat);
ig=find(~isnan(p_ave(:,2)));
n2_smooth=interp1(p_ave(ig,2),n2(ig,:),C.p);

C.n2_smoothed=nanmean(n2_smooth,2);
C.n2_comment='based on station average, smoothed over 40 m';


%% save temporarily
save(fullfile(outdir,[ 'CTD_', Names{station}, '.mat']),'C')
end

% %% mode shapes
% % create smoothed profiles for mode shapes
% a=1; b=ones(40,1)/40;
% p0=[0:5:max(C.p)]'; ig=find(~isnan(C.p)); [xx,ig0]=unique(C.p(ig)); ig=ig(ig0);
% t0=interp1(C.p(ig),nanfilt(b,a,nanmean(C.t(ig,:),2)),p0(:));
% s0=interp1(C.p(ig),nanfilt(b,a,nanmean(C.s(ig,:),2)),p0(:));
% 
% %
% ig=find(~isnan(t0)&~isnan(s0)&~isnan(p0)); s0=s0(ig); t0=t0(ig); p0=p0(ig);
% [c2, Psi, G, N2, Pmid] = VERT_FSFB(t0,s0,p0);
% 
% C.mode_speeds=sqrt(c2(1:10));
% C.mode_shapes=interp1(p0(:),Psi(:,1:10),C.p); 
% C.mode_comment='Based on smoothed N2 profile';
% 
% 
% 
% %% spectral plots to see overturn noise levels
% dospectra=1;
% dz=nanmean(diff(C.z));dzhi=nanmean(diff(C.z_hi));
% m0=[.02:.02:12];
% Ptot_t=NaN*ones(length(C.lat),length(m0)); Ptot_r=Ptot_t; Ptot_t0=Ptot_t;Ptot_r0=Ptot_t; Ptot_s0=Ptot_t; Ptot_s=Ptot_t;
% if dospectra
%     sgth_hi = sw_pden(C.s_hi,C.t_hi,C.p_hi,0)-1e3;
%     for ii=1:length(C.lat)
%         ig=find(~isnan(C.t(:,ii))&~isnan(C.sgth(:,ii))&~isnan(C.s(:,ii))); 
%         if length(ig)>.5*length(C.z)
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(C.t(ig,ii)))/dz,dz,'t',1,0,64);
%             Ptot_t(ii,:)=interp1(m,Ptot,m0);
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(C.sgth(ig,ii)))/dz,dz,'t',1,0,64);
%             Ptot_r(ii,:)=interp1(m,Ptot,m0);
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(C.s(ig,ii)))/dz,dz,'t',1,0,64);
%             Ptot_s(ii,:)=interp1(m,Ptot,m0);
%         end
%         ig=find(~isnan(C.t_hi(:,ii))&~isnan(sgth_hi(:,ii))&~isnan(C.s_hi(:,ii))); 
%         if length(ig)>.5*length(C.z_hi)
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(C.t_hi(ig,ii)))/dzhi,dzhi,'t',1,0,64);
%             Ptot_t0(ii,:)=interp1(m,Ptot,m0);
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(sgth_hi(ig,ii)))/dzhi,dzhi,'t',1,0,64);
%             Ptot_r0(ii,:)=interp1(m,Ptot,m0);
%             [Pcw,Pccw,Ptot,m] = psd_jen(detrend(diff(C.s_hi(ig,ii)))/dzhi,dzhi,'t',1,0,64);
%             Ptot_s0(ii,:)=interp1(m,Ptot,m0);
%         end
%     end
% end
% 
% %%
% if dospectra
% figure(1);clf; set(gcf,'paperposition',[.5 .5 15 10]); wysiwyg
% mmax=3; 
% phit=5e-6; phir=3e-7; phis=5e-6;
% phit=.6e-5; phir=.4e-6; phis=7e-6;
% %phit=2e-6; phir=1.5e-7; phis=5e-6;
% phit=2e-5; phir=.6e-6; phis=7e-6;
% phit=.2e-5; phir=.08e-6; phis=3e-6;
% 
% 
% 
% subplot(231)
% loglog(m0,nanmean(Ptot_t0)./m0.^2./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_t)./m0.^2./sinc(m0*dz/2/pi).^2,m0,phit*ones(size(m0))); fg;
% hold on; plot(mmax*[1 1],[1e-6 1e0],'c--')
% set(gca,'xlim',[.02 15])
% text(.2,1e-0,['Temperature noise level  ' num2str(phit,2)])
% text(.2,1e-1,['sigma =  ' num2str(sqrt(phit*mmax),2)])
% ylabel('^({\circ}C m^{-1})^2')
% 
% subplot(232)
% loglog(m0,nanmean(Ptot_s0)./m0.^2./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_s)./m0.^2./sinc(m0*dz/2/pi).^2,m0,phis*ones(size(m0))); fg;
% hold on; plot(mmax*[1 1],[1e-6 1e0],'c--')
% set(gca,'xlim',[.02 15])
% text(.2,1e-0,['salinity noise level  ' num2str(phis,2)])
% text(.2,1e-1,['sigma =  ' num2str(sqrt(phis*mmax),2)])
% ylabel('^({\circ}C m^{-1})^2')
% 
% subplot(233)
% loglog(m0,nanmean(Ptot_r0)./m0.^2./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_r)./m0.^2./sinc(m0*dz/2/pi).^2,m0,phir*ones(size(m0)),'b'); fg;
% hold on; plot(mmax*[1 1],[1e-7 1e-2],'c--')
% set(gca,'xlim',[.02 15])
% text(.2,1e-2,['Rho noise level  ' num2str(phir,2)])
% text(.2,1e-3,['sigma =  ' num2str(sqrt(phir*mmax),2)])
% xlabel('\omega / s^{-1}')
% %print(gcf,'-djpeg','/Users/jen/projects/iwap/notes/mixing/noise_spectra')
% 
% subplot(234)
% loglog(m0,nanmean(Ptot_t0)./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_t)./sinc(m0*dz/2/pi).^2,m0,phit*ones(size(m0)).*m0.^2); fg;
% hold on; plot(mmax*[1 1],[1e-6 1e0],'c--')
% set(gca,'xlim',[.02 15])
% axis([.02 15 1e-5 1e-2])
% 
% subplot(235)
% loglog(m0,nanmean(Ptot_s0)./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_s)./sinc(m0*dz/2/pi).^2,m0,phis*ones(size(m0)).*m0.^2); fg;
% hold on; plot(mmax*[1 1],[1e-6 1e0],'c--')
% set(gca,'xlim',[.02 15])
% axis([.02 15 1e-7 1e-3])
% 
% ylabel('^({\circ}C m^{-1})^2')
% subplot(236)
% loglog(m0,nanmean(Ptot_r0)./sinc(m0*dzhi/2/pi).^2,m0,nanmean(Ptot_r)./sinc(m0*dz/2/pi).^2,m0,phir*ones(size(m0)).*m0.^2,'b'); fg;
% hold on; plot(mmax*[1 1],[1e-7 1e-2],'c--')
% axis([.02 15 1e-7 1e-2])
% 
% xlabel('\omega / s^{-1}')
% 
% 
% sigma_t=sqrt(phit*mmax);
% sigma_rho=sqrt(phir*mmax);
% %sigma_t0=sqrt(phit*10); %for hi resolution t
% 
% else
% 
% % from previous looks at spectra, so it's automated
%  if station==1; sigma_t=3e-3;sigma_rho=6e-4; 
%  elseif station==2; sigma_t=4e-3;sigma_rho=1e-3;
%  elseif station==3; sigma_t=3e-3;sigma_rho=6e-4;
%  elseif station>=4&station<=7; sigma_t=1e-3;sigma_rho=4e-4;
%  elseif station==8; sigma_t=2.5e-3;sigma_rho=6e-4; 
%  elseif station==9; sigma_t=1e-3;sigma_rho=4e-4;
%  elseif station>9; sigma_t=2e-3; sigma_rho=4e-4;
%  end
% 
% end
%     
% %% overturns
% % calculate separately for each sensor pair
% disp(['Calculating overturns'])
% 
% %phit=.6e-5; phir=.4e-6;
% sigma_t=0.0042; sigma_rho=0.0011;
% sigma_t=.0024/2;  sigma_rho=4.9e-4/2;
% % setup sizes   
% C.eps_t1=NaN*C.t; C.eps_t1_hi=NaN*C.t1_hi; C.eps_rho1=C.eps_t1; 
% C.eps_t2=NaN*C.t; C.eps_t2_hi=NaN*C.t1_hi; C.eps_rho2=C.eps_t1;
% Lot_rho1=NaN*C.t; Lt_rho1=NaN*C.t;
% Lot_rho2=NaN*C.t; Lt_rho2=NaN*C.t;
% Lot_t1=NaN*C.t; Lt_t1=NaN*C.t;
% Lot_t2=NaN*C.t; Lt_t2=NaN*C.t;
% 
% 
% 
% for idrop=1:length(C.lat)
%    if rem(idrop,20)==0; disp(idrop); end
%  
%     in=find(~isnan(C.t1(:,idrop))&~isnan(C.s1(:,idrop)));
%      if length(in)<30; continue; end
%      % t1,s1 for 1 m data
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p(in),C.t1(in,idrop),C.s1(in,idrop),C.lat(idrop),1,3,sigma_t,0); 
%     C.eps_t1(in,idrop) = Epsout;
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p(in),C.t1(in,idrop),C.s1(in,idrop),C.lat(idrop),0,3,sigma_rho,0); 
%     C.eps_rho1(in,idrop) = Epsout;
%     Lot_rho1(in,idrop)=Lot; Lt_rho1(in,idrop)=Lttot;
%     
%     
%      % t1,s1 for 25cm data
%     in=find(~isnan(C.t1_hi(:,idrop))&~isnan(C.s1_hi(:,idrop)));
%      if length(in)<30; continue; end
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p_hi(in),C.t1_hi(in,idrop),C.s1_hi(in,idrop),C.lat(idrop),1,2,sigma_t*2,0); 
%     C.eps_t1_hi(in,idrop) = Epsout;
%     Lot_t1(in,idrop)=Lot; Lt_t1(in,idrop)=Lttot;
%     
%     in=find(~isnan(C.t2(:,idrop))&~isnan(C.s2(:,idrop)));
%      if length(in)<30; continue; end
%      % t2,s2 for 1m data
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p(in),C.t2(in,idrop),C.s2(in,idrop),C.lat(idrop),1,3,sigma_t,0); 
%     C.eps_t2(in,idrop) = Epsout;
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p(in),C.t2(in,idrop),C.s2(in,idrop),C.lat(idrop),0,3,sigma_rho,0); 
%     C.eps_rho2(in,idrop) = Epsout;
%     Lot_rho2(in,idrop)=Lot; Lt_rho2(in,idrop)=Lttot;
%         
%     
%      % t2,s2 for 25 m data
%     in=find(~isnan(C.t2_hi(:,idrop))&~isnan(C.s2_hi(:,idrop)));
%      if length(in)<30; continue; end
%     [Epsout,Lmin,Lot,runlmax,Lttot]=compute_overturns2(C.p_hi(in),C.t2_hi(in,idrop),C.s2_hi(in,idrop),C.lat(idrop),1,2,sigma_t*2,0); 
%     C.eps_t2_hi(in,idrop) = Epsout;
%     Lot_t2(in,idrop)=Lot; Lt_t2(in,idrop)=Lttot;
% end
% 
% %% take the least value for each
% %eps_t=min(C.eps_t1,C.eps_t2);
% %eps_t=interp1(C.z_hi,min(C.eps_t1_hi,C.eps_t2_hi),C.z);
% %eps_rho=min(C.eps_rho1,C.eps_rho2);
% [pmax,ipmax]=max(C.z_hi);
% eps_t=interp1(C.z_hi(1:ipmax),(C.eps_t1_hi(1:ipmax,:)),C.z);
% eps_rho=C.eps_rho1;
% 
% eps_min=min(eps_t,eps_rho);
% 
% 
% 
% C.epsilon_OT=eps_min;
% C.epsilon_OT_t=eps_t;
% C.epsilon_OT_rho=eps_rho;
% 
% %% prune a bit
% 
% C1.down.z=C.z; C1.down.p=C.p; C1.down.dnum=nanmean(C.datenum_down); C1.down.t=C.t; C1.down.s=C.s; C1.down.sgth=C.sgth;
% C1.down.n2=C.n2_smoothed; C1.down.eps_t=C.epsilon_OT_t; C1.down.eps_rho=C.epsilon_OT_rho;
% C1.down.lat=C.lat; C1.down.lon=C.lon;
% 
% C1.up.z=C.z; C1.up.p=C.p; C1.up.dnum=nanmean(C.datenum_up); C1.up.t=C.t1_up; C1.up.s=C.s1_up;
% C1.up.n2=C.n2_smoothed; C1.up.eps_t=NaN*C.epsilon_OT_t; C1.up.eps_rho=NaN*C.epsilon_OT_rho;
% C1.up.sgth = sw_pden(C1.up.s,C1.up.t,C1.up.p,0)-1e3;
% C1.up.lat=C.lat; C1.up.lon=C.lon;
% 
% 
% C1.all.z=C1.down.z; C1.all.p=C1.down.p;C1.all.n2=C1.down.n2;
% todo={'t','s','sgth','eps_t','eps_rho'};
% nn=length(C.lat); nn2=2*nn;
% for ido=1:length(todo)
%     C1.all.(todo{ido})=NaN*ones(length(C1.all.z),2*nn);
%     C1.all.(todo{ido})(:,1:2:(nn2-1))=C1.down.(todo{ido});
%     C1.all.(todo{ido})(:,2:2:(nn2))=C1.up.(todo{ido});
% end
% todo={'dnum','lat','lon'};
% for ido=1:length(todo)
%     C1.all.(todo{ido})=NaN*ones(1,2*nn);
%     C1.all.(todo{ido})(1:2:(nn2-1))=C1.down.(todo{ido});
%     C1.all.(todo{ido})(2:2:(nn2))=C1.up.(todo{ido});
% end
%   
% %% save interim version
% C=C1;
% save([outdir 'CTD_' Names{station} '.mat'],'C')
% 
% end
% 
% break
% %% add LADCP data
% clear L
% ladcp_dir=[rootdir 'ladcp/data/processed/'];
% z0=[0:8:max(C.z)]';
% L.u=NaN*ones(length(z0),length(files)); L.v=L.u; 
% ind=1;
% for ifile=files
%     load([ladcp_dir 'ttide_leg1_' int2str(ifile) '.mat'])
%  nz=length(dr.z);
% L.u(:,ind)=interp1(dr.z,dr.u,z0); 
% L.v(:,ind)=interp1(dr.z,dr.v,z0);;
% 
% ind=ind+1;
% end
% L.z=z0(:); L.p=sw_pres(L.z,nanmean(C.lat));
% L.datenum=nanmean([C.datenum_down;C.datenum_up]);
% 
% % some bad ladcp data for ifiles 66,67,68 when battery ran out!! :( 
% ibad=[]; ibad=[ibad,find(files==66),find(files==67),find(files==68)];
% L.u(:,ibad)=NaN; L.v(:,ibad)=NaN;
% %
% %% save interim version
% save([outdir 'LADCP_' Names{station} '.mat'],'L')
% 
% %%
% C.author='Jen';
% C.creation=datestr(now);
% %% make output file
% clear All down up 
% 
% todo = {'z','p','t','c','s','sgth','n2_smoothed','n2_comment','mode_speeds','mode_shapes','epsilon_OT','epsilon_OT_t','epsilon_OT_rho','lat','lon','cast','author','creation'};
% for j=1:length(todo);
%     down.(todo{j})=C.(todo{j});
% end
% down.datenum=C.datenum_down;
% down.datenum_avg=nanmean(C.datenum_down);
% 
% if doup
%     up.t=C.t_up; up.c=C.c_up; up.p=C.p_up; up.datenum=C.datenum_up;
%     up.s=sw_salt(10*up.c/sw_c3515, up.t,up.p);
%     up.sgth = sw_pden(up.s,up.t,up.p,0)-1e3;
% 
%    up.datenum_avg=nanmean(C.datenum_up);
% end
% 
% 
% %
% All.ctd_down=down;
% All.ctd_up=up;
% All.ladcp=L;
% 
% %% save
% save([outdir 'Station_' Names{station} '.mat'],'All')
% 
% %% energy flux
% 
% % use combination of up and down-cast. 
% EF.sgth=.5*(All.ctd_down.sgth+All.ctd_up.sgth);
% for ii=1:length(All.ctd_down.datenum_avg)
%     ig=find(~isnan(EF.sgth(:,ii)));
%     EF.sgth(:,ii)=interp1(All.ctd_down.z(ig),EF.sgth(ig,ii),All.ctd_down.z);
% end
% rhop=EF.sgth-nanmean(EF.sgth,2)*ones(1,length(All.ctd_up.datenum_avg));
% iiz=find(~isnan(mean(rhop,2)));
% pp=NaN*rhop;
% pp(iiz,:)=flipud(cumsum(flipud(rhop(iiz,:))))*9.81*mean(diff(All.ctd_down.z));
% pp=pp-ones(length(All.ctd_down.z),1)*nanmean(pp);
% ppi=interp1(All.ctd_down.z,pp,L.z);
% up=L.u-nanmean(L.u,2)*ones(1,length(L.datenum));
% vp=L.v-nanmean(L.v,2)*ones(1,length(L.datenum));
% figure(34); clf; fig_setup(0)
% subplot(121)
% imagesc(L.datenum-min(L.datenum),L.z,up.*ppi); caxis([-1 1]*6); colorbar; shg
% subplot(122)
% imagesc(L.datenum-min(L.datenum),L.z,vp.*ppi);caxis([-1 1]*6); colorbar; shg
% colormap(redblue)
% 
% All.ladcp.efu=up.*ppi;
% All.ladcp.efv=vp.*ppi;
% 
% figure(35); fig_setup(0); clf
% plot(All.ladcp.z*0,All.ladcp.z,nanmean(All.ladcp.efu,2),All.ladcp.z,nanmean(All.ladcp.efv,2),All.ladcp.z); axis ij
% 
% %% save
% save([outdir 'Station_' Names{station} '.mat'],'All')
