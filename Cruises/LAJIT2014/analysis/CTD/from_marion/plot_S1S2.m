% Plot S1 & S2

clear all
close all
clc

set(0,'defaultaxesfontsize',15,'defaultaxeslinewidth',0.7,...
'defaultlinelinewidth',0.7,'defaultpatchlinewidth',0.7)
%% CTD Plots

ctddir='/Users/marionsofiaalberty/MATLAB/Coursework/Proposal/LAJIT/CTD/procMSA/';
ctdlist = dirs(fullfile(ctddir,'*.mat'));

% Remove raw from directory
ii=[];
for i=1:length(ctdlist)
    str=ctdlist(i).name;
    if strcmp('raw.mat',str(end-6:end))
        ii=[ii i];
    elseif str2num(str(1:3)) > 6
        ii=[ii i];
    end
end
ctdlist(ii)=[];
total=length(ctdlist);
clear i ii str

% split between S1 and S2
k=123;


% Parameters for S1 & S2
ds1=160;        % [m]
ns1=321;        % [bins]
ls1=length(ctdlist(1:k-1));
ds2=200;        % [m]
ns2=401;        % [bins]
ls2=length(ctdlist(k:end));
% Initialize matricies
t1=nan(ns1,ls1);
s1=t1;
sig1=t1;
ox1=t1;
dep1=0:0.5:ds1;
tim1=nan(1,ls1);
t2=nan(ns2,ls2);
s2=t2;
sig2=t2;
ox2=t2;
dep2=0:0.5:ds2;
tim2=nan(1,ls2);

for i=1:k-1
    load([ctddir ctdlist(i).name],'datad_05m')
    if length(datad_05m.t1) <= ns1
        BB=length(datad_05m.t1);
    else
        BB=ns1;
    end
    % Add data to matrix
    if sum(isnan(datad_05m.s1)) < sum(isnan(datad_05m.s2))
        t1(1:BB,i)=datad_05m.t1(1:BB);
        s1(1:BB,i)=datad_05m.s1(1:BB);
        sig1(1:BB,i)=datad_05m.sigma1(1:BB);
    else
        t1(1:BB,i)=datad_05m.t2(1:BB);
        s1(1:BB,i)=datad_05m.s2(1:BB);
        sig1(1:BB,i)=datad_05m.sigma2(1:BB);
    end
    ox1(1:BB,i)=datad_05m.oxygen(1:BB);
    tim1(i)=nanmean(datad_05m.datenum);
end

ii=1;
for i=k:total
    load([ctddir ctdlist(i).name],'datad_05m')
    if length(datad_05m.t1) <= ns2
        BB=length(datad_05m.t1);
    else
        BB=ns2;
    end
    % Add data to matrix
    if sum(isnan(datad_05m.s1)) < sum(isnan(datad_05m.s2))
        t2(1:BB,ii)=datad_05m.t1(1:BB);
        s2(1:BB,ii)=datad_05m.s1(1:BB);
        sig2(1:BB,ii)=datad_05m.sigma1(1:BB);
    else
        t2(1:BB,ii)=datad_05m.t2(1:BB);
        s2(1:BB,ii)=datad_05m.s2(1:BB);
        sig2(1:BB,ii)=datad_05m.sigma2(1:BB);
    end
    ox2(1:BB,ii)=datad_05m.oxygen(1:BB);
    tim2(ii)=nanmean(datad_05m.datenum);
    ii=ii+1;
end

% Plot
% figure('color','white')
% contourf(tim1,dep1,t1,50)
% colormap(jet(100))
% shading flat
% datetick
% colorbar
% title('S1 (12/6/14) Temperature [deg C]')
% ylabel('Depth [m]')
% axis ij tight
% 
% figure('color','white')
% contourf(tim2,dep2,t2,50)
% colormap(jet(100))
% shading flat
% datetick
% colorbar
% title('S2 (12/7/14) Temperature [degC]')
% ylabel('Depth [m]')
% axis ij tight

% figure('color','white')
% contourf(tim1,dep1,s1,75)
% colormap(jet(75))
% shading flat
% datetick
% colorbar
% title('S1 Salinity [PSU]')
% ylabel('Depth [m]')
% axis ij tight
% 
% figure('color','white')
% contourf(tim2,dep2,s2,75)
% colormap(jet(75))
% shading flat
% datetick
% colorbar
% title('S2 Salinity [PSU]')
% ylabel('Depth [m]')
% axis ij tight

% figure('color','white')
% contourf(tim1,dep1,sig1,75)
% colormap(jet(75))
% shading flat
% datetick
% colorbar
% title('S1 Density [kg/m^3]')
% ylabel('Depth [m]')
% axis ij tight
% 
% figure('color','white')
% contourf(tim2,dep2,sig2,75)
% colormap(jet(75))
% shading flat
% datetick
% colorbar
% title('S2 Density [kg/m^3]')
% ylabel('Depth [m]')
% axis ij tight

figure('color','white')
contourf(tim1,dep1,ox1,50)
colormap(redblue(50))
shading flat
datetick
colorbar
title('S1 Oxygen [ml/L]')
ylabel('Depth [m]')
axis ij tight

figure('color','white')
contourf(tim2,dep2,ox2,50)
cm=colormap(redblue(50)); cm=flipud(cm);
shading flat
datetick
colorbar
title('S2 Oxygen [ml/L]')
ylabel('Depth [m]')
axis ij tight


%% LADCP with/without SADCP comparison plots 

% % LADCP data and path
% path='/Users/marionsofiaalberty/MATLAB/Coursework/Proposal/LAJIT/LADCP/processed/';
% list = dirs(fullfile(path,'*.mat'));
% 
% % SADCP data
% load('/Users/marionsofiaalberty/MATLAB/Coursework/Proposal/LAJIT/SADCP/sadcp.mat')
% 
% for i=1:length(list)
%     % Load LADCP data
%     load([path list(i).name],'dr')
%     
%     % Find overlapping SADCP
%     ii=find(tim_sadcp > dr.tim(1),1,'first')-1;
%     kk=find(tim_sadcp > dr.tim(end),1,'first');
%     
%     % Plot u
%     figure('color','white')
%     plot(dr.u,dr.z,'k',dr.u_shear_method,dr.z,'r',u_sadcp(:,ii:kk),z_sadcp,'g')
%     axis ij tight
%     legend('Inverse Method','Shear Method','SADCP')
%     xlabel('Velocity [m/s]')
%     ylabel('Dspth [m]')
%     title(' U (East) Velocity Comparison')
%     eval(['print -dpng ' path 'figures/' list(i).name(1:end-4) '_ucomp.png'])
%     close all
%     
%     figure('color','white')
%     plot(dr.v,dr.z,'k',dr.v_shear_method,dr.z,'r',v_sadcp(:,ii:kk),z_sadcp,'g')
%     axis ij tight
%     legend('Inverse Method','Shear Method','SADCP')
%     xlabel('Velocity [m/s]')
%     ylabel('Dspth [m]')
%     title(' V (North) Velocity Comparison')
%     eval(['print -dpng ' path 'figures/' list(i).name(1:end-4) '_vcomp.png'])
%     close all
% end


%% Depth-Time LADCP Plots

% % Parameters for S1 & S2
% ds1=172;        % [m]
% ns1=ds1/4+1;        % [bins]
% ls1=length(ctdlist(1:k-1));
% ds2=200;        % [m]
% ns2=ds2/4+1;        % [bins]
% ls2=length(ctdlist(k:end));
% % Initialize matricies
% u1=nan(ns1,ls1);
% v1=t1;
% dep1=4:4:ds1;
% tim1=nan(1,ls1);
% t2=nan(ns2,ls2);
% s2=t2;
% sig2=t2;
% ox2=t2;
% dep2=0:0.5:ds2;
% tim2=nan(1,ls2);










