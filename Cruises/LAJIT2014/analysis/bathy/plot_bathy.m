load ljcan_merged 
load('locations.mat')

figure(11); clf
colormap(ocean(128))

indexlat = find(ljcan.lat>=32.85 & ljcan.lat<=32.88);
indexlon = find(ljcan.lon>=-117.28 & ljcan.lon<=-117.255);

lon = ljcan.lon(indexlon); lat = ljcan.lat(indexlat);
lon = ljcan.lon(indexlon); lat = ljcan.lat(indexlat);
depth = ljcan.Z(indexlat,indexlon);
surf(lon,lat,depth,'edgecolor','none'); view(0,90)

hold on;
plot(-(117+15.441/60),32+52.022/60,'sk')

piershore = [-(117+15/60+15.25/3600), 32+51/60+58.23/3600];
pierend = [-(117+15/60+26.28/3600), 32+52/60+1.24/3600];

c = plot([pierend(1) piershore(1)],[pierend(2) piershore(2)],'k-');
lw(c,6)
plot(pos.sta1(1),pos.sta1(2),'om')
plot(pos.sta2(1),pos.sta2(2),'om')
plot(pos.M1(1),pos.M1(2),'sm')

npts=100;
bathyline.sta2lat=linspace(pos.sta2end1(1),pos.sta2end2(1),npts);
bathyline.sta2lon=linspace(pos.sta2end1(2),pos.sta2end2(2),npts);
idx = find(ljcan.lon>pos.sta2end1(2) & ljcan.lon<pos.sta2end2(2));
idy = find(ljcan.lat>pos.sta2end1(1) & ljcan.lat<pos.sta2end2(1));
bathyline.sta1depth=interp2( ljcan.lon(idx),((ljcan.lat(idy))),ljcan.Z(idy,idx),bathyline.sta2lon,bathyline.sta2lat);

%%
lj.lat = ljcan.lat;
lj.lonmin=ljcan.lon(indexlon(1));
lj.latmin=ljcan.lat(indexlat(1));
lj.lonmax=ljcan.lon(indexlon(2));
lj.latmax=ljcan.lat(indexlat(2));
lj.lon = ljcan.lon;
lj.depth = ljcan.Z;
lj.linelon = [pos.sta2end1(1) pos.sta2end2(1) pos.sta1end1(1) pos.sta1end2(1)];
lj.linelat = [pos.sta2end1(2) pos.sta2end2(2) pos.sta1end1(2) pos.sta1end2(2)];
lj.type='La Jolla Canyon';

lj = bathyline(lj);