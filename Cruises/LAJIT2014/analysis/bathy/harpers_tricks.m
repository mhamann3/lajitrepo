%-- 2/2/15, 7:03 PM --%
addMaddie('lajit')
load_shipboard_ADCP
load_shipboard_ADCP_plus
load_shipboard_ADCP
plot_bathy
surf(ljcan.lon,ljcan.lat,ljcan.Z)
view(0,90)
shading flat
ljcan
surfl(ljcan.lon,ljcan.lat,ljcan.Z);shading flat;camlight;rotate3d on
ls
ls
caxis
clf
skip=10;surfl(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end));shading flat;camlight;rotate3d on
axis tight
% get(gca,'dataaspectratio')
% round(ans)
% set(gca,'dataaspectratio',[1,1,1e4])
% set(gca,'dataaspectratio',[1,1,2e4])
set(gca,'dataaspectratio',[1,1,3e4])
pause
clf
% imagesc(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end)+gradient(ljcan.Z(1:skip:end,1:skip:end)));caxis
% imagesc(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end)+0*gradient(ljcan.Z(1:skip:end,1:skip:end)));caxis
% imagesc(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end)+0*gradient(ljcan.Z(1:skip:end,1:skip:end)));caxis([-600,0])
% imagesc(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end)+0*gradient(ljcan.Z(1:skip:end,1:skip:end)));caxis([-800,0])
imagesc(ljcan.lon(1:skip:end),ljcan.lat(1:skip:end),ljcan.Z(1:skip:end,1:skip:end)+10*gradient(ljcan.Z(1:skip:end,1:skip:end)));caxis([-800,0])
colormap(bone)
axis xy