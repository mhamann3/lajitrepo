% 1/30/15 MMH; update 1/31/15
% grids chipod data at stations 1 and 2 and adds new fields to 'sta1' and
% 'sta2' structures in chi_stations if they don't already exist
% WILL NEED TO REMOVE FIELDS AFTER FIXING THE CHIPOD PROCESSING AND RERUN
% CHI_STATIONS

clear tmp
setup_LAJIT2014

load(fullfile(rec.matfiles,'chi_stations.mat'))
pvar = 'eps1';
if strcmp(pvar,'eps1')
    clim = [-10,-4];
end
    
plotlog = 1;
% plotlog = 0;

try
tmp=eval(['sta1.chi_grid.',pvar]);
catch err
    if ~exist('tmp','var'), tmp=[]; end
end

if isempty(tmp)
    
%     sta1.avg.datevec = sta1.avg.datenum(1):1/24/60*5:sta1.avg.datenum(end);  % 5 minute grid
%     sta1.avg.Pvec = 0:1:ceil(max(sta1.avg.P))+5;    % 1 dbar grid in depth
%     [sta1.avg.dnum,sta1.avg.Pgrid] = meshgrid(sta1.avg.datevec,sta1.avg.Pvec);  % make grid coordinates
%     
    id = find(~isnan(eval(['sta1.avg.',pvar])));
    eval(['sta1.chi_grid.',pvar,' = griddata(sta1.avg.datenum(id),sta1.avg.P(id),sta1.avg.',pvar,'(id),sta1.avg.dnum,sta1.avg.Pgrid)']);
    
%     sta2.avg.datevec = sta2.avg.datenum(1):1/24/60*5:sta2.avg.datenum(end);  % 5 minute grid
%     sta2.avg.Pvec = 0:1:ceil(max(sta2.avg.P))+5;    % 1 dbar grid in depth
%     [sta2.avg.dnum,sta2.avg.Pgrid] = meshgrid(sta2.avg.datevec,sta2.avg.Pvec);  % make grid coordinates
%        
    id = find(~isnan(eval(['sta2.avg.',pvar])));
    eval(['sta2.chi_grid.',pvar,' = griddata(sta2.avg.datenum(id),sta2.avg.P(id),sta2.avg.',pvar,'(id),sta2.avg.dnum,sta2.avg.Pgrid)']);
       
end

figure(10)
if plotlog
contourf(sta1.avg.datevec,sta1.avg.Pvec,log10(eval(['sta1.chi_grid.',pvar])),20)
else
    contourf(sta1.avg.datevec,sta1.avg.Pvec,eval(['sta1.chi_grid.',pvar]),20)
end
shading flat
caxis(clim)
axis ij tight
title(['Station 1: ',pvar])
dynamicDateTicks
colorbar
bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours/'),['sta1_',pvar],'p',300,'fontsize',11)

figure(11)
if plotlog
contourf(sta2.avg.datevec,sta2.avg.Pvec,log10(eval(['sta2.chi_grid.',pvar])),20)
else
    contourf(sta2.avg.datevec,sta2.avg.Pvec,eval(['sta2.chi_grid.',pvar]),20)
end
shading flat
caxis(clim)
axis ij tight
title(['Station 2: ',pvar])
dynamicDateTicks
colorbar
title('station 2')
bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours/'),['sta2_',pvar],'p',300,'fontsize',11)


save(fullfile(rec.matfiles,'chi_stations'),'sta1','sta2')
