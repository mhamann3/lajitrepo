% 1/30/15 MMH
% Plot timeseries of 

function h = chi_station_plot(station,variable,ax);
if nargin<3
    hf = figure;
end

if nargin<2
    error('Specify variable and station (1 or 2)')
end

if station == 1, station = 'sta1'; 
elseif station == 2, station = 'sta2'; 
else error('Station must be 1 or 2'); end

eval('ezcf(