% 1/30/15 MMH; update 1/31/15
% grids chipod data at stations 1 and 2 and adds new fields to 'sta1' and
% 'sta2' structures in chi_stations if they don't already exist
% WILL NEED TO REMOVE FIELDS AFTER FIXING THE CHIPOD PROCESSING AND RERUN
% CHI_STATIONS

clear tmp
setup_LAJIT2014

load(fullfile(rec.matfiles,'chi_stations.mat'))
ctd_fields =     {'t1', 't2','c1','c2','s1','s2','theta1','theta2','sigma1',...
    'sigma2','oxygen','trans','fl','lon','lat','time','nscan','depth','p',...
    'datenum','N2','N2_20','dTdz','dTdz_20','N2_50','dTdz_50'};


% for jj=[1,5,7,9,11,12,13,21]
for jj=[12,13,21]
pvar = ctd_fields{jj};
% pvar = 'theta1';

% plotlog = 1;
plotlog = 0;

try
tmp=eval(['sta1.ctd_grid.',pvar]);
catch err
    if ~exist('tmp','var'), tmp=[]; end
end

if isempty(tmp)
    
%     sta1.ctd.datevec = nanmin(sta1.ctd.datenum):1/24/60*5:nanmax(sta1.ctd.datenum);  % 5 minute grid
%     sta1.ctd.Pvec = 0:1:ceil(max(sta1.ctd.depth))+5;    % 1 dbar grid in depth
%     [sta1.ctd.dnum,sta1.ctd.Pgrid] = meshgrid(sta1.ctd.datevec,sta1.ctd.Pvec);  % make grid coordinates
%     
    id = find(~isnan(eval(['sta1.ctd.',pvar])));
    eval(['sta1.ctd_grid.',pvar,' = griddata(sta1.ctd.datenum(id),sta1.ctd.depth(id),sta1.ctd.',pvar,'(id),sta1.ctd.dnum,sta1.ctd.Pgrid)']);
    
%     sta2.ctd.datevec = nanmin(sta2.ctd.datenum):1/24/60*5:nanmax(sta2.ctd.datenum);  % 5 minute grid
%     sta2.ctd.Pvec = 0:1:ceil(max(sta2.ctd.P))+5;    % 1 dbar grid in depth
%     [sta2.ctd.dnum,sta2.ctd.Pgrid] = meshgrid(sta2.ctd.datevec,sta2.ctd.Pvec);  % make grid coordinates
%        
    id = find(~isnan(eval(['sta2.ctd.',pvar])));
    eval(['sta2.ctd_grid.',pvar,' = griddata(sta2.ctd.datenum(id),sta2.ctd.depth(id),sta2.ctd.',pvar,'(id),sta2.ctd.dnum,sta2.ctd.Pgrid)']);
       
end

figure(10)
if plotlog
contourf(sta1.ctd.datevec,sta1.ctd.Pvec,log(eval(['sta1.ctd_grid.',pvar])),20)
else
    contourf(sta1.ctd.datevec,sta1.ctd.Pvec,eval(['sta1.ctd_grid.',pvar]),20)
end
shading flat
axis ij tight
dynamicDateTicks
colorbar
bdr_savefig(fullfile(rec.prpath,'chi_pods'),['sta1.avg_',pvar],'p',300)

figure(11)
if plotlog
contourf(sta2.ctd.datevec,sta2.ctd.Pvec,log(eval(['sta2.ctd_grid.',pvar])),20)
else
    contourf(sta2.ctd.datevec,sta2.ctd.Pvec,eval(['sta2.ctd_grid.',pvar]),20)
end
shading flat
axis ij tight
dynamicDateTicks
colorbar
title('station 2')
bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours'),['sta2_',pvar],'p',300)


save(fullfile(rec.matfiles,'chi_stations'),'sta1','sta2')
end
