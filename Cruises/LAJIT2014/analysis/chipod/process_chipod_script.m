
%% Now let's go through all of the casts.

setup_LAJIT2014

%load CTD profile
CTD_path=rec.CTD.proc_cast;
chi_processed_path=rec.chi.proc;

CTD_list=dir([CTD_path '/*.mat']);
t_extra = 2/84600;

% flag_offset = zeros(length(CTD_list),1);
% for ii=1:length(CTD_list) 
for ii=39
    
    load(fullfile(CTD_path, CTD_list(ii).name))
    %create a CTD time
    time_range=[min(data2.datenum)-t_extra max(data2.datenum)+t_extra];
    cast_suffix=num2str(1000+ii); cast_suffix=cast_suffix(2:4);
    datestr(time_range)
    for up_down_big=2
        % load chipod data
        short_labs={'up','down','big'};
        switch up_down_big
            case 1
                chi_path=fullfile(rec.chi.raw,'up/'); az_correction=-1;
                suffix='mlg';
                isbig=0;
                cal.coef.T1P=0.097;
                is_downcast=0;
            case 2
                chi_path=fullfile(rec.chi.raw,'dn/'); az_correction=1;
                suffix='mlg';
                isbig=0;
                cal.coef.T1P=0.097;
                is_downcast=1;
                %     			case 3
                %     				chi_path='../data/A16S/Chipod_CTD/';az_correction=1;
                %     				suffix='1002';
                %     				isbig=1;
                %     				cal.coef.T1P=0.105;
                %     				cal.coef.T2P=0.105;
                %     				is_downcast=0;
        end
        
        processed_file=fullfile(chi_processed_path, ['chi_', short_labs{up_down_big}, '/cast_' ...
            cast_suffix '_' short_labs{up_down_big} '.mat']);
        
        if  exist(processed_file,'file')
            load(processed_file)
        else
            chidat=load_chipod_data(chi_path,time_range,suffix,isbig);
            % SWAP CHANNELS FOR AX AND AZ--probably chi-pod not checked by
            % jn before semt for LAJIT deployment? -- MMH 1/31/15
            try
            tmp = chidat.AX;
            tmp2 = chidat.AZ;
            chidat.AX = tmp2;
            chidat.AZ = tmp;
            save(processed_file,'chidat')
            catch
                disp(['chidat empty for number ',num2str(ii)])
            end
        end
        
        if length(chidat.datenum)>1000
            %%% First we'll compute fallspeed from dp/dz and compare this to chipod's
            %%% AZ to get the time offset.
            
            data2.p_lp=conv2(medfilt1(data2.p),hanning(30)/sum(hanning(30)),'same');
            data2.dpdt=gradient(data2.p_lp,nanmedian(diff(data2.datenum*86400)));
            data2.dpdt_hp=data2.dpdt-conv2(data2.dpdt,hanning(750)/sum(hanning(750)),'same');

            tmp=az_correction*9.8*(chidat.AZ-median(chidat.AZ));
            tmp(abs(tmp)>10)=0;
            tmp2=tmp-conv2(tmp,hanning(3000)/sum(hanning(3000)),'same');
            w_from_chipod=cumsum(tmp2*nanmedian(diff(chidat.datenum*86400)));
            id = find(~isnan(w_from_chipod));
            
            ginds=get_profile_inds(data2.p,10);
            % here's the plot:
            figure(100)
            h(1)=subplot(2,1,1);
            hold off, plot(data2.datenum(ginds),data2.dpdt_hp(ginds),'b');
            hold on, plot(chidat.datenum(id),w_from_chipod(id),'r');
            %             legend('dpdt','w_from_chipod')
            dynamicDateTicks
            try
                flag_offset = [];
                offset=TimeOffset(data2.datenum(ginds),data2.dpdt_hp(ginds),chidat.datenum(id),w_from_chipod(id));
                if abs(offset)>2/86400
                    flag_offset(ii) = 1;
                    load default_offset
                    flag_offset='flag';

                end
            catch
            end
            chidat.datenum=chidat.datenum+offset;
            chidat.time_offset_correction_used=offset;
            
            % here's the plot for corrected offset
            h(2)=subplot(2,1,2);
            plot(data2.datenum(ginds),data2.dpdt_hp(ginds),'b',chidat.datenum,w_from_chipod,'r')
            dynamicDateTicks
            linkaxes(h,'x')
            title(flag_offset)
            print('-dpng','-r300',[rec.prpath,'/chi_pods/offsets/cast_' cast_suffix '_' short_labs{up_down_big}]);

%             pause
            
            chidat.fspd=interp1(data2.datenum,-data2.dpdt,chidat.datenum);
            
            %%% Now we'll calibrate T by comparison to the CTD.
            cal.datenum=chidat.datenum;
            datestr(cal.datenum(1))
            cal.P=interp1(data2.datenum,data2.p_lp,chidat.datenum);
            cal.T_CTD=interp1(data2.datenum,data2.t1,chidat.datenum);
            cal.fspd=chidat.fspd;
            
            [cal.coef.T1,cal.T1]=get_T_calibration(data2.datenum(ginds),data2.t1(ginds),chidat.datenum,chidat.T1);
            %%% And now we apply our calibration for DTdt.  Note that
            %%% this differs from the chameleon calibration, which
            %%% produces a calibrated signal that depends on fallspeed,
            %%% and gives units of K/m, whereas this gives K/s.
            cal.T1P=calibrate_chipod_dtdt(chidat.T1P,cal.coef.T1P,chidat.T1,cal.coef.T1);
            %	[cal.coef.T1,cal.T1]=get_T_calibration(data2.datenum(ginds),data2.t1(ginds),chidat.datenum,chidat.T2);
            %   cal.T1P=calibrate_chipod_dtdt(chidat.T2P,cal.coef.T2P,chidat.T1,cal.coef.T1);
            
            test_dtdt=0; %%% this does a digital differentiation to determine whether the differentiator time constant is correct.
            if test_dtdt
                dt=median(diff(chidat.datenum))*3600*24;
                cal.dTdt_dig=[0 ; diff(cal.T1)/dt];
                oset=min(chidat.datenum);
                figure(101); clf
                plot(chidat.datenum-oset,cal.dTdt_dig,chidat.datenum-oset,cal.T1P);
                %                 pause,
                ax=axis;
                ginds2=find((chidat.datenum-oset)>ax(1) & (chidat.datenum-oset)<ax(2));
                [p,f]=fast_psd(cal.T1P(ginds2),256,100);
                [p2,f]=fast_psd(cal.dTdt_dig(ginds2),256,100);
                figure(104); clf
                loglog(f,p2,f,p);
                legend('T1P','dT/dt')
            end
            
            if isbig
                
                [cal.coef.T2,cal.T2]=get_T_calibration(data2.datenum(ginds),data2.t1(ginds),chidat.datenum,chidat.T2);
                cal.T2P=calibrate_chipod_dtdt(chidat.T2P,cal.coef.T2P,chidat.T2,cal.coef.T2);
            else
                cal.T2=cal.T1;
                cal.T2P=cal.T1P;
            end
            
            do_timeseries_plot=1;
            if do_timeseries_plot  
                try
                xls=[min(data2.datenum(ginds)) max(data2.datenum(ginds))];
    
                figure(105)
                clf
                h(1)=subplot(411);
                plot(data2.datenum(ginds),data2.t1(ginds),'k'); hold on
                plot(chidat.datenum,cal.T1,'b')
                plot(chidat.datenum,cal.T2,'r')
                legend('t1','T1','T2')
                ylabel('T [C]')
                xlim(xls)
%                 dynamicDateTicks
                title(['Cast ' cast_suffix ', ' short_labs{up_down_big} '  ' datestr(time_range(1),'dd-mmm-yyyy HH:MM') '-' datestr(time_range(2),15) ', ' CTD_list(ii).name])
                
                
                h(4)=subplot(414);
                plot(chidat.datenum,chidat.fspd)
                ylabel('fallspeed [m/s]')
                xlim(xls)
%                 dynamicDateTicks
                
                h(3)=subplot(413);
                plot(chidat.datenum,cal.T1P-.01,chidat.datenum,cal.T2P+.01)
                ylabel('dTdt [K/s]')
                xlim(xls)
%                 dynamicDateTicks
                
                h(2)=subplot(412);
                %                 plot(data2.datenum(ginds),data2.p(ginds));
                plot(data2.datenum,data2.p);
                ylabel('T [C]')
                xlim(xls)
%                 dynamicDateTicks
                axis tight
                
                linkaxes(h,'x');
                orient tall
                pause(.01)
                print('-dpng','-r300',[rec.prpath,'/chi_pods/profile_png/cast_' cast_suffix '_' short_labs{up_down_big}]);
                catch
                    disp(['plot number ',num2str(ii),' failed'])
                end
                
            end
            test_cal_coef=0;
            
            if test_cal_coef
                ccal.coef1(ii,1:5)=cal.coef.T1;
                ccal.coef2(ii,1:5)=cal.coef.T2;
                figure(106)
                plot(ccal.coef1),hold on,plot(ccal.coef2)
            end
            
            %%% now let's do the computation of chi..
            
            % 1-m CTD data was read in with original file.
            
            [p_max,ind_max]=max(cal.P);
            if is_downcast
                fallspeed_correction=-1;
                try
                ctd=datad_1m;
                catch
                    try
                        ctd=datad;
                    catch
                        disp('Your CTD data is garbage, you sloppy programmer. Why don''t you go hard code a little more?')
                    end
                end
                chi_inds=[1:ind_max];
                sort_dir='descend';
            else
                fallspeed_correction=1;
                ctd=datau_1m;
                chi_inds=[ind_max:length(cal.P)];
                sort_dir='ascend';
            end
            
            smooth_len=20;
            [bfrq,vort,p_ave] = sw_bfrq(ctd.s1,ctd.t1,ctd.p,nanmean(ctd.lat));
            ctd.N2=abs(conv2(bfrq,ones(smooth_len,1)/smooth_len,'same')); % smooth once
            ctd.N2=conv2(ctd.N2,ones(smooth_len,1)/smooth_len,'same'); % smooth twice
            ctd.N2_20=ctd.N2([1:end end]);
            tmp1=sw_ptmp(ctd.s1,ctd.t1,ctd.p,1000);
            ctd.dTdz=[0 ; abs(conv2(diff(tmp1),ones(smooth_len,1)/smooth_len,'same'))./diff(ctd.p)];
            ctd.dTdz_20=conv2(ctd.dTdz,ones(smooth_len,1)/smooth_len,'same');
            
            smooth_len=50;
            [bfrq,vort,p_ave] = sw_bfrq(ctd.s1,ctd.t1,ctd.p,nanmean(ctd.lat));
            ctd.N2=abs(conv2(bfrq,ones(smooth_len,1)/smooth_len,'same')); % smooth once
            ctd.N2=conv2(ctd.N2,ones(smooth_len,1)/smooth_len,'same'); % smooth twice
            ctd.N2_50=ctd.N2([1:end end]);
            tmp1=sw_ptmp(ctd.s1,ctd.t1,ctd.p,1000);
            ctd.dTdz=[0 ; abs(conv2(diff(tmp1),ones(smooth_len,1)/smooth_len,'same'))./diff(ctd.p)];
            ctd.dTdz_50=conv2(ctd.dTdz,ones(smooth_len,1)/smooth_len,'same');
            
            ctd.dTdz=max(ctd.dTdz_50,ctd.dTdz_20);
            ctd.N2=max(ctd.N2_50,ctd.N2_20);
             
            doplot=1;
            if doplot
                figure(107)
                subplot(121)
                plot(log10(abs(ctd.N2)),ctd.p),
                xlabel('N^2'),ylabel('depth')
                subplot(122)
                plot(log10(abs(ctd.dTdz)),ctd.p) % ,log10(abs(ctd.dTdz2)),ctd.p,log10(abs(ctd.dTdz3)),ctd.p)
                xlabel('dTdz'),ylabel('depth')
                print('-dpng','-r300',[rec.prpath,'/chi_pods/N2_profile/cast_' cast_suffix '_' short_labs{up_down_big}]);
                
            end
            % now let's do the chi computations:
            
            extra_z=0.5; % number of extra meters to get rid of.
            wthresh = 0.1;
            [datau2,bad_inds] = ctd_rmdepthloops(data2,extra_z,wthresh);
            tmp=ones(size(datau2.p));
            tmp(bad_inds)=0;
            cal.is_good_data=interp1(datau2.datenum,tmp,cal.datenum,'nearest');
            length(find(cal.is_good_data==1))
            
            %%% Now we'll do the main looping through of the data.
            clear avg
            nfft=128;
            todo_inds=chi_inds(1:nfft/2:(length(chi_inds)-nfft))';
            tfields={'datenum','P','N2','dTdz','fspd','T','S','P','theta','sigma',...
                'chi1','eps1','chi2','eps2','KT1','KT2','TP1var','TP2var'};
            for n=1:length(tfields)
                avg.(tfields{n})=NaN*ones(size(todo_inds));
            end
            avg.datenum=cal.datenum(todo_inds+(nfft/2)); % This is the mid-value of the bin
            avg.P=cal.P(todo_inds+(nfft/2));
            good_inds=find(~isnan(ctd.p));
            avg.N2=interp1(ctd.p(good_inds),ctd.N2(good_inds),avg.P);
            avg.dTdz=interp1(ctd.p(good_inds),ctd.dTdz(good_inds),avg.P);
            avg.T=interp1(ctd.p(good_inds),ctd.t1(good_inds),avg.P);
            avg.S=interp1(ctd.p(good_inds),ctd.s1(good_inds),avg.P);
            % In order for these to work, need to use vectorized seawater
            % routines (same as regular, but with dots :)
            avg.nu=sw_visc(avg.S,avg.T,avg.P); 
            avg.tdif=sw_tdif(avg.S,avg.T,avg.P);
            avg.samplerate=1./nanmedian(diff(cal.datenum))/24/3600;
            
            h = waitbar(0,[CTD_list(ii).name(1:end-8) 'Please wait...']);
            for n=1:length(todo_inds)
                inds=todo_inds(n)-1+[1:nfft];
                if all(cal.is_good_data(inds)==1)
                    avg.fspd(n)=mean(cal.fspd(inds));
                    
                    [tp_power,freq]=fast_psd(cal.T1P(inds),nfft,avg.samplerate);
                    avg.TP1var(n)=sum(tp_power)*nanmean(diff(freq));
                    if avg.TP1var(n)>1e-4
                        fixit=0;
                        if fixit
                            trans_fcn=0;
                            trans_fcn1=0;
                            thermistor_filter_order=2;
                            thermistor_cutoff_frequency=32;
                            analog_filter_order=4;
                            analog_filter_freq=50;
                            
                            tp_power=invert_filt(freq,invert_filt(freq,tp_power,thermistor_filter_order, ...
                                thermistor_cutoff_frequency),analog_filter_order,analog_filter_freq);
                        end
                        try
                            [chi1,epsil1,k,spec,kk,speck,stats]=get_chipod_chi(freq,tp_power,abs(avg.fspd(n)),avg.nu(n),...
                                avg.tdif(n),avg.dTdz(n),'nsqr',avg.N2(n),'doplots',1);
                            figure(17)
                            print('-dpng','-r300',[rec.prpath,'/chi_pods/spectra/cast_' cast_suffix '_' short_labs{up_down_big} num2str(n)]);

%                             if doplot
%                                 figure(200); clf
%                                 loglog(k,spec,'b'); hold on
%                                 loglog(kk,speck,'r');
%                                 set(gca,'Xlim',[0,200],'Ylim',[10^-10,10^-3])
%                                 pause
%                             end
                        catch
                            chi1=NaN;
                            epsil1=NaN; 
                            disp(['Chi Computation failed for number',num2str(ii)])
                        end
                        
                        avg.chi1(n)=chi1(1);
                        avg.eps1(n)=epsil1(1);
                        avg.KT1(n)=0.5*chi1(1)/avg.dTdz(n)^2;
                    end
                else
                    
                end
                if ~mod(n,10)
                    waitbar(n/length(todo_inds),h);
                end
            end
            delete(h)
            
            figure(108)
            subplot(131)
            plot(log10(avg.chi1),avg.P),axis ij, title('log_1_0(chi1)')
            xlim([-10,0])
            subplot(132)
            plot(log10(avg.KT1),avg.P),axis ij, title('log_1_0(KT1)')
            xlim([-10,0])
            
            subplot(133)
            plot(log10(abs(avg.dTdz)),avg.P),axis ij, title('log_1_0dTdz')
            print('-dpng','-r300',[rec.prpath,'/chi_pods/chi_profile/cast_' cast_suffix '_' short_labs{up_down_big}]);
            
            
            processed_file=fullfile(chi_processed_path, ['chi_' short_labs{up_down_big} '/avg/avg_' ...
                cast_suffix '_' short_labs{up_down_big} '.mat']);
            save(processed_file,'avg','ctd')
            
            
        end

    end
    
end



