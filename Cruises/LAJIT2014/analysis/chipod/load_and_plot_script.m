% Updated 1/27/15 MMH for LAJIT Student Cruise

% script to figure out what Mark's file format is:

% fname='chipod113062004.101';
% fname='chipod113061923.101';
% %fname='chipod313062000.103';
% fname='../chipod_midi2/midi2_chi13062520.A0101'
% %fname='data/chipod113062400.103';
% fname='data/chipod113062320.103';
% fname='data/lab_RBR_13101617.102';
% fname='bench_tests/bench13102400.A0102';
% fname='bench_tests/benche13102421.A1009';
% fname='bench_tests/bench13102502.A1009';
% fname='bench_tests/bench13102502.A1005';
% fname='bench_tests/bench13102503.A1012';
% fname='bench_tests/bench13102504.A1006';
% fname='bench_tests/bench13102504.A1013';
% fname='bench_tests/bench13102505.A1008';
% fname='bench_tests/bench13102505.A1014';
% fname='bench_tests/bench13102505.A1011';
% fname='bench_tests/TI_bench13110114.A1007';
% fname='bench_tests/TI_bench13110115.A1010';
% fname='bench_tests/TI_bench_b13110115.A1010'; %this is an ice test.  The sensor is totally on-scale.
% fname='bench_tests/TI_bench_final_13110202.A1007'; %this is a long outdoor test where the unit started automatically by itself.
% fname='bench_tests/TI_bench_finalx_13110204.A1007';
% fname='bench_tests/TI_bench_finalx_13110120.A1010';
% fname='bench_tests/colombo13110820.A1011';
% fname='at_sea/colombo13110900.A1014';
% fname='at_sea/underway13110916.A1011';
% 
% tpath='at_sea/';
% tpath='../data/chipod_ww/asiri2/';
% the_files=dir([tpath 'asiri2_131120*.A1008'])
% 
% tpath='./';
% the_files=dir([tpath 'bench*'])

%tpath='../data/bow_chain/chipods/';
%the_files=dir([tpath 'bow_131118*.A1006'])

fnames = help_FilenamesFromPath(rec.chi.raw,'mlg',0);
fnames_no = help_FilenamesFromPath(rec.chi.raw,'mlg',1);

nfiles=length(fnames);
for ii=5:nfiles
	figure(ii)
	clf
	fname=fnames{ii};
[out,the_time,counter]=load_mini_chipod(fname);
save(fullfile(rec.chi.proc,fnames_no{ii}),'out','the_time','counter')
h(1)=subplot(311);
plot(the_time,out(:,2))
datetick('keeplimits')
hold on
title([fname ' ::: ' datestr(the_time(1)) ' - ' datestr(the_time(end))],'interpreter','none')
h(2)=subplot(312);
plot(the_time,out(:,1))
hold on
datetick('keeplimits')
h(3)=subplot(313);
plot(the_time,out(:,3))
hold on,plot(the_time,out(:,4),'r')
linkaxes(h,'x')
axis auto
datetick('keeplimits')
% pause
bdr_savefig(rec.prpath,fname,'p',300,'fontsize',12);  
pause(.1)

end


%%
 
