% Updated 1/31/15 MMH

%Makes empty station structures with fields of the averaged chipod data

sta1.avg.datenum=[];
sta1.avg.P=[];
sta1.avg.N2=[];
sta1.avg.dTdz=[];
sta1.avg.T=[];
sta1.avg.S=[];
sta1.avg.theta=[];
sta1.avg.sigma=[];
sta1.avg.chi1=[];
sta1.avg.eps1=[];
sta1.avg.chi2=[];
sta1.avg.eps2=[];
sta1.avg.KT1=[];
sta1.avg.KT2=[];
sta1.avg.TP1var=[];
sta1.avg.TP2var=[];
sta1.avg.nu=[];
sta1.avg.tdif=[];
sta1.avg.fspd=[];
sta1.avg.samplerate=[];

sta2.avg.datenum=[];
sta2.avg.P=[];
sta2.avg.N2=[];
sta2.avg.dTdz=[];
sta2.avg.T=[];
sta2.avg.S=[];
sta2.avg.theta=[];
sta2.avg.sigma=[];
sta2.avg.chi1=[];
sta2.avg.eps1=[];
sta2.avg.chi2=[];
sta2.avg.eps2=[];
sta2.avg.KT1=[];
sta2.avg.KT2=[];
sta2.avg.TP1var=[];
sta2.avg.TP2var=[];
sta2.avg.nu=[];
sta2.avg.tdif=[];
sta2.avg.fspd=[];
sta2.avg.samplerate=[];


sta1.ctd.N2=[];
sta1.ctd.t1=[];
sta1.ctd.t2=[];
sta1.ctd.c1=[];
sta1.ctd.c2=[];
sta1.ctd.s1=[];
sta1.ctd.s2=[];
sta1.ctd.theta1=[];
sta1.ctd.theta2=[];
sta1.ctd.sigma1=[];
sta1.ctd.sigma2=[];
sta1.ctd.oxygen=[];
sta1.ctd.trans=[];
sta1.ctd.fl=[];
sta1.ctd.lon=[];
sta1.ctd.lat=[];
sta1.ctd.time=[];
sta1.ctd.nscan=[];
sta1.ctd.depth=[];
sta1.ctd.p=[];
sta1.ctd.N2_20=[];
sta1.ctd.dTdz=[];
sta1.ctd.dTdz_20=[];
sta1.ctd.N2_50=[];
sta1.ctd.dTdz_50=[];
sta1.ctd.datenum=[];

sta2.ctd.N2=[];
sta2.ctd.t1=[];
sta2.ctd.t2=[];
sta2.ctd.c1=[];
sta2.ctd.c2=[];
sta2.ctd.s1=[];
sta2.ctd.s2=[];
sta2.ctd.theta1=[];
sta2.ctd.theta2=[];
sta2.ctd.sigma1=[];
sta2.ctd.sigma2=[];
sta2.ctd.oxygen=[];
sta2.ctd.trans=[];
sta2.ctd.fl=[];
sta2.ctd.lon=[];
sta2.ctd.lat=[];
sta2.ctd.time=[];
sta2.ctd.nscan=[];
sta2.ctd.depth=[];
sta2.ctd.p=[];
sta2.ctd.N2_20=[];
sta2.ctd.dTdz=[];
sta2.ctd.dTdz_20=[];
sta2.ctd.N2_50=[];
sta2.ctd.dTdz_50=[];
sta2.ctd.datenum=[];









