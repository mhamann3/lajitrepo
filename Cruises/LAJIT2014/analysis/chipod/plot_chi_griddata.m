setup_LAJIT2014

load(fullfile(rec.matfiles,'chi_stations.mat'))
load cast_times
load adcps

pvar='eps1';
isovar = 'theta1';
clim=[-10,-4];

figure(10); clf
h(1) = subplot(3,1,1);
contourf(sta1.avg.datevec,sta1.avg.Pvec,log10(eval(['sta1.chi_grid.',pvar])),20,'EdgeColor','none'); hold on
contour(sta1.ctd.datevec,sta1.ctd.Pvec,log(eval(['sta1.ctd_grid.',isovar])),10,'k','Linewidth',2)
plot(down_start(1:123),5*ones(123,1),'k+')
caxis(clim);
colormap('jet')
freezeColors
c=colorbar;
ylabel(c,'Dissipation (Epsilon), m^2s^{-3}')
cbfreeze(c);
axis ij
set(h(1),'Xlim',[sta1.ctd.datevec(1),sta1.ctd.datevec(end)],'Ylim',[0,170])
title(['Station 1'])

h(2) = subplot(3,1,2);
id = find(adcps.dnum>=sta1.avg.datevec(1) & adcps.dnum<=sta1.avg.datevec(end));
contourf(adcps.dnum(id),adcps.depth,adcps.upCanyon(:,id),15,'EdgeColor','none'); hold on
contour(sta1.ctd.datevec,sta1.ctd.Pvec,log(eval(['sta1.ctd_grid.',isovar])),10,'k','Linewidth',2)
colormap('redblue'); caxis([-0.3,0.3]); c = colorbar; 
ylabel(c,'Up-Canyon Velocity, m/s');
axis ij
set(h(2),'Xlim',[sta1.ctd.datevec(1),sta1.ctd.datevec(end)],'Ylim',[0,170])

h(3) = subplot(3,1,3);
contourf(adcps.dnum(id),adcps.depth,adcps.crossCanyon(:,id),15,'EdgeColor','none'); hold on
contour(sta1.ctd.datevec,sta1.ctd.Pvec,log(eval(['sta1.ctd_grid.',isovar])),10,'k','Linewidth',2)
colormap('redblue'); caxis([-0.3,0.3]); c = colorbar; 
ylabel(c,'Across-Canyon Velocity, m/s');
axis ij
set(h(3),'Xlim',[sta1.ctd.datevec(1),sta1.ctd.datevec(end)],'Ylim',[0,170])


datetick(h(1),'keeplimits')
datetick(h(2),'keeplimits')
datetick(h(3),'keeplimits')

bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours/'),['sta1_',pvar,'_withisopycnals_vels'],'p',300,'fontsize',11)

% figure(11); clf
% contourf(sta2.avg.datevec,sta2.avg.Pvec,log10(eval(['sta2.chi_grid.',pvar])),20,'EdgeColor','none')
% % shading flat
% caxis(clim); hold on
% contour(sta2.ctd.datevec,sta2.ctd.Pvec,log(eval(['sta2.ctd_grid.',isovar])),10,'k','Linewidth',2)
% plot(down_start(124:222),5*ones(99,1),'k+')
% axis ij tight
% title(['Station 2: ',pvar])
% % dynamicDateTicks
% datetick
% colorbar
% title('station 2')
% % bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours/'),['sta2_',pvar,'_withisopycnals'],'p',300,'fontsize',11)


clim=[-10,-4];

figure(11); clf
h(1) = subplot(3,1,1);
contourf(sta2.avg.datevec,sta2.avg.Pvec,log10(eval(['sta2.chi_grid.',pvar])),20,'EdgeColor','none'); hold on
contour(sta2.ctd.datevec,sta2.ctd.Pvec,log(eval(['sta2.ctd_grid.',isovar])),10,'k','Linewidth',2)
plot(down_start(1:123),5*ones(123,1),'k+')
caxis(clim);
colormap('jet')
freezeColors
c=colorbar;
cbfreeze(c);
axis ij
set(h(1),'Xlim',[sta2.ctd.datevec(1),sta2.ctd.datevec(end)],'Ylim',[0,230])
title(['Station 2'])

h(2) = subplot(3,1,2);
id = find(adcps.dnum>=sta2.avg.datevec(1) & adcps.dnum<=sta2.avg.datevec(end));
contourf(adcps.dnum(id),adcps.depth,adcps.upCanyon(:,id),15,'EdgeColor','none'); hold on
contour(sta2.ctd.datevec,sta2.ctd.Pvec,log(eval(['sta2.ctd_grid.',isovar])),10,'k','Linewidth',2)
colormap('redblue'); caxis([-0.3,0.3]); c = colorbar; 
ylabel(c,'Along-Canyon Velocity, m/s');
axis ij
set(h(2),'Xlim',[sta2.ctd.datevec(1),sta2.ctd.datevec(end)],'Ylim',[0,230])

h(3) = subplot(3,1,3);
contourf(adcps.dnum(id),adcps.depth,adcps.crossCanyon(:,id),15,'EdgeColor','none'); hold on
contour(sta2.ctd.datevec,sta2.ctd.Pvec,log(eval(['sta2.ctd_grid.',isovar])),10,'k','Linewidth',2)
colormap('redblue'); caxis([-0.3,0.3]); c = colorbar; 
ylabel(c,'Across-Canyon Velocity, m/s');
axis ij
set(h(3),'Xlim',[sta2.ctd.datevec(1),sta2.ctd.datevec(end)],'Ylim',[0,230])


datetick(h(1),'keeplimits')
datetick(h(2),'keeplimits')
datetick(h(3),'keeplimits')

bdr_savefig(fullfile(rec.prpath,'chi_pods/station_contours/'),['sta2_',pvar,'_withisopycnals_vels'],'p',300,'fontsize',11)


