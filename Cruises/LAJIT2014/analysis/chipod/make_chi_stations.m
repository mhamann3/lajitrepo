% 1/29/15 MMH
% Make structures for all chipod data from the two stations
% Updated 1/31/15

% Chi profiles:
% cast 1:     001 to 028  (001_028)
% cast 2:     029 to 082  (002_054)
% cast 3:     083 to 122  (003_040)
% cast 4:     123 to 135  (004_013)
% cast 5:     135 to 182  (005_047)
% cast 6:     136 to 221  (006_039)
% cast 7:     222
% cast 8:     223
% cast 9:     224
% cast 10:    225
% cast 11:    226
% cast 12:    227
% cast 13:    228
% cast 14:    229

%223,224,200,104 are missing

setup_LAJIT2014

% The upward-looking chipod was garbage, so we'll use ony the
% downward-looking data we processed
dn_path = rec.chi.dn_avg;   % averaged data for the profile from chipod

empty_stations

fnames = help_FilenamesFromPath(dn_path,'.mat',0);
for ii=1:length(fnames)
    load(fnames{ii})
    if ii<122    % First station (casts 1-3)    % Change to 123 if lost casts are found
        sta1.avg.datenum=[sta1.avg.datenum, avg.datenum'];
        sta1.avg.P=[sta1.avg.P, avg.P'];
        sta1.avg.N2=[sta1.avg.N2, avg.N2'];
        sta1.avg.dTdz=[sta1.avg.dTdz, avg.dTdz'];
        sta1.avg.T=[sta1.avg.T, avg.T'];
        sta1.avg.S=[sta1.avg.S, avg.S'];
        sta1.avg.theta=[sta1.avg.theta, avg.theta'];
        sta1.avg.sigma=[sta1.avg.sigma, avg.sigma'];
        sta1.avg.chi1=[sta1.avg.chi1, avg.chi1'];
        sta1.avg.eps1=[sta1.avg.eps1, avg.eps1'];
        sta1.avg.chi2=[sta1.avg.chi2, avg.chi2'];
        sta1.avg.eps2=[sta1.avg.eps2, avg.eps2'];
        sta1.avg.KT1=[sta1.avg.KT1, avg.KT1'];
        sta1.avg.KT2=[sta1.avg.KT2, avg.KT2'];
        sta1.avg.TP1var=[sta1.avg.TP1var, avg.TP1var'];
        sta1.avg.TP2var=[sta1.avg.TP2var, avg.TP2var'];
        sta1.avg.nu=[sta1.avg.nu, avg.nu'];
        sta1.avg.tdif=[sta1.avg.tdif, avg.tdif'];
        sta1.avg.fspd=[sta1.avg.fspd, avg.fspd'];
        sta1.avg.samplerate=[sta1.avg.datenum, avg.samplerate];
        
        sta1.ctd.N2=[sta1.ctd.N2, ctd.N2'];
        sta1.ctd.t1=[sta1.ctd.t1, ctd.t1'];
        sta1.ctd.t2=[sta1.ctd.t2, ctd.t2'];
        sta1.ctd.c1=[sta1.ctd.c1, ctd.c1'];
        sta1.ctd.c2=[sta1.ctd.c2, ctd.c2'];
        sta1.ctd.s1=[sta1.ctd.s1, ctd.s1'];
        sta1.ctd.s2=[sta1.ctd.s2, ctd.s2'];
        sta1.ctd.theta1=[sta1.ctd.theta1, ctd.theta1'];
        sta1.ctd.theta2=[sta1.ctd.theta2, ctd.theta2'];
        sta1.ctd.sigma1=[sta1.ctd.sigma1, ctd.sigma1'];
        sta1.ctd.sigma2=[sta1.ctd.sigma2, ctd.sigma2'];
        sta1.ctd.oxygen=[sta1.ctd.oxygen, ctd.oxygen'];
        sta1.ctd.trans=[sta1.ctd.trans, ctd.trans'];
        sta1.ctd.fl=[sta1.ctd.fl, ctd.fl'];
        sta1.ctd.lon=[sta1.ctd.lon, ctd.lon'];
        sta1.ctd.lat=[sta1.ctd.lat, ctd.lat'];
        sta1.ctd.time=[sta1.ctd.time, ctd.time'];
        sta1.ctd.nscan=[sta1.ctd.nscan, ctd.nscan'];
        sta1.ctd.depth=[sta1.ctd.depth, ctd.depth'];
        sta1.ctd.p=[sta1.ctd.p, ctd.p'];
        sta1.ctd.N2_20=[sta1.ctd.N2_20, ctd.N2_20'];
        sta1.ctd.dTdz=[sta1.ctd.dTdz, ctd.dTdz'];
        sta1.ctd.dTdz_20=[sta1.ctd.dTdz_20, ctd.dTdz_20'];
        sta1.ctd.N2_50=[sta1.ctd.N2_50, ctd.N2_50'];
        sta1.ctd.dTdz_50=[sta1.ctd.dTdz_50, ctd.dTdz_50'];
        sta1.ctd.datenum=[sta1.ctd.datenum, ctd.datenum'];
        
        
        
    elseif ii<220   %Second station (casts 4-6)     % change to 222 if lost profiles are found
        sta2.avg.datenum=[sta2.avg.datenum, avg.datenum'];
        sta2.avg.P=[sta2.avg.P, avg.P'];
        sta2.avg.N2=[sta2.avg.N2, avg.N2'];
        sta2.avg.dTdz=[sta2.avg.dTdz, avg.dTdz'];
        sta2.avg.T=[sta2.avg.T, avg.T'];
        sta2.avg.S=[sta2.avg.S, avg.S'];
        sta2.avg.theta=[sta2.avg.theta, avg.theta'];
        sta2.avg.sigma=[sta2.avg.sigma, avg.sigma'];
        sta2.avg.chi1=[sta2.avg.chi1, avg.chi1'];
        sta2.avg.eps1=[sta2.avg.eps1, avg.eps1'];
        sta2.avg.chi2=[sta2.avg.chi2, avg.chi2'];
        sta2.avg.eps2=[sta2.avg.eps2, avg.eps2'];
        sta2.avg.KT1=[sta2.avg.KT1, avg.KT1'];
        sta2.avg.KT2=[sta2.avg.KT2, avg.KT2'];
        sta2.avg.TP1var=[sta2.avg.TP1var, avg.TP1var'];
        sta2.avg.TP2var=[sta2.avg.TP2var, avg.TP2var'];
        sta2.avg.nu=[sta2.avg.nu, avg.nu'];
        sta2.avg.tdif=[sta2.avg.tdif, avg.tdif'];
        sta2.avg.fspd=[sta2.avg.fspd, avg.fspd'];
        sta2.avg.samplerate=[sta2.avg.datenum, avg.samplerate];
        
        sta2.ctd.N2=[sta2.ctd.N2, ctd.N2'];
        sta2.ctd.t1=[sta2.ctd.t1, ctd.t1'];
        sta2.ctd.t2=[sta2.ctd.t2, ctd.t2'];
        sta2.ctd.c1=[sta2.ctd.c1, ctd.c1'];
        sta2.ctd.c2=[sta2.ctd.c2, ctd.c2'];
        sta2.ctd.s1=[sta2.ctd.s1, ctd.s1'];
        sta2.ctd.s2=[sta2.ctd.s2, ctd.s2'];
        sta2.ctd.theta1=[sta2.ctd.theta1, ctd.theta1'];
        sta2.ctd.theta2=[sta2.ctd.theta2, ctd.theta2'];
        sta2.ctd.sigma1=[sta2.ctd.sigma1, ctd.sigma1'];
        sta2.ctd.sigma2=[sta2.ctd.sigma2, ctd.sigma2'];
        sta2.ctd.oxygen=[sta2.ctd.oxygen, ctd.oxygen'];
        sta2.ctd.trans=[sta2.ctd.trans, ctd.trans'];
        sta2.ctd.fl=[sta2.ctd.fl, ctd.fl'];
        sta2.ctd.lon=[sta2.ctd.lon, ctd.lon'];
        sta2.ctd.lat=[sta2.ctd.lat, ctd.lat'];
        sta2.ctd.time=[sta2.ctd.time, ctd.time'];
        sta2.ctd.nscan=[sta2.ctd.nscan, ctd.nscan'];
        sta2.ctd.depth=[sta2.ctd.depth, ctd.depth'];
        sta2.ctd.p=[sta2.ctd.p, ctd.p'];
        sta2.ctd.N2_20=[sta2.ctd.N2_20, ctd.N2_20'];
        sta2.ctd.dTdz=[sta2.ctd.dTdz, ctd.dTdz'];
        sta2.ctd.dTdz_20=[sta2.ctd.dTdz_20, ctd.dTdz_20'];
        sta2.ctd.N2_50=[sta2.ctd.N2_50, ctd.N2_50'];
        sta2.ctd.dTdz_50=[sta2.ctd.dTdz_50, ctd.dTdz_50'];
        sta2.ctd.datenum=[sta2.ctd.datenum, ctd.datenum'];
        
    end
end

    sta1.avg.datevec = sta1.avg.datenum(1):1/24/60*5:sta1.avg.datenum(end);  % 5 minute grid
    sta1.avg.Pvec = 0:1:ceil(max(sta1.avg.P))+5;    % 1 dbar grid in depth
    [sta1.avg.dnum,sta1.avg.Pgrid] = meshgrid(sta1.avg.datevec,sta1.avg.Pvec);  % make grid coordinates
   

    sta2.avg.datevec = sta2.avg.datenum(1):1/24/60*5:sta2.avg.datenum(end);  % 5 minute grid
    sta2.avg.Pvec = 0:1:ceil(max(sta2.avg.P))+5;    % 1 dbar grid in depth
    [sta2.avg.dnum,sta2.avg.Pgrid] = meshgrid(sta2.avg.datevec,sta2.avg.Pvec);  % make grid coordinates
    
    sta1.ctd.datevec = nanmin(sta1.ctd.datenum):1/24/60*5:nanmax(sta1.ctd.datenum);  % 5 minute grid
    sta1.ctd.Pvec = 0:1:ceil(max(sta1.ctd.depth))+5;    % 1 dbar grid in depth
    [sta1.ctd.dnum,sta1.ctd.Pgrid] = meshgrid(sta1.ctd.datevec,sta1.ctd.Pvec);  % make grid coordinates
    
    
    sta2.ctd.datevec = nanmin(sta2.ctd.datenum):1/24/60*5:nanmax(sta2.ctd.datenum);  % 5 minute grid
    sta2.ctd.Pvec = 0:1:ceil(max(sta2.ctd.depth))+5;    % 1 dbar grid in depth
    [sta2.ctd.dnum,sta2.ctd.Pgrid] = meshgrid(sta2.ctd.datevec,sta2.ctd.Pvec);  % make grid coordinates
    
save(fullfile(rec.matfiles,'chi_stations'),'sta1','sta2')








