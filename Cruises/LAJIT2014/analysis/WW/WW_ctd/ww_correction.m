%% Variables
% time - time in datenumber.
% P - pressure,dbar
% T - temperature,degC
% C - conductivity, S/m
% v1 - chlorophyll fluorescence
% v2 - oxygen voltage
% S - salinity
% rho - density(bulk)
% dPdt - change in pressure over time
% std.profiles - gridded data from make_standard_profiles
% idx - start and end indices of upcasts.

load ./WWctd;
for k = 100:100 % all profiles - 1:189
    ii = ww.idx(k,1):ww.idx(k,2);
    [data,tau(k),L(k),fval] = ww_despike(ww.T(ii),ww.C(ii),ww.P(ii),1);
    figure(4);
    subplot(1,3,1:2);plot(ww.S(ii),ww.P(ii),'k');
    % subplot(1,3,1:2);plot(ww.std_profiles.S(:,k),ww.std_profiles.P,'k');
    hold on; plot(data.S,data.P,'r'); hold off; axis ij; ylim([0,100]);
    legend('Original salinity','Corrected salinity');
    xlabel('Salinity');ylabel('Pressure,dbar');
    
    subplot(1,3,3);plot(ww.std_profiles.T(:,k),ww.std_profiles.P,'b');
    axis ij; ylim([0,100]); plot_label('Temperature,degC','Pressure,dbar','Temperature profile');
    print(1,'-dpng',['./despike_fig/coh_',num2str(k)]);
    print(2,'-dpng',['./despike_fig/pha_',num2str(k)]);  
    print(4,'-depsc',['./despike_fig/profile_',num2str(k)]);
    save('./despike_param.mat','tau','L');
end