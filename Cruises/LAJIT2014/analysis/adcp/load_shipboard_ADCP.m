% 2/2/15 MMH
% Load shipboard ADCP data from LAJIT2014
% Based on routine used by MHA in WaWaves14

setup_LAJIT2014

year=2014;
datadir = fullfile(rec.sADCP,'proc');

wh_adcps={'nb300'};
todo=1;

for c=todo%:length(wh_adcps)
    wh_adcp=wh_adcps{c};
    D = load_getmat(fullfile(datadir, wh_adcp, 'contour', 'allbins_'),'all');
    
%     adcp.u=D.u.*D.nanmask;
%     adcp.v=D.v.*D.nanmask;
%     adcp.w =D.w.*D.nanmask;
    
    adcp.amp=D.amp.*D.nanmask;
    
    adcp.lon=D.lon;
    adcp.lat=D.lat;
    adcp.yday=D.dday;
    adcp.dnum=yday2datenum(adcp.yday,year);
    adcp.z=D.depth(:,1); %make sure # of bins and bin size does not change to do this!
    adcp.depth=adcp.z;
    
    adcp.wh_adcp=wh_adcp;
    %     adcps{c}=adcp;
    
    %rotate velocities to canyon coordinates
rot=0.7077; %angle to be rotated in radians- positive is counterclockwise rotation

% rotate vectors to new coordinates
adcp.upCanyon = adcp.u.*cos(rot)-adcp.v.*sin(rot); %ac= along canyon
adcp.crossCanyon = adcp.u.*sin(rot)+adcp.v.*cos(rot); %xc= across canyon

    
%     [adcp.upCanyon,adcp.crossCanyon] = rotate_vel2D(adcp.u,adcp.v,-55);
    adcps=adcp;
    
end


save(fullfile(rec.matfiles,'adcps'),'adcps')






