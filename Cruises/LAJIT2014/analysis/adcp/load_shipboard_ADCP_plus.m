% 2/2/15 MMH
% Load shipboard ADCP data from LAJIT2014
setup_LAJIT2014
% prefix = fullfile(rec.cruise,[info.unols_name,'adcp']);
% data = load_getmat(prefix,'all');

% load(fullfile(rec.cruise,'SP1411adcp/proc/nb300/contour/allbins_other.mat'))
% load(fullfile(rec.cruise,'SP1411adcp/proc/nb300/contour/allbins_u.mat'))
% load(fullfile(rec.cruise,'SP1411adcp/proc/nb300/contour/allbins_v.mat'))
% load(fullfile(rec.cruise,'SP1411adcp/proc/nb300/contour/allbins_w.mat'))

% figure(1)
% pcolor(yday2datenum(DAYS,2014),DEPTH(:,1),U)
% shading flat
% axis ij
% datetick


%% LoadNewADCP_oceanus.m
%8/2014 MHA
%Load in the ADCP data for the cruise.
%This is the bones of a routine that should be turned into a function.
%cruise='oc1408b_02';
% cruise='current_cruise';
% basedir='/Volumes/data/';

year=2014;
% 
% if ~exist('cruise')
%     cruise='current_cruise';
% end
datadir = fullfile(rec.sADCP,'proc');
% datadir=fullfile(basedir,cruise,'proc'); %for on board ship
%datadir='/Volumes/DataDrive1/KM1228/km1228-ADCP_Data/proc/'; %for afterwards.
%datadir='/Volumes/DataDrive/KM1228/km1228-ADCP_Data/proc/'; %for afterwards, at work

% wh_adcps={'wh300';'os150bb';'os150nb';'os75bb';'os75nb'};
wh_adcps={'nb300'};
% todo=1:2;
todo=1;
%wh_adcp='wh300'; %for wh300
%wh_adcp='os150bb'; %for narrowband
%wh_adcp='os150bb'; %for broadband

for c=todo%:length(wh_adcps)
    wh_adcp=wh_adcps{c};
    D = load_getmat(fullfile(datadir, wh_adcp, 'contour', 'allbins_'));
    
    %
    adcp.u=D.u.*D.nanmask;
    adcp.v=D.v.*D.nanmask;
    adcp.w = D.w.*D.nanmask;
    
    adcp.amp=D.amp.*D.nanmask;
    
    adcp.lon=D.lon;
    adcp.lat=D.lat;
    adcp.yday=D.dday;
    adcp.dnum=yday2datenum(adcp.yday,year);
    adcp.z=D.depth(:,1); %make sure # of bins and bin size does not change to do this!
    adcp.depth=adcp.z;
    
    % Screen the spurious deep values
    %iz=find(adcp.z > 1600);
    
    
    
    %adcp.u(iz,:)=nan;
    %adcp.v(iz,:)=nan;
    
    %adcp.dnum=yday2datenum(adcp.yday,2012);
    adcp.wh_adcp=wh_adcp;
    adcps{c}=adcp;
    
end
%%
% %
% %for wh150 (5-min data) get x-position, defined in km from waypoint A
% 
% %8/28: adjust x_km calculation when we switched lines from AB to CD
% lineCD_min=datenum(2014,8,28,19,50,0); 
% lineCD_max=datenum(2014,8,28,22,46,0);
% 
% adcps{1}.x_km=nan*adcps{1}.lat;adcps{1}.x_nm=adcps{1}.x_km;
% for c=1:length(adcps{1}.lat)
%     DisplayProgress(c,100)
%     if adcps{1}.dnum(c) > lineCD_min & adcps{1}.dnum(c) < lineCD_max
%     adcps{1}.x_nm(c)=nav2([lat1(1) adcps{1}.lat(c)],[zero22pi(lon1(1)) adcps{1}.lon(c)]);
%     else
%     adcps{1}.x_nm(c)=nav2([lat0(1) adcps{1}.lat(c)],[zero22pi(lon0(1)) adcps{1}.lon(c)]);
%     end
%     
% end
% 
% adcps{1}.x_km=nm2km(adcps{1}.x_nm);
% %%
% %Then interpolate it onto the others
% if length(todo)>1
%     for f=2:length(todo)
%         adcps{f}.x_km=interp1(adcps{1}.yday,adcps{1}.x_km,adcps{f}.yday);
%     end
% end
% %%
% plotit=1;
% if plotit
%     printit=0;
%     
%     dt_adcp=3/24; %time range to plot - days
%     cl=[-.3 .3];
%     rightnow=now + 7/24; %put computer time in UTC if needed!
%     xl=rightnow + [-dt_adcp 0];
%     yl=[0 150];
%     figure(1)
%     clf
%     ax=MySubplot(.1,.1,0.01,.1,.2,0.01,2,length(todo))
%     for c=todo
%         adcp=adcps{c};
%         ig=find(adcp.dnum > rightnow - dt_adcp);
%         
%         axes(ax(2*c-1))
%         imagesc(adcp.dnum(ig),adcp.z,adcp.u(:,ig))
%         ylim(yl)
%         xlim(xl)
%         caxis(cl)
%         if c==5
%             datetick('keepticks','keeplimits')
%         else xtloff
%         end
%         SubplotLetter([wh_adcps{c} 'u'],.01,.03);
%         axes(ax(2*c))
%         imagesc(adcp.dnum(ig),adcp.z,adcp.v(:,ig))
%         ytloff
%         ylim(yl)
%         xlim(xl)
%         caxis(cl)
%         if c==5
%             datetick('keepticks','keeplimits')
%         else xtloff
%         end
%         SubplotLetter([wh_adcps{c} 'v'],.01,.03);
%     end
%     mycolorbar2(cl,'m/s','horiz',[.4 .1 .2 .02],length(colormap),10);
%     suptitle(cruise)
%     setpp(7.5,10.5)
%     if printit
%         %prpath='/Users/malford/Dropbox/SharedProjects/WashingtonWaves/analysis/'
%         WritePDF([cruise '_velplot'],prpath);
%     end
% end