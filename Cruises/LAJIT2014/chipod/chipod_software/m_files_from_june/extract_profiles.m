function [down_inds up_inds]=extract_profiles(in,thres,smooth);

if nargin<3
	smooth=0;
end

if size(in,2)~=1
	in=in';
end

in_sm=conv2(in,ones(smooth,1)/smooth);

%plot(in_sm);

good_inds=find(in>thres);

prof_inds=find(diff(good_inds)>1)

end_inds=good_inds([prof_inds; end])
beg_inds=good_inds([1 ; prof_inds+1])

for a=1:(length(prof_inds)+1)
	inds{a}=beg_inds(a):end_inds(a)
	this_in=in(inds{a});
	[dummy,inn]=max(in(inds{a}));
	down_inds{a}=inds{a}(1:inn);
	up_inds{a}=inds{a}(inn:end);
end