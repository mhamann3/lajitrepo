%%
%ctd calibration data for IWISE11, starting 10 June

%%

% ctd
cfg.ctdsn = '1';

% c1
cfg.c1sn = 3860; %aw
cfg.c1cal.ghij = [-1.03274896e+001 1.48508936e+000 -1.19352173e-003 1.68798328e-004]; 
cfg.c1cal.ctpcor = [3.2500e-6 -9.5700e-8];
cfg.c1date = '27-Jun-13';

% t1
cfg.t1sn = 1370; %aw
cfg.t1cal.ghij = [4.84045562e-003 6.75025433e-004 2.38639563e-005 1.66384057e-006];
cfg.t1cal.f0 = 1000;
cfg.t1date = '12-Apr-13';
    
% c2 
cfg.c2sn = 1467; %AW
cfg.c2cal.ghij = [-9.92095733e+000 1.56776505e+000 -7.76918296e-004 1.52010113e-004]; 
cfg.c2cal.ctpcor = [3.2500e-6 -9.5700e-8];
cfg.c2date = '04-Oct-13';

% t2
cfg.t2sn = 1710; %AW
cfg.t2cal.ghij = [4.81564694e-003 6.72728225e-004 2.55853146e-005 1.98956171e-006];
cfg.t2cal.f0 = 1000;
cfg.t2date = '6-Mar-13';

% p 
cfg.psn = 0957;	%AW
cfg.pcal.c = [-4.701953e+004 -3.199230e-001 1.464100e-002];
cfg.pcal.d = [3.748600e-002 0.000000e+0];
cfg.pcal.t = [3.002465e+001 -3.417080e-004 4.148380e-006 2.793720e-009 0.0000e+000];
cfg.pcal.AD590 = [1.281500e-002 -9.225010e+000];
cfg.pcal.linear = [1.0 0.0];
cfg.pdate = '22-Sep-09';

% oxygen ***from PsaReport.txt not cal sheet (amy didn't update this one)
cfg.oxsn = 0255;
cfg.oxcal.soc = 4.8090e-001;
cfg.oxcal.boc = 0.0;
cfg.oxcal.tcor = 0.0009;
cfg.oxcal.pcor = 1.35e-004;
cfg.oxcal.voffset = -0.5165;
cfg.oxdate = '';
