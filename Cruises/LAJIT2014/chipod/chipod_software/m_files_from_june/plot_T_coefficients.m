%%% script to plot the differences in calibration coefficients over the
%%% period of the experiment.


V=linspace(0,4,20)';

V_vec=[ones(size(V)) V V.^2 V.^3 0*ones(size(V))];

ccal.coef1(3,:)
clf
ranges={[2:77],[78:80],[81:85],[2:85]}
for b=1:4
	subplot(2,2,b)
	for a=ranges{b} %1:114
		T1=ccal.coef1(a,:)*V_vec';
		T2=ccal.coef2(a,:)*V_vec';
		if T1(1)<-1 & T1(1)>-25 & T2(1)<-1 & T2(1)>-25
			
			plot(V,T1',V,T2');
			hold on
			xlabel('raw voltage'), ylabel('Temperature [c]')
		end
	end
	xlim([1 2])
	ylim([0 10])
grid on
	title(['unit 1002, casts' num2str(ranges{b}(1)) ' - ' num2str(ranges{b}(end))])
end
print -r300 -dpng eps/cali_coefs.png