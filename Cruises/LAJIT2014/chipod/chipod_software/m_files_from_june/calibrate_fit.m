function [uc,coef]=calibrate_fit(uc,c,mini,maxi,flag)
% function [uc,coef]=calibrate_uc(uc,c,min,max,flag) determines the calibration
% coefficients for the microconductivity sensor based on the N-B 
% conductivity cell over the range of indices from mini to maxi,
% corresponding to the C series.  
%  
% Note that C already be calibrated.  Also note that this doesn't
% despike.  But we do get rid of the bad data, allowing for NaNs in
% the either series.
  
  if size(uc,2)==2
    uc=ch(uc);
  end
  uc=makelen(uc,length(c));
  goodinds=intersect(find(~isnan(uc)),find(~isnan(c)));
  uc=uc(goodinds);
  c=c(goodinds);
  
  if nargin<3
    mini=1;, maxi=length(c);
  end
  
  step=ceil((maxi-mini)/400);
  
[b,a]=butter(2,.05);
uc_filt=filtfilt(b,a,uc);
c_filt=filtfilt(b,a,c);
index=mini:step:maxi;
cf=c_filt(index);
ucf=uc_filt(index);
b=regress(cf,[ones(size(ucf)) ucf ucf.^2 ucf.^3]); 
uc=b(1)+uc*b(2)+uc.^2*b(3)+uc.^3*b(4);
coef=[b' 1];
% plot(cf,'r')
% hold on
% plot(b(1)+ucf*b(2)+ucf.^2*b(3)+ucf.^3*b(4));