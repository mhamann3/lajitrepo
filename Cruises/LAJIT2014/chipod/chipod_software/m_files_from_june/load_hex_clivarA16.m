%function load_hex_mendo = load_hex_iwise(icast)
addpath ./mfiles_from_jen/ctd_proc2
cruise= 'Clivar A16'; 
cruise='rh1';
cruise_out='A16S_';
%
disp('=============================================================')

datadir='../data/A16S/CTD_RawData/';
outdir='../data/A16S/processed/CTD/';



ctdlist = dirs(fullfile(datadir, [cruise '*.hex']))

for icast=29 % % 5:length(ctdlist); 
close all


ctdname = [datadir ctdlist(icast).name]
paus
outname=ctdlist(icast).name;
matname = [outdir outname(1:end - 4) '.mat'];
matname25 = [outdir '25cm/' outname(1:end - 4) '.mat'];
disp(['CTD file: ' ctdname])
%%
disp('configuring:')
% I've input all the calibration data for the sensors during MENDO12.  If
% we swap out a sensor, copy this file to cgfload001 and update the new
% file. 


%cfgload000
cfgload_clivarA16

% 24 Hz data 
disp('loading:')
% *** include ch4
d = hex_read(ctdname);

disp('parsing:')
data1 = hex_parse(d);

% check for modcount errors
dmc = diff(data1.modcount);
mmc = mod(dmc, 256);
%figure; plot(mmc); title('mod diff modcount')
fmc = find(mmc - 1); 
if ~isempty(fmc); 
  disp(['Warning: ' num2str(length(dmc(mmc > 1))) ' bad modcounts']); 
  disp(['Warning: ' num2str(sum(mmc(fmc))) ' missing scans']); 
end

% check for time errors
dt = data1.time(end) - data1.time(1);
ds = dt*24;
np = length(data1.p);
mds = np - ds;
if abs(mds) >= 24; disp(['Warning: ' num2str(mds) ' difference in time scans']); end

% time is discretized
nt=length(data1.time);
time0=data1.time(1):1/24:data1.time(end);

% convert freq, volatage data
disp('converting:')
% *** fl, trans, ch4 
data2 = physicalunits(data1, cfg);
data2.time0=time0'/24/3600+datenum([1970 1 1 0 0 0])

%% output raw data for someone
disp(['saving: ' matname])
matname0 = [outdir outname(1:end - 4) '_raw.mat'];
save(matname0, 'data2')

try
do_higher_order=1;
if do_higher_order
tic
%% specify the depth range over which t-c lag fitting is done. For deep
% stations, use data below 500 meters, otherwise use the entire depth
% range. 
if max(data2.p)>800
    data2.tcfit=[500 max(data2.p)];
else
    data2.tcfit=[200 max(data2.p)];
end
%%
disp('cleaning:')
data3 = ctd_cleanup(data2, icast);

disp('correcting:')
% ***include ch4
[datad4, datau4] = ctd_correction_updn(data3); % T lag, tau; lowpass T, C, oxygen

disp('calculating:')
% *** despike oxygen
datad5 = swcalcs(datad4, cfg); % calc S, theta, sigma, depth
datau5 = swcalcs(datau4, cfg); % calc S, theta, sigma, depth

%%
disp('removing loops:')
wthresh = 0.1;
datad6 = ctd_rmloops(datad5, wthresh, 1);
datau6 = ctd_rmloops(datau5, wthresh, 0);

%% despike

datad7 = ctd_cleanup2(datad6);
datau7 = ctd_cleanup2(datau6);


%%
dobin=1;
if dobin
disp('binning:')
dz = 0.25; % m
zmin = 0; % surface
[zmax, imax] = max([max(datad7.depth) max(datau7.depth)]);
zmax = ceil(zmax); % full depth
datad = ctd_bincast(datad7, zmin, dz, zmax);
datau = ctd_bincast(datau7, zmin, dz, zmax);
datad.datenum=datad.time/24/3600+datenum([1970 1 1 0 0 0]);
datau.datenum=datau.time/24/3600+datenum([1970 1 1 0 0 0]);

disp(['saving: ' matname])
save(matname25, 'datad', 'datau')

end

%% 1-m bin
dobin=1;
if dobin
disp('binning:')
dz = 1; % m
zmin = 0; % surface
[zmax, imax] = max([max(datad7.depth) max(datau7.depth)]);
zmax = ceil(zmax); % full depth
datad_1m = ctd_bincast(datad7, zmin, dz, zmax);
datau_1m = ctd_bincast(datau7, zmin, dz, zmax);
datad_1m.datenum=datad_1m.time/24/3600+datenum([1970 1 1 0 0 0]);
datau_1m.datenum=datau_1m.time/24/3600+datenum([1970 1 1 0 0 0]);



disp(['saving: ' matname])
save(matname, 'datad_1m', 'datau_1m') %,'datad','datau')
toc
end
end
catch 
end
%%


%%
end
