function [data,prof_dn,prof_up]=load_rbr_duo(thepath,basefile)

addpath ~/matlab/RSKtools/

% thepath='/Users/nash/analysis/CTD-chipod/data/A16S/RBRuplooker/';
%D=dir([thepath '050633*.rsk']);
D=dir([thepath basefile '*.rsk']);
data2=[];
for aaa=1:length(D)

	fname=D(aaa).name;
	[RSK,dbid] = RSKopen([thepath fname]);
	out=RSKreaddata(RSK);
	%
	if strcmp(out.instruments.model,'RBRduo')
		data.time=out.data.tstamp;
%		data.C=NaN*out.data.values(:,1); % there is no C.
		data.T=out.data.values(:,1);
		data.P=out.data.values(:,2)-10;
		
	else
		data.time=out.data.tstamp;
		data.C=out.data.values(:,1);
		data.T=out.data.values(:,2);
		data.P=out.data.values(:,3)-10;
	end
	data2=mergefields(data2,data,1,1)
end
data=data2
clear data2
	%%
	clear big
	[tmp, tmp_u]=extract_profiles(data.P,10,5);
	
	clf
	big.inds=tmp;
	clear profiles
	plot(data.time,data.P,'b-'), hold on
	a=1;
	for b=1:length(big.inds)
		if length(tmp{b})>500
			plot(data.time(tmp{b}),data.P(tmp{b}),'r');
			profiles{a}.inds=tmp{b};
			profiles{a}.time=data.time(tmp{b});
			profiles{a}.T=data.T(tmp{b});
			profiles{a}.P=data.P(tmp{b});
			plot(profiles{a}.time(end),profiles{a}.P(end),'r.')
			a=a+1;
		end
	end
	prof_dn=profiles;
	tmp=tmp_u;
	for b=1:length(big.inds)
		if length(tmp{b})>500
			plot(data.time(tmp{b}),data.P(tmp{b}),'r');
			profiles{a}.inds=tmp{b};
			profiles{a}.time=data.time(tmp{b});
			profiles{a}.T=data.T(tmp{b});
			profiles{a}.P=data.P(tmp{b});
			plot(profiles{a}.time(end),profiles{a}.P(end),'r.')
			a=a+1;
		end
	end
	prof_up=profiles;

	
return	
	%%
	delz=1;
	maxz=200;
	new_z=[delz:delz:maxz];
	init=NaN*ones(length(new_z),length(big.inds));
	big.Z=new_z';
	big.C=init;
	big.T=init;
	big.P=init;
	big.time=init;
	
	
	for b=1:length(big.inds)
		for a=1:length(new_z)
			inds=find(data.P(big.inds{b})>(new_z(a)-delz/2) & data.P(big.inds{b})<(new_z(a)+delz/2));
			big.C(a,b)=nanmean(data.C(big.inds{b}(inds)));
			big.T(a,b)=nanmean(data.T(big.inds{b}(inds)));
			big.P(a,b)=nanmean(data.P(big.inds{b}(inds)));
		end
		big.S(:,b)=sw_salt(big.C(:,b)./sw_c3515,big.T(:,b),big.P(:,b));
		big.sigma(:,b)=sw_pden(big.S(:,b),big.T(:,b),big.P(:,b),0);
	end
	
	save([thepath fname(1:(end-4))],'big')



