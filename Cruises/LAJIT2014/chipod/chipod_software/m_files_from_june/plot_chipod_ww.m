% load sensor_configs
wirew{1}.drews_name='mm';
wirew{2}.drews_name='gfk';
wirew{3}.drews_name='odb';
wirew{1}.deploys=[2:4]; %2:4
wirew{2}.deploys=[1:5];
wirew{3}.deploys=[1:3];

load sensor_configs

% first number is the WW, the second is the deployment number 
configs(1,1).lags=[0 0];
configs(1,2).lags=[-9.9 -9.9]; ; % [-21 -1800]+13-3;
configs(1,3).lags=[-3.9 -3.9];
configs(1,4).lags=[-3.9 -3.9];
chi_files_before=14;

configs(3,1).lags=[4.7 3.22];
configs(3,2).lags=[2.5 0.5];
configs(3,3).lags=[.45 -2.4];

configs(2,1).lags=[-3 -3];
configs(2,2).lags=[-7 -7.2];
configs(2,2).lags=[-7.85 -7.8];
configs(2,3).lags=[-6.3 -6.4];
configs(2,4).lags=[-6.53 -6.73];
configs(2,5).lags=[-7 -7.3];


% Try a couple of cases...
for wwalks=1:3
wirewalker_number=wwalks;
drews_name=wirew{wwalks}.drews_name;
to_dos=wirew{wwalks}.deploys;

% plot_chipod_ww
for the_case=to_dos(1:end);
	clear chipod chi big ww
    other_name='asiri3';
    other_name=['asiri' num2str(wirewalker_number)];
    ww_data_path=['../data/processed/wirewalkers/' other_name '/'];
    chipod_data_path=['../data/chipod_ww/' other_name '/'];
    ww_fname=[drews_name '_d' num2str(the_case)];
    ww_fname=[drews_name '_d' num2str(the_case)];
	fig_title=['ASIRI-' num2str(wirewalker_number) ', "' upper(drews_name) '", deployment ' num2str(the_case)'];
	
    switch the_case
        case 1
        case 2
        case 3
    end
% step one: load wirewalker data:

do_raw=0

if do_raw

load([ww_data_path ww_fname]);

eval(['ww=' ww_fname ';']);
eval(['clear ' ww_fname ';']);

%%

% load appropriate chipod data (not quite sure how to do this efficiently?)
% Hardwire for the time being. 

ww.mlab_time=719529+ww.time/24/3600;
ww.chipod_lags=configs(wirewalker_number,the_case).lags;
ww.tlims=ww.mlab_time([1 end]); % these are the time limits of the wirewalker data.
ww.chipod_time=ww.mlab_time+linspace(ww.chipod_lags(1),ww.chipod_lags(2),length(ww.mlab_time))'/24/3600;
the_files=dir([chipod_data_path other_name '*'])
big=[];
nfiles=length(the_files)
for a=1:nfiles
	fname=the_files(a).name;
	file_time=fname(8:15);
	if datenum(file_time,'yymmddhh')>(ww.tlims(1)-chi_files_before/24) & datenum(file_time,'yymmddhh')<(ww.tlims(2)+2/24)
		% we've got the right file, so let's load it. 
		fname=[chipod_data_path the_files(a).name];
		try 
		[out,counter]=load_mini_chipod(fname);
		catch
		[out,counter]=load_mini_chipod(fname,8400);
		end
		chidat.date=counter';
		chidat.T=out(:,2)';
		chidat.TP=out(:,1)';
		big=mergefields(big,chidat);
%	else
		% do nothing.
	end

end

%% determine temperature calibration:

	do_plot=1
	if do_plot
		figure(1)
		h(1)=subplot(211)
		plot(big.date(1:10:end),big.T(1:10:end));
		h(2)=subplot(212)
		plot(ww.chipod_time,ww.T)
		linkaxes(h,'x')
	end

	%paus
	%chi.time_offset=-2.4/3600/24;
	chi.date=big.date%-chi.time_offset;
	
% Now determine TP gain...

% determine transfer_coefficients for the Temperature probe.
% use calibrate_fit:

%chi.T=10.9*(big.T-2.2079)+16.27+(+1*(big.T-2.2079).^2)+.1;

%paus

do_calibrations=0
if do_calibrations
	
	
	% first select a profile (find a good profile?)
	prof=2 % and 195; %
	%prof=292
	
	ww_inds=[ww.idx(prof,1):ww.idx(prof,2)];
	chi_inds=find(chi.date>ww.chipod_time(ww_inds(1)) & ...
		chi.date<ww.chipod_time(ww_inds(end)));
	figure(103)
	%plot(ww.T(ww_inds),ww.chipod_time(ww_inds),chi.T(chi_inds),chi.date(chi_inds))
	
	ww_T2=interp1(ww.chipod_time(ww_inds),ww.T(ww_inds),chi.date(chi_inds));
	
	[temp,coef]=calibrate_fit(big.T(chi_inds)',ww_T2');
	
	configs(wirewalker_number,the_case).coef_T=coef;
	
	chi.T=calibrate_poly(big.T,coef);
	oset=ww.mlab_time(ww_inds(1))
	%plot(ww.T(ww_inds),ww.mlab_time(ww_inds),chi.T(chi_inds),chi.date(chi_inds))
	figure(105)
	plot(ww.chipod_time(ww_inds)-oset,ww.T(ww_inds),chi.date(chi_inds)-oset,chi.T(chi_inds))
	
	do_plot=1
	if do_plot
		figure(106)
		plot(ww.chipod_time,ww.T,chi.date(1:10:end),chi.T(1:10:end));
	end
	save sensor_configs configs
	%paus
else
	chi.T=calibrate_poly(big.T,configs(wwalks,the_case).coef_T);
end	

% here is what I think the calibrations are for ASIRI3, deployment 2.
%chi.T=10.9*(big.T-2.2079)+16.27+(+1*(big.T-2.2079).^2)+.1;
if strcmp(drews_name,'mm')
	if the_case==2
		chi.dTdt=-118*(big.TP-2.047);
	elseif the_case==3
		chi.dTdt=-112*(big.TP-2.047);
	else
		chi.dTdt=-124*(big.TP-2.047);
	end
else
	chi.dTdt=-121*(big.TP-2.047);
end

chi.dTdt_dig=[0 diff(chi.T)/(1/100)];

determine_transfer_function=0;

if determine_transfer_function
	figure(2)
	plot(chi.date(1:10:end),chi.T(1:10:end),ww.chipod_time,ww.T);
%	axis(ax)
	
	% determine the transfer function
	figure(3)
	
%	chi.dTdt_dig=[0 diff(chi.T)/(1/100)];% we already did this...
%	chi.dTdt=-121*(big.TP-2.047);
	oset=min(chi.date);
	plot(chi.date-oset,chi.dTdt_dig,chi.date-oset,chi.dTdt);
	paus
	ax=axis
	ginds=find((chi.date-oset)>ax(1) & (chi.date-oset)<ax(2));
	[p,f]=fast_psd(chi.dTdt(ginds),256,100);
	[p2,f]=fast_psd(chi.dTdt_dig(ginds),256,100);
	figure(4)
	loglog(f,p,f,p2);
	paus
end

%%

% now it's time to put the chipod upcast data into profiles, and assign
% pressure and fallspeed to each datapoint.

tmp=diff(ww.time);tmp(tmp>10)=NaN;ww.delt=nanmean(tmp); % this is the sampling frequency

delz=1;
p_bins=[0.5:1:140];

for a=1:length(ww.idx)
	% run through each profile, compute dT/dt, dT/dz, N2 and ultimately
	% chi....	
	ww_times=ww.mlab_time(ww.idx(a,:));
	ginds=find(chi.date>ww_times(1) & chi.date<ww_times(2));
	if length(ginds)>1000 % we have at least 10 seconds of a profile...
		chipod(a).date=chi.date(ginds);
		chipod(a).T=chi.T(ginds);
		chipod(a).dTdt=chi.dTdt(ginds);
		chipod(a).dTdt_dig=chi.dTdt_dig(ginds);
		chipod(a).P=interp1(ww.chipod_time,ww.P,chi.date(ginds));
		chipod(a).S=interp1(ww.chipod_time,ww.S,chi.date(ginds));
		try
		chipod(a).sigma=interp1(ww.chipod_time,ww.sigma,chi.date(ginds));
		catch
		chipod(a).sigma=interp1(ww.chipod_time,ww.rho,chi.date(ginds));
		end
		tmp=diff(ww.P)./ww.delt;tmp(end+1)=tmp(end);, tmp(tmp>10)=NaN;
		tmp=interp1(ww.mlab_time,tmp,chi.date(ginds));tmp([1 end])=NaN;
		tmp=conv2(tmp,ones(1,round(16/ww.delt))/(round(16/ww.delt)),'same');
		chipod(a).fallspd=extrapolate_data(tmp,'both');
		chipod(a).T_ww=interp1(ww.chipod_time,ww.T,chi.date(ginds));
		
		% now let's compute a second estimate of the mean gradients:
		the_filt=ones(6,1)/6;
		N2=conv2([0 ; sw_bfrq(ww.std_profiles.S(:,a),ww.std_profiles.T(:,a),ww.std_profiles.P',10)],the_filt,'same');
		dTdz=conv2([0 ; diff(ww.std_profiles.T(:,a))]./mean(diff(ww.std_profiles.P)),the_filt,'same');
		
		% now let's attempt to compute chi:
		
		chipod(a).P_bin=p_bins;
		chipod(a).N2_lp=interp1(ww.std_profiles.P,N2,p_bins);
		chipod(a).dTdz_lp=interp1(ww.std_profiles.P,dTdz,p_bins);
		nfft=256;fmax=20;gamma=0.2; % fmax and gamma not used...
		init=NaN*p_bins;chipod(a).chi=init;chipod(a).dTdz=init;chipod(a).N2=init;
		chipod(a).chi=init;chipod(a).eps=init;
		chipod(a).KT=init;chipod(a).Krho=init;
		chipod(a).chi2=init;chipod(a).eps2=init;
		chipod(a).KT2=init;chipod(a).Krho2=init;
		chipod(a).fspd_used=init;%chipod(a).Krho=init;
		for b=1:length(p_bins);
			inds=find(chipod(a).P>(p_bins(b)-delz) & chipod(a).P<(p_bins(b)+delz));
			if length(inds)>nfft
				chipod(a).N2(b)=9.8*(max(chipod(a).sigma(inds))-min(chipod(a).sigma(inds)))/...
					(max(chipod(a).P(inds))-min(chipod(a).P(inds)))/1030;
				chipod(a).dTdz(b)=(max(chipod(a).T(inds))-min(chipod(a).T(inds)))/...
					(max(chipod(a).P(inds))-min(chipod(a).P(inds)));
				chipod(a).fspd_used(b)=abs(nanmean(chipod(a).fallspd(inds)));
				S=nanmean(chipod(a).S(inds));
				T=nanmean(chipod(a).T(inds));
				nu=sw_visc(S,T,p_bins(b));
				tdif=sw_tdif(S,T,p_bins(b));

				[tp_power,freq]=fast_psd(chipod(a).dTdt(inds),nfft,100);
				
				           
fixit=1;
if fixit
                trans_fcn=0;
                trans_fcn1=0;
                thermistor_filter_order=2;
                thermistor_cutoff_frequency=32;
                analog_filter_order=4;
                analog_filter_freq=50;

                tp_power=invert_filt(freq,invert_filt(freq,tp_power,thermistor_filter_order, ...
                thermistor_cutoff_frequency),analog_filter_order,analog_filter_freq);
end

				
%				    [chi1,epsil1,k,spec,kk,speck,stats]=compute_chipod_chi3(freq,tp_power,chipod(a).fspd_used(b),nu,...
%						tdif,chipod(a).dTdz(b),chipod(a).N2(b));
%					[chi2,epsil2,k,spec,kk,speck,stats]=compute_chipod_chi3(freq,tp_power,chipod(a).fspd_used(b),nu,...
%						tdif,chipod(a).dTdz_lp(b),chipod(a).N2_lp(b));
				
				try
				    [chi1,epsil1,k,spec,kk,speck,stats]=get_chipod_chi(freq,tp_power,chipod(a).fspd_used(b),nu,...
						tdif,chipod(a).dTdz(b),'nsqr',chipod(a).N2(b));
				catch
					chi1=NaN;
					epsil1=NaN;
				end
				try
					[chi2,epsil2,k,spec,kk,speck,stats]=get_chipod_chi(freq,tp_power,chipod(a).fspd_used(b),nu,...
						tdif,chipod(a).dTdz_lp(b),'nsqr',chipod(a).N2_lp(b));
				catch
					chi2=NaN;
					epsil2=NaN;
				end
					%,...
                                      %    'fmax',fmax,...
                                      %    'gamma',gamma,...
                                      %'corrections',corrections, ...
                                      %'f_c',f_c,...
                                      %'doplots',0,...
                                      %'filt_ord',filt_ord);
									  chipod(a).chi(b)=chi1(1);
									  chipod(a).eps(b)=epsil1(1);
									  chipod(a).KT(b)=0.5*chi1(1)/chipod(a).dTdz(b)^2;
									  chipod(a).Krho(b)=0.2*epsil1(1)/chipod(a).N2(b);
									  chipod(a).chi2(b)=chi2(1);
									  chipod(a).eps2(b)=epsil2(1);
									  chipod(a).KT2(b)=0.5*chi2(1)/chipod(a).dTdz_lp(b)^2;
									  chipod(a).Krho2(b)=0.2*epsil2(1)/chipod(a).N2_lp(b);
									  doplot=0;
									  if doplot
									  clf
									  loglog(k,spec), hold on, axis(axis), loglog(kk,speck,'g');
									  jtext(['chi=' num2str(chi1(1)) ', epsilon=' num2str(epsil1(1))]);
									  fig
									  pause(.5)
									  end

			end
			
		end
		
		
		do_plot=1
		if do_plot
			figure(100)
			clf
			subplot(151)
			[h(1),h(2)]=plotxx(chipod(a).T,-chipod(a).P,chipod(a).S,-chipod(a).P);
			axes(h(1)),xlim([18 29])
			xlabel('T (g,b)')
			hold on
			plot(chipod(a).T_ww,-chipod(a).P,'g')
			axes(h(2)),xlim([32 35]);
			xlabel('S (r)')
			h(3)=subplot(152)
			plot(chipod(a).dTdt,-chipod(a).P),xlim([-3 3])
			xlabel('dT/dt')
			%axis tight, xl=xlim;
			%plot(chipod(a).dTdt_dig+1,-chipod(a).P,chipod(a).dTdt,-chipod(a).P)
			%xlim(xl);
			h(4)=subplot(153)
			plot(chipod(a).fallspd,-chipod(a).P);
			title([other_name '_' ww_fname '_' num2str(1000+a)],'interpreter','none')
			subplot(154)
			[h(5),h(6)]=plotxx(log10(chipod(a).chi),-chipod(a).P_bin,log10(chipod(a).eps),-chipod(a).P_bin);
			set(h(5),'xlim',[-10 -6]);
			set(h(6),'xlim',[-10 -7]);
			axes(h(5))
			xlabel('\chi [K^2/s]')
			axes(h(6))
			xlabel('\epsilon [W/kg]')
			h(7)=subplot(155)
%			plot(log10(chipod(a).KT),-chipod(a).P_bin,log10(chipod(a).Krho),-chipod(a).P_bin,log10(chipod(a).KT2),-chipod(a).P_bin,log10(chipod(a).Krho2),-chipod(a).P_bin)
			plot(log10(chipod(a).KT),-chipod(a).P_bin)
			xlim([-8 -4])
			fig,linkaxes(h,'y')
			orient landscape
			print('-dpng',['jpeg_profiles/' other_name '_' ww_fname '_' num2str(1000+a)])  
		end
	end
a
end

%%
tfields={'chi','eps','KT'};
for aa=1:length(tfields), ww.std_profiles.(tfields{aa})=NaN*ww.std_profiles.T;,end
for a=1:length(chipod)
	if ~isempty(chipod(a).KT)
		for aa=1:3
			ww.std_profiles.(tfields{aa})(:,a)=interp1(chipod(a).P_bin,chipod(a).(tfields{aa}),ww.std_profiles.P);
		end
	end
end

save(['chipod_' ww_fname],'chipod','ww')

save(['../data/processed/chipod_ww/chi_' ww_fname],'ww')

else 
load(['chipod_' ww_fname])
end





%%
if ~isfield(ww.std_profiles,'sigma')
	ww.std_profiles.sigma=ww.std_profiles.rho;
end

ww.std_profiles.mtime=interp1(ww.time,ww.mlab_time,ww.std_profiles.time);
figure(101)
clf
subplot(211)
pcolor(ww.std_profiles.mtime,-ww.std_profiles.P,log10(ww.std_profiles.chi)),shading flat
caxis([-9.5 -6.5]), colorbar
title(fig_title)
hold on 
kdatetick
%[c,h]=contour(ww.std_profiles.time,-ww.std_profiles.P,(ww.std_profiles.S),'k')
subplot(212)
pcolor(ww.std_profiles.mtime,-ww.std_profiles.P,(ww.std_profiles.S)),shading flat
hold on 
caxis(caxis);
[c,h]=contour(ww.std_profiles.mtime,-ww.std_profiles.P,(ww.std_profiles.sigma),'k');
colorbar
%subplot(211)
%pcolor(ww.std_profiles.time,-ww.std_profiles.P,log10(ww.std_profiles.KT)),shading flat
%pcolor(ww.std_profiles.time,-ww.std_profiles.P,log10(ww.std_profiles.KT)),shading flat
% colorbar
kdatetick
%print -dpng -r200 png/asiri3_3.png
print('-dpng','-r200',['png/' ww_fname ])
end


end
