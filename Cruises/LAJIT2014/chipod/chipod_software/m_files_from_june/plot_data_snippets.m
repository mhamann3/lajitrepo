% plot data Snippets

% first do a "Process_chipod_script.m" to get the data loaded you want.
% I'm using file 27, which I think is a good one....


prange=[1760 1820];
prange=[1100 1240];
prange=[100 7000];

[dummy,maxi]=max(avg.P);
tmp=find(avg.P(1:maxi)>prange(1) & avg.P(1:maxi)<prange(2));
avg_inds=tmp(1):tmp(end);
[dummy,maxi]=max(cal.P);
tmp=find(cal.P(1:maxi)>prange(1) & cal.P(1:maxi)<prange(2));
cal_inds=tmp(1):tmp(end);
[dummy,maxi]=max(data2.p);
tmp=find(data2.p(1:maxi)>prange(1) & data2.p(1:maxi)<prange(2));
data_inds=tmp(1):tmp(end);
tmp=find(datad.p>prange(1) & datad.p<prange(2));
datad_inds=tmp(1):tmp(end);


cal.T1P_lp=conv2(cal.T1P,ones(5,1)/5,'same');
cal.T1_lp=conv2(cal.T1,ones(5,1)/5,'same');

data2.s1=sw_salt(10*data2.c1./sw_c3515,data2.t1,data2.p);
data2.sigma2=sw_pden(data2.s1,data2.t1,data2.p,2000);
datad.sigma1=sw_pden(datad.s1,datad.t1,datad.p,1000);
datad.sigma2=sw_pden(datad.s1,datad.t1,datad.p,2000);
avg.sigma1=sw_pden(avg.S,avg.T,avg.P,1000);
avg.sigma2=sw_pden(avg.S,avg.T,avg.P,2000);
avg.sigma3=sw_pden(avg.S,avg.T,avg.P,3000);
avg.sigma4=sw_pden(avg.S,avg.T,avg.P,4000);

%%
clf
h(1)=subplot(1,6,1)

plot(avg.fspd(avg_inds),avg.P(avg_inds),'color',[.5 .5 .5]);
fix_fig, axis ij
ylabel('Depth [m]')
set(gca,'yticklabelmode','auto')
xlabel('vertical speed [m/s]')
xlim([-2 0])
jtext('speed',0.02,1.03,'fontsize',14)

h(2)=subplot(1,6,2)
%plot(avg.S(avg_inds),avg.P(avg_inds),datad.s1(datad_inds),datad.p(datad_inds));
plot(datad.s1(datad_inds),datad.p(datad_inds),'r');
fix_fig, axis ij
xlabel('S [psu]')
jtext('salinity',0.02,1.03,'fontsize',14)

h(3)=subplot(1,6,3)
plot(datad.sigma2(datad_inds)-1000,datad.p(datad_inds),'g');
fix_fig, axis ij
xlabel('\sigma _{2000} [kg/m^3]')
jtext('density',0.02,1.03,'fontsize',14)
%plot(datad.t1(datad_inds),datad.p(datad_inds),cal.T1_lp(cal_inds),cal.P(cal_inds));

h(4)=subplot(1,6,4)
plot(cal.T1_lp(cal_inds),cal.P(cal_inds));
fix_fig, axis ij
xlabel('T [C]')
jtext('temperature',0.02,1.03,'fontsize',14)


h(5)=subplot(1,6,5)
%plot(cal.T1P_lp(cal_inds),cal.P(cal_inds));
plot(cal.T1P(cal_inds),cal.P(cal_inds));
xlim([-.5 .5])
fix_fig, axis ij
xlabel('dT/dt [K/s]')
jtext('temp gradient',0.02,1.03,'fontsize',14)


h(6)=subplot(1,6,6)
plot(log10(avg.TP1var(avg_inds)),avg.P(avg_inds));
fix_fig, axis ij
xlabel('log_{10}var(dT/dt) [K^2/s^2]')
jtext('gradient variance',0.02,1.03,'fontsize',14)


linkaxes(h,'y')
fig

dual_print_pdf2('example1','same')
dual_print_pdf2('example1','same')
dual_print_pdf2('example8','same')
