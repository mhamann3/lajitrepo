function [inds]=get_profile_inds(p,min_p);

[dummy,maxi]=max(p);

first_ind=max(find(p(1:maxi)<min_p));
last_ind=min(find(p(maxi:end)<min_p))+maxi;
inds=first_ind:last_ind;