function [sig_sort,T_sort]=smart_sort(sigma,T,sort_dir);
% function to resort in density and temperature independently, but over
% regions where density is piecewise unstable.

if nargin==2
    sort_dir='descend';
end

%sort_dir='descend'
%sigma=x.sigma;
%T=x.T_gl;

%sigma=glider.SigT(gl.inds);
%T=glider.Temp(gl.inds);
sig_sort=NaN*sigma;
T_sort=NaN*T;

[sigma,good_inds]=nonnan(sigma);
T=T(good_inds);

[sig_sortx sig_inds]=sort(sigma,sort_dir);
sig_sort(good_inds)=sig_sortx;
T_sortx=T; % this is the first guess...

tmp=cumsum(sig_inds-[1:length(sig_inds)]'); % this is nonzero only in patches...

patch_inds=find(tmp>0);

patch_boundaries=[1 ; find(diff(patch_inds)>1) ; length(patch_inds)];

start_inds=patch_inds([1 ; patch_boundaries(2:end-1)+1]);
stop_inds=patch_inds([patch_boundaries(2:end)]);

doplot=0;
if doplot
    clf
    h(1)=subplot(211)
    plot(sigma)
    hold on
    h(2)=subplot(212)
    plot(T)
    hold on
    linkaxes(h,'x')
end
maxi=length(sigma);
for a=1:length(start_inds)
    these_inds=(start_inds(a):stop_inds(a));
    
    % now determine which way the gradient is.  
    back_grad=T(these_inds(end))-T(these_inds(1));
    minT=min(T(these_inds));
    maxT=max(T(these_inds));
    
    flag=1;
	these_inds_orig=these_inds;
    if back_grad>0
        ss='ascend';
        for b=1:5
            if maxT>T(these_inds(end)+1)
                these_inds=[these_inds these_inds(end)+1];
                these_inds=these_inds(these_inds<maxi);
            else
                flag=0;
            end
        end
    else
        ss='descend';
        for b=1:5
            if minT<T(these_inds(end)+1)
                these_inds=[these_inds these_inds(end)+1];
                these_inds=these_inds(these_inds<maxi);
            else 
                flag=0;
            end
        end
    end
    
    if flag
        these_inds=these_inds_orig;
        % we failed; the adding of extra data did not help
    end
    
    % now determine if we need to go further
    
    T_sortx(these_inds)=sort(T(these_inds),ss);
    T_sort(good_inds)=T_sortx;
	
if doplot    
    subplot(211)
    plot(these_inds,sig_sort(these_inds),'r','linewidth',2)
    subplot(212)
    plot(these_inds,T_sort(these_inds),'r','linewidth',2)
end
 



end







