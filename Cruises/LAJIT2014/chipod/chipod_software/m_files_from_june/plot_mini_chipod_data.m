clear all

serialnum_list=[1010]
castnum=06;
t_step=36000;

if t_step==10
    x_tick='HH:MM';
elseif t_step==3600*5
    x_tick='MM';
else x_tick='SS';
end

for serialnum=serialnum_list;
    
    thepath=['../data/Ttide2015/Chipod_CTD/' num2str(serialnum) '/'];
    D=dir([thepath '*.A*'])
    
    n=length(D)
    
    [data,tme]=load_mini_chipod([thepath D(n).name]);
    
end
cast=num2str(castnum);
dvt=datevec(tme(1));
mm=dvt(2);
dd=dvt(3);
t_start=1;
t_end=length(tme);
% ctdpath='../data/Beaufort2014/CTD/';
% ctdfile='2014-11-0015.cnv';
% fname=[ctdpath ctdfile];
% headerlines=320;
% [T,S,P,D,SN,JD,t]=load_CTD_data(fname,headerlines);
% figure(1); clf;
% plot(T,D)
% axis ij tight
% xlim([-1.5 1])
% ii_t = find(tme>=t(1) & tme<=t(end));
% t_end   = find();
% tchi=tme(t_start:t_end);
% 
% [Tctd,Sctd,Pctd,Dctd,SNctd,JDctd,tctd]=interp_CTD_data(T,S,P,D,SN,JD,t,tchi);
figure(serialnum)
clf;
h(1)=subplot(311)
plot(tme(t_start:t_end),data(t_start:t_end,2))
% set(gca,'XTick',tme(t_start:t_step:t_end))
datetick('x')
title(num2str(serialnum))
axis tight
xlabel(x_tick)
ylabel('Temp [V]')
h(2)=subplot(312)
plot(tme(t_start:t_end),data(t_start:t_end,1))
datetick('x')
ylabel('dT/dt [V/s]')
h(3)=subplot(313)
plot(tme(t_start:t_end),data(t_start:t_end,3),'r')
hold on
plot(tme(t_start:t_end),data(t_start:t_end,4),'b');
datetick('x')
ylabel('AX,AZ')
linkaxes(h,'x')
fig
if 0
    %%
    time=tme(t_start:t_end);
    D=data(t_start:t_end,:);
    cast_path='../data/Beaufort2014/casts/';
    dv=datevec(tme(t_start));
    yy=sprintf('%4i',dv(1));
    MM=sprintf('%02i',dv(2));
    dd=sprintf('%02i',dv(3));
    hh=sprintf('%02i',dv(4));
    mm=sprintf('%02i',dv(5));
    filename=[cast '_' num2str(serialnum) '_' looker 'looker_' yy MM dd hh mm 'dwn.mat'];
    save([cast_path filename],'D','time')
    %%
end