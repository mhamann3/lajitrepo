function [I,start,stop]=DespikeChi(T1P,T2P,doplot);

%%% Accepts time series t1p and t2p, and finds indices contaminated by ~1
%%% second spikes. Returns a matrix I which flags spikes with zeros, good
%%% blocks are numbered conscutively. start and stop contain the first and
%%% last indices of the flagged values.

T1P=fill_data(T1P)';
T2P=fill_data(T2P)';

if nargin<3
    doplot=0;
end

nn=106;
N=length(T1P);
Nnew=floor(N./nn)*nn;
delN=(N-Nnew);

if delN==0
    T1matrix=T1P;
    T2matrix=T2P;
else
    if mod(delN,2)==0
        T1matrix=T1P(delN/2:end-delN/2-1);
        T2matrix=T2P(delN/2:end-delN/2-1);
    else
        T1matrix=T1P(floor(delN/2)-1:end-floor(delN/2)-1);
        T2matrix=T2P(floor(delN/2)-1:end-floor(delN/2)-1);
    end
end
N1=length(T1matrix)/nn;
N2=length(T2matrix)/nn;

T1matrix=reshape(T1matrix,nn,N1);
T2matrix=reshape(T2matrix,nn,N2);

T1mat=medfilt2(T1matrix,[1 50]);
T1mat=T1mat-repmat(nanmedian(T1mat),nn,1);
T2mat=medfilt2(T2matrix,[1 50]);
T2mat=T2mat-repmat(nanmedian(T2mat),nn,1);
Tmat=0.5*(T1mat+T2mat);

coefs=nan(size(Tmat));

for m=1:N1;
    coefs(:,m) = nanmean(abs(cwt(Tmat(:,m),4:7,'cgau6')));
end

coefs=medfilt2(coefs,[1 50]);
h = fspecial('motion', 250, -2);
coefs_lp = imfilter(coefs,h,'replicate');

[~,max_ind]=max(coefs_lp);

if doplot==1;
    figure;
    subplot(3,1,1);
    pcolorjw(Tmat); caxis([-.001 .001]); hold on;
    plot(1:N1,max_ind,'wo','markerfacecolor','w','markersize',2);
    
    subplot(3,1,2);
    pcolorjw(coefs);
    
    subplot(3,1,3);
    pcolorjw(coefs_lp); hold on;
    
end

%%% Now we need to make an vector of indices %%%
I=ones(size(Tmat));
for m=1:N1;
    I(max_ind(m),m)=0;
end

I=reshape(I,nn*N1,1);

if mod(delN,2)==0
    I=[nan(floor(delN/2),1);I;nan(floor(delN/2),1)];
else
    I=[nan(floor(delN/2)-1,1);I;nan(floor(delN/2),1)];
end

ind=find(I==0);
for m=1:length(ind);
    ss=max(1,ind(m)-7):min(length(I),ind(m)+7);
    I(ss)=0;
end

% I(ind-7)=0; I(ind+1)=0;
% I(ind-6)=0; I(ind+2)=0;
% I(ind-5)=0; I(ind+3)=0;
% I(ind-4)=0; I(ind+4)=0;
% I(ind-3)=0; I(ind+5)=0;
% I(ind-2)=0; I(ind+6)=0;
% I(ind-1)=0; I(ind+7)=0;

I(isnan(I)==1)=0;

start=[1;find(diff(I)<0)+1];
stop=[find(diff(I)>0);length(I)];

for m=1:length(start)-1;
    I(stop(m)+1:start(m+1)-1)=m;
end

