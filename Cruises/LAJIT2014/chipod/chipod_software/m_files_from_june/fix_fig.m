function fix_fig()
set(gca,'linewidth',1,'yticklabel','','fontsize',14)
resize_gca([-.02 0 .02])
hh=get(gca,'children'),set(hh,'linewidth',1)