    LADCP processing protocol 
 
Station name         : LaJIT_005_043 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7 18 22 58 
Prof end (LADCP)     : 2014 12  7 18 37 32 
Start position       : 32�N 51.8156'   117�W 16.5228' 
End position         : 32�N 51.8100'   117�W 16.4640' 
Magnetic deviation   : 13.198288 
Time offset          : 0.000000 
Depths               : 9.558568e+00 2.246001e+02 5.818977e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.104657 
v ship speed   (m/s) : -0.011872 
x displacement   (m) : 91.470203 
y displacement   (m) : -10.375939 
bottom found at  (m) : 240.107911 
maximum instr dep(m) : 224.600087 
maximum prof dep (m) : 232.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 75033 to 75476 
 
Computer : MACI64 
Total processing time:  234 seconds 
