    LADCP processing protocol 
 
Station name         : LaJIT_003_023 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7  0  2 33 
Prof end (LADCP)     : 2014 12  7  0 12 38 
Start position       : 32�N 51.6012'   117�W 16.3213' 
End position         : 32�N 51.6048'   117�W 16.3308' 
Magnetic deviation   : 13.196803 
Time offset          : 0.000000 
Depths               : 1.025934e+01 1.434960e+02 6.327173e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.024528 
v ship speed   (m/s) : 0.011020 
x displacement   (m) : -14.839300 
y displacement   (m) : 6.667200 
bottom found at  (m) : 148.604552 
maximum instr dep(m) : 143.496004 
maximum prof dep (m) : 144.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 42082 to 42396 
 
Computer : MACI64 
Total processing time:  225 seconds 
