    LADCP processing protocol 
 
Station name         : LaJIT_004_011 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7  6 37 45 
Prof end (LADCP)     : 2014 12  7  6 50 17 
Start position       : 32�N 51.8153'   117�W 16.4520' 
End position         : 32�N 51.8016'   117�W 16.5024' 
Magnetic deviation   : 13.197994 
Time offset          : 0.000000 
Depths               : 1.026436e+01 1.625695e+02 6.319727e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.104259 
v ship speed   (m/s) : -0.033805 
x displacement   (m) : -78.403095 
y displacement   (m) : -25.421223 
bottom found at  (m) : 194.761601 
maximum instr dep(m) : 162.569477 
maximum prof dep (m) : 192.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 53915 to 54298 
 
Computer : MACI64 
Total processing time:  227 seconds 
