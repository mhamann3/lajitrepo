    LADCP processing protocol 
 
Station name         : LaJIT_005_002 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7  8 14 21 
Prof end (LADCP)     : 2014 12  7  8 28 37 
Start position       : 32�N 51.7944'   117�W 16.4916' 
End position         : 32�N 51.7968'   117�W 16.4856' 
Magnetic deviation   : 13.198149 
Time offset          : 0.000000 
Depths               : 1.012896e+01 2.035941e+02 4.865004e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.010904 
v ship speed   (m/s) : 0.005193 
x displacement   (m) : 9.333724 
y displacement   (m) : 4.444800 
bottom found at  (m) : 232.143840 
maximum instr dep(m) : 203.594085 
maximum prof dep (m) : 224.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 56801 to 57250 
 
Computer : MACI64 
Total processing time:  228 seconds 
