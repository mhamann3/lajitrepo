    LADCP processing protocol 
 
Station name         : LaJIT_002_052 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 19  7 42 
Prof end (LADCP)     : 2014 12  6 19 15 28 
Start position       : 32�N 51.6094'   117�W 16.3344' 
End position         : 32�N 51.6168'   117�W 16.3596' 
Magnetic deviation   : 13.196907 
Time offset          : 0.000000 
Depths               : 1.028959e+01 1.130529e+02 5.495512e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.084127 
v ship speed   (m/s) : 0.029323 
x displacement   (m) : -39.202987 
y displacement   (m) : 13.664398 
bottom found at  (m) : 175.953255 
maximum instr dep(m) : 113.052865 
maximum prof dep (m) : 164.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 33258 to 33501 
 
Computer : MACI64 
Total processing time:  223 seconds 
