    LADCP processing protocol 
 
Station name         : LaJIT_006_002 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7 20  0 29 
Prof end (LADCP)     : 2014 12  7 20 11 52 
Start position       : 32�N 51.8256'   117�W 16.4796' 
End position         : 32�N 51.8280'   117�W 16.4993' 
Magnetic deviation   : 13.198233 
Time offset          : 0.000000 
Depths               : 9.679031e+00 1.635769e+02 6.881943e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.044804 
v ship speed   (m/s) : 0.006508 
x displacement   (m) : -30.601182 
y displacement   (m) : 4.444800 
bottom found at  (m) : 205.082902 
maximum instr dep(m) : 163.576851 
maximum prof dep (m) : 196.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 77952 to 78295 
 
Computer : MACI64 
Total processing time:  228 seconds 
