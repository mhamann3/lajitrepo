    LADCP processing protocol 
 
Station name         : LaJIT_003_006 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 20 35 29 
Prof end (LADCP)     : 2014 12  6 20 45 54 
Start position       : 32�N 51.5664'   117�W 16.2720' 
End position         : 32�N 51.5928'   117�W 16.3092' 
Magnetic deviation   : 13.196514 
Time offset          : 0.000000 
Depths               : 1.075461e+01 1.469851e+02 6.271452e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.092594 
v ship speed   (m/s) : 0.078228 
x displacement   (m) : -57.871440 
y displacement   (m) : 48.892800 
bottom found at  (m) : 162.178506 
maximum instr dep(m) : 146.985141 
maximum prof dep (m) : 156.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 35884 to 36202 
 
Computer : MACI64 
Total processing time:  228 seconds 
