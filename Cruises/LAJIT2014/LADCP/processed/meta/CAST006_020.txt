    LADCP processing protocol 
 
Station name         : LaJIT_006_020 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  8  0 28 31 
Prof end (LADCP)     : 2014 12  8  0 43 27 
Start position       : 32�N 51.8124'   117�W 16.5036' 
End position         : 32�N 51.8028'   117�W 16.5048' 
Magnetic deviation   : 13.198249 
Time offset          : 0.000000 
Depths               : 1.012902e+01 2.160090e+02 5.665385e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.002083 
v ship speed   (m/s) : -0.019843 
x displacement   (m) : -1.866741 
y displacement   (m) : -17.779200 
bottom found at  (m) : 232.868100 
maximum instr dep(m) : 216.009031 
maximum prof dep (m) : 224.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 85978 to 86427 
 
Computer : MACI64 
Total processing time:  228 seconds 
