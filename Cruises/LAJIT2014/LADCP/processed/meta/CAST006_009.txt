    LADCP processing protocol 
 
Station name         : LaJIT_006_009 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7 21 42  8 
Prof end (LADCP)     : 2014 12  7 21 54 47 
Start position       : 32�N 51.8278'   117�W 16.5027' 
End position         : 32�N 51.8326'   117�W 16.4892' 
Magnetic deviation   : 13.198308 
Time offset          : 0.000000 
Depths               : 9.842747e+00 1.945501e+02 7.064534e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.027576 
v ship speed   (m/s) : 0.011744 
x displacement   (m) : 20.930239 
y displacement   (m) : 8.913544 
bottom found at  (m) : 228.851743 
maximum instr dep(m) : 194.550071 
maximum prof dep (m) : 220.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 81055 to 81432 
 
Computer : MACI64 
Total processing time:  222 seconds 
