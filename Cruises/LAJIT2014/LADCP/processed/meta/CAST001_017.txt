    LADCP processing protocol 
 
Station name         : LaJIT_001_017 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6  5 58 50 
Prof end (LADCP)     : 2014 12  6  6 10 21 
Start position       : 32�N 51.6012'   117�W 16.3056' 
End position         : 32�N 51.6156'   117�W 16.3128' 
Magnetic deviation   : 13.196760 
Time offset          : 0.000000 
Depths               : 9.718980e+00 1.580692e+02 5.029560e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.016210 
v ship speed   (m/s) : 0.038486 
x displacement   (m) : -11.200863 
y displacement   (m) : 26.593629 
bottom found at  (m) : 179.018173 
maximum instr dep(m) : 158.069165 
maximum prof dep (m) : 168.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 9637 to 9989 
 
Computer : MACI64 
Total processing time:  230 seconds 
