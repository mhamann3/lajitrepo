    LADCP processing protocol 
 
Station name         : LaJIT_006_024 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  8  1 22 49 
Prof end (LADCP)     : 2014 12  8  1 35 45 
Start position       : 32�N 51.8319'   117�W 16.5063' 
End position         : 32�N 51.7728'   117�W 16.4532' 
Magnetic deviation   : 13.198333 
Time offset          : 0.000000 
Depths               : 9.951490e+00 1.799792e+02 6.923428e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.106404 
v ship speed   (m/s) : -0.140986 
x displacement   (m) : 82.569212 
y displacement   (m) : -109.405371 
bottom found at  (m) : 212.127854 
maximum instr dep(m) : 179.979158 
maximum prof dep (m) : 204.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 87662 to 88048 
 
Computer : MACI64 
Total processing time:  226 seconds 
