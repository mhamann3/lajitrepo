    LADCP processing protocol 
 
Station name         : LaJIT_002_002 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6  8 51 36 
Prof end (LADCP)     : 2014 12  6  9  4  3 
Start position       : 32�N 51.6132'   117�W 16.3164' 
End position         : 32�N 51.5748'   117�W 16.3092' 
Magnetic deviation   : 13.196860 
Time offset          : 0.000000 
Depths               : 9.638918e+00 1.585746e+02 4.526824e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.014995 
v ship speed   (m/s) : -0.095203 
x displacement   (m) : 11.200894 
y displacement   (m) : -71.116800 
bottom found at  (m) : 181.942205 
maximum instr dep(m) : 158.574609 
maximum prof dep (m) : 172.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 14804 to 15195 
 
Computer : MACI64 
Total processing time:  217 seconds 
