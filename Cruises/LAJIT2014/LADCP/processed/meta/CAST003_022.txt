    LADCP processing protocol 
 
Station name         : LaJIT_003_022 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 23 48 50 
Prof end (LADCP)     : 2014 12  6 23 59 57 
Start position       : 32�N 51.5868'   117�W 16.2917' 
End position         : 32�N 51.6036'   117�W 16.3116' 
Magnetic deviation   : 13.196663 
Time offset          : 0.000000 
Depths               : 9.643927e+00 1.529680e+02 4.733419e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.046497 
v ship speed   (m/s) : 0.046647 
x displacement   (m) : -31.013705 
y displacement   (m) : 31.113600 
bottom found at  (m) : 178.008793 
maximum instr dep(m) : 152.968036 
maximum prof dep (m) : 168.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 41732 to 42064 
 
Computer : MACI64 
Total processing time:  230 seconds 
