    LADCP processing protocol 
 
Station name         : LaJIT_005_009 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7 10  1 30 
Prof end (LADCP)     : 2014 12  7 10 13 15 
Start position       : 32�N 51.7884'   117�W 16.4664' 
End position         : 32�N 51.7896'   117�W 16.4700' 
Magnetic deviation   : 13.198032 
Time offset          : 0.000000 
Depths               : 9.668887e+00 1.855828e+02 5.825529e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.007944 
v ship speed   (m/s) : 0.003152 
x displacement   (m) : -5.600242 
y displacement   (m) : 2.222400 
bottom found at  (m) : 231.210376 
maximum instr dep(m) : 185.582804 
maximum prof dep (m) : 224.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 60016 to 60375 
 
Computer : MACI64 
Total processing time:  225 seconds 
