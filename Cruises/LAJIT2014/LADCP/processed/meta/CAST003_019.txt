    LADCP processing protocol 
 
Station name         : LaJIT_003_019 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 23 13 40 
Prof end (LADCP)     : 2014 12  6 23 23 38 
Start position       : 32�N 51.5784'   117�W 16.3152' 
End position         : 32�N 51.5520'   117�W 16.2444' 
Magnetic deviation   : 13.196702 
Time offset          : 0.000000 
Depths               : 8.879056e+00 1.303057e+02 1.303057e+02 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.184185 
v ship speed   (m/s) : -0.081761 
x displacement   (m) : 110.142716 
y displacement   (m) : -48.892800 
bottom found at  (m) : 238.166033 
maximum instr dep(m) : 130.305656 
maximum prof dep (m) : 224.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 40679 to 40977 
 
Computer : MACI64 
Total processing time:  229 seconds 
