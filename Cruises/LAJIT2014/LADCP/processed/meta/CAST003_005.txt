    LADCP processing protocol 
 
Station name         : LaJIT_003_005 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 20 23 18 
Prof end (LADCP)     : 2014 12  6 20 34 47 
Start position       : 32�N 51.5712'   117�W 16.2912' 
End position         : 32�N 51.5593'   117�W 16.2648' 
Magnetic deviation   : 13.196562 
Time offset          : 0.000000 
Depths               : 1.023216e+01 1.515715e+02 5.364517e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.059608 
v ship speed   (m/s) : -0.032077 
x displacement   (m) : 41.070165 
y displacement   (m) : -22.101073 
bottom found at  (m) : 167.757709 
maximum instr dep(m) : 151.571514 
maximum prof dep (m) : 160.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 35521 to 35871 
 
Computer : MACI64 
Total processing time:  228 seconds 
