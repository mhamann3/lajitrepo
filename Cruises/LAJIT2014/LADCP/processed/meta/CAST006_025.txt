    LADCP processing protocol 
 
Station name         : LaJIT_006_025 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  8  1 36 21 
Prof end (LADCP)     : 2014 12  8  1 49 29 
Start position       : 32�N 51.7748'   117�W 16.4756' 
End position         : 32�N 51.7932'   117�W 16.4880' 
Magnetic deviation   : 13.198004 
Time offset          : 0.000000 
Depths               : 9.871384e+00 1.920194e+02 6.042086e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.024433 
v ship speed   (m/s) : 0.043190 
x displacement   (m) : -19.253337 
y displacement   (m) : 34.033462 
bottom found at  (m) : 227.157475 
maximum instr dep(m) : 192.019408 
maximum prof dep (m) : 220.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 88067 to 88460 
 
Computer : MACI64 
Total processing time:  225 seconds 
