    LADCP processing protocol 
 
Station name         : LaJIT_001_006 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6  3 38 39 
Prof end (LADCP)     : 2014 12  6  3 48  0 
Start position       : 32�N 51.6024'   117�W 16.2955' 
End position         : 32�N 51.6012'   117�W 16.2960' 
Magnetic deviation   : 13.196760 
Time offset          : 0.000000 
Depths               : 1.004356e+01 1.475660e+02 4.630931e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.001478 
v ship speed   (m/s) : -0.003961 
x displacement   (m) : -0.829362 
y displacement   (m) : -2.222400 
bottom found at  (m) : 172.451474 
maximum instr dep(m) : 147.566016 
maximum prof dep (m) : 168.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 5444 to 5740 
 
Computer : MACI64 
Total processing time:  228 seconds 
