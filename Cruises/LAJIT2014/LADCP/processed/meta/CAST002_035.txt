    LADCP processing protocol 
 
Station name         : LaJIT_002_035 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 15 55 37 
Prof end (LADCP)     : 2014 12  6 16  6 40 
Start position       : 32�N 51.5988'   117�W 16.2972' 
End position         : 32�N 51.6036'   117�W 16.2804' 
Magnetic deviation   : 13.196745 
Time offset          : 0.000000 
Depths               : 1.022927e+01 1.520281e+02 5.626844e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.039420 
v ship speed   (m/s) : 0.013408 
x displacement   (m) : 26.135383 
y displacement   (m) : 8.889600 
bottom found at  (m) : 171.209514 
maximum instr dep(m) : 152.028140 
maximum prof dep (m) : 160.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 27504 to 27845 
 
Computer : MACI64 
Total processing time:  227 seconds 
