    LADCP processing protocol 
 
Station name         : LaJIT_006_035 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  8  3 54  9 
Prof end (LADCP)     : 2014 12  8  4  8  6 
Start position       : 32�N 51.8172'   117�W 16.4640' 
End position         : 32�N 51.8328'   117�W 16.5036' 
Magnetic deviation   : 13.198143 
Time offset          : 0.000000 
Depths               : 9.615758e+00 2.005007e+02 9.291450e+00 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.073599 
v ship speed   (m/s) : 0.034518 
x displacement   (m) : -61.602240 
y displacement   (m) : 28.891200 
bottom found at  (m) : 225.869239 
maximum instr dep(m) : 200.500729 
maximum prof dep (m) : 220.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 92194 to 92610 
 
Computer : MACI64 
Total processing time:  221 seconds 
