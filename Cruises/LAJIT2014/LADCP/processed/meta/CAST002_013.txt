    LADCP processing protocol 
 
Station name         : LaJIT_002_013 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 11 16 46 
Prof end (LADCP)     : 2014 12  6 11 28 12 
Start position       : 32�N 51.5868'   117�W 16.2985' 
End position         : 32�N 51.5772'   117�W 16.3020' 
Magnetic deviation   : 13.196684 
Time offset          : 0.000000 
Depths               : 9.851966e+00 1.540513e+02 5.127113e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.008006 
v ship speed   (m/s) : -0.025917 
x displacement   (m) : -5.492287 
y displacement   (m) : -17.779200 
bottom found at  (m) : 175.034320 
maximum instr dep(m) : 154.051251 
maximum prof dep (m) : 164.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 19148 to 19501 
 
Computer : MACI64 
Total processing time:  217 seconds 
