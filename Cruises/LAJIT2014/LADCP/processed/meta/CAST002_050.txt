    LADCP processing protocol 
 
Station name         : LaJIT_002_050 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 18 46  4 
Prof end (LADCP)     : 2014 12  6 18 55  9 
Start position       : 32�N 51.6240'   117�W 16.3619' 
End position         : 32�N 51.6096'   117�W 16.3440' 
Magnetic deviation   : 13.196915 
Time offset          : 0.000000 
Depths               : 1.028450e+01 1.184608e+02 5.414941e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.051220 
v ship speed   (m/s) : -0.048934 
x displacement   (m) : 27.914763 
y displacement   (m) : -26.668800 
bottom found at  (m) : 162.610342 
maximum instr dep(m) : 118.460797 
maximum prof dep (m) : 156.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 32607 to 32890 
 
Computer : MACI64 
Total processing time:  224 seconds 
