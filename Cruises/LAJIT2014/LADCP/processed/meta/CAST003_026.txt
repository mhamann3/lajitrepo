    LADCP processing protocol 
 
Station name         : LaJIT_003_026 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7  0 36  4 
Prof end (LADCP)     : 2014 12  7  0 47 36 
Start position       : 32�N 51.5770'   117�W 16.3002' 
End position         : 32�N 51.6258'   117�W 16.2793' 
Magnetic deviation   : 13.196581 
Time offset          : 0.000000 
Depths               : 9.708884e+00 1.539983e+02 3.724436e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.046925 
v ship speed   (m/s) : 0.130634 
x displacement   (m) : 32.471795 
y displacement   (m) : 90.399064 
bottom found at  (m) : 164.304340 
maximum instr dep(m) : 153.998346 
maximum prof dep (m) : 156.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 43093 to 43465 
 
Computer : MACI64 
Total processing time:  228 seconds 
