    LADCP processing protocol 
 
Station name         : LaJIT_005_037 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7 17  3 51 
Prof end (LADCP)     : 2014 12  7 17 15 37 
Start position       : 32�N 51.8244'   117�W 16.4784' 
End position         : 32�N 51.7848'   117�W 16.4763' 
Magnetic deviation   : 13.198239 
Time offset          : 0.000000 
Depths               : 1.017926e+01 1.780832e+02 5.779504e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.004724 
v ship speed   (m/s) : -0.103991 
x displacement   (m) : 3.335486 
y displacement   (m) : -73.417608 
bottom found at  (m) : 219.273416 
maximum instr dep(m) : 178.083166 
maximum prof dep (m) : 200.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 72667 to 73024 
 
Computer : MACI64 
Total processing time:  223 seconds 
