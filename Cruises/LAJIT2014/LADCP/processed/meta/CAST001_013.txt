    LADCP processing protocol 
 
Station name         : LaJIT_001_013 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6  5  0 40 
Prof end (LADCP)     : 2014 12  6  5 12  6 
Start position       : 32�N 51.6084'   117�W 16.3116' 
End position         : 32�N 51.6192'   117�W 16.3233' 
Magnetic deviation   : 13.196799 
Time offset          : 0.000000 
Depths               : 9.861980e+00 1.580319e+02 3.111337e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.026562 
v ship speed   (m/s) : 0.029157 
x displacement   (m) : -18.221314 
y displacement   (m) : 20.001600 
bottom found at  (m) : 177.448727 
maximum instr dep(m) : 158.031949 
maximum prof dep (m) : 168.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 7894 to 8264 
 
Computer : MACI64 
Total processing time:  232 seconds 
