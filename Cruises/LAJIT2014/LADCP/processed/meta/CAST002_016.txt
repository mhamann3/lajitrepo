    LADCP processing protocol 
 
Station name         : LaJIT_002_016 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6 11 50  5 
Prof end (LADCP)     : 2014 12  6 12  0 40 
Start position       : 32�N 51.6096'   117�W 16.3068' 
End position         : 32�N 51.5868'   117�W 16.3128' 
Magnetic deviation   : 13.196778 
Time offset          : 0.000000 
Depths               : 1.012902e+01 1.435549e+02 1.136857e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.014699 
v ship speed   (m/s) : -0.066497 
x displacement   (m) : -9.334071 
y displacement   (m) : -42.225600 
bottom found at  (m) : 189.263062 
maximum instr dep(m) : 143.554855 
maximum prof dep (m) : 184.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 20149 to 20473 
 
Computer : MACI64 
Total processing time:  217 seconds 
