    LADCP processing protocol 
 
Station name         : LaJIT_002_004 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  6  9 18 32 
Prof end (LADCP)     : 2014 12  6  9 31 44 
Start position       : 32�N 51.6084'   117�W 16.3020' 
End position         : 32�N 51.5916'   117�W 16.3164' 
Magnetic deviation   : 13.196778 
Time offset          : 0.000000 
Depths               : 9.643761e+00 1.635768e+02 4.521836e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : -0.028285 
v ship speed   (m/s) : -0.039285 
x displacement   (m) : -22.401762 
y displacement   (m) : -31.113600 
bottom found at  (m) : 178.162517 
maximum instr dep(m) : 163.576806 
maximum prof dep (m) : 172.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 15612 to 16017 
 
Computer : MACI64 
Total processing time:  217 seconds 
