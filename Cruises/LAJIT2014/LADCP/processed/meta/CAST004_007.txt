    LADCP processing protocol 
 
Station name         : LaJIT_004_007 
 
LADCP input files    : 
down                 : ~/MATLAB/Coursework/Proposal/LAJIT/LADCP/raw/LAJMD002.000 
up                   :   
 
Command file info    :  
 
Prof start (LADCP)   : 2014 12  7  5 34 37 
Prof end (LADCP)     : 2014 12  7  5 51 56 
Start position       : 32�N 51.7932'   117�W 16.5180' 
End position         : 32�N 51.7931'   117�W 16.5084' 
Magnetic deviation   : 13.198214 
Time offset          : 0.000000 
Depths               : 9.268949e+00 2.220239e+02 5.610891e+01 
 
Processing thresholds  
 
min percent good     : 0.000000 
max error velocity   : 0.500000 
max w variability    : 0.200000 
 
Processing results  
 
u ship speed   (m/s) : 0.014373 
v ship speed   (m/s) : -0.000231 
x displacement   (m) : 14.933966 
y displacement   (m) : -0.239917 
bottom found at  (m) : 234.568705 
maximum instr dep(m) : 222.023873 
maximum prof dep (m) : 228.000000 
bottom track mode    : 3.000000 
bottom track thrsd   : 10.000000 
bottom track range   : 300.000000 
bottom track range   : 50.000000 
bottom track source  : normal data 
profile ensembles    : 52086 to 52604 
 
Computer : MACI64 
Total processing time:  225 seconds 
